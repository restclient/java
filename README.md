Universal API Client
==========

> RestClient/Java is a universal Java client library for RESTful Web services.

_Just migrated the old project. The information in this README is not entirely correct at this point. WIP._



Goal
---

The REST has become the _de facto_ standard for the Web-based APIs.
It is 
* simple (or, simpler than other alternatives), and
* "universal" in the sense that all RESTful APIs share the common characteristics.

Unfortunately, programming at the HTTP level is still a complex task.
There are many "high level" wrapper libraries which hides the details of the HTTP protocol layer complexity
and expose the underlying Web "resources" as conventional programming APIs.
These libraries are inherently specific to a particular Web services
encapsualting the Web resources. For example, Google has client libraries for Google APIs and only for Google APIs (in various programming languages).
If you plan to access Twitter API, you need a separate client library.
If you plan to access Facebook API, then you need yet another separate client library.
And, so forth, so forth...

What's worse is that these "client libraries" all use different style of (programming) APIs,
completely "nullifying" the universality of the underlying REST principle.

The goal of `RestClient` is to provide a "mid level" client library,
* which hides the complexity of the HTTP layer programming, and
* which is universal enough to be useable with _all_ RESTful web services.



Modules
---

`RestClient/Java` comprises multiple modules.
But the core part of the functionalitis is included in the `restclient-core` module.
You can normally specify the Maven dependency on the core module only.
The `restclient-ext` module includes some experimental packages.
In particular, it includes various client classees adapted to Google API, or Twitter API, etc.
They should be considered "examples" of the core library.
Generally, you need not create a subclass(es) of the REST API client classes for any specific Web services.



Usage
---

If you use Maven, you can (locally) build and install the `RestClient/Java` library by running standard Maven goals. 
(If you don't use Maven, then there is an ant script under _nomaven/scripts_.)
Otherwise, you can add, in your project, the following dependency for the core module:

  	<dependency>
      <groupId>restclient</groupId>
      <artifactId>restclient-core</artifactId>
      <version>0.8.3</version>
  	</dependency>

Or, if you use the (experimental) ext module, then include in your POM file the following dependency:

  	<dependency>
      <groupId>restclient</groupId>
      <artifactId>restclient-ext</artifactId>
      <version>0.8.3</version>
  	</dependency>



API Design
---

(TBD)



Java Docs
---

Please refer to [the online API Docs](http://restclient.gitlab.io/java-apidocs/).

<!--
Please refer to [the Project wiki pages](https://gitlab.com/restclient/java/wiki/_pages) 
or [the online API Docs](http://restclient.gitlab.io/java-apidocs/).
-->



