package restclient.factory.manager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import restclient.factory.RestServiceClientFactory;
import restclient.factory.RestUserClientFactory;
import restclient.factory.impl.AbstractRestServiceClientFactory;
import restclient.factory.mock.MockRestServiceClientFactory;

public class AbstractRestClientFactoryManagerTest
{

    private AbstractRestClientFactoryManager abstractRestClientFactoryManager;
    
    @Before
    public void setUp() throws Exception
    {
        abstractRestClientFactoryManager = TestRestClientFactoryManager.getInstance();
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGetRestServiceClientFactory()
    {
        RestServiceClientFactory restServiceClientFactory1 = MockRestServiceClientFactory.getInstance();
        abstractRestClientFactoryManager.setRestClientFactories(restServiceClientFactory1);
        
        RestServiceClientFactory restServiceClientFactory2 = abstractRestClientFactoryManager.getRestServiceClientFactory();
        System.out.println("restServiceClientFactory2 = " + restServiceClientFactory2);
        RestUserClientFactory restUserClientFactory2 = abstractRestClientFactoryManager.getRestUserClientFactory();
        System.out.println("restUserClientFactory2 = " + restUserClientFactory2);

    }

    @Test
    public void testGetRestUserClientFactory()
    {
        RestServiceClientFactory restServiceClientFactory1 = AbstractRestServiceClientFactory.getInstance();
        abstractRestClientFactoryManager.setRestClientFactories(restServiceClientFactory1);
        
        RestServiceClientFactory restServiceClientFactory2 = abstractRestClientFactoryManager.getRestServiceClientFactory();
        System.out.println("restServiceClientFactory2 = " + restServiceClientFactory2);
        RestUserClientFactory restUserClientFactory2 = abstractRestClientFactoryManager.getRestUserClientFactory();
        System.out.println("restUserClientFactory2 = " + restUserClientFactory2);

    }

}
