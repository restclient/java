package restclient.factory.manager;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AbstractApiClientFactoryManagerTest.class,
        AbstractRestClientFactoryManagerTest.class })
public class AllTests
{

}
