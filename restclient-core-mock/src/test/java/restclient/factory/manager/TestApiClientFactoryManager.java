package restclient.factory.manager;



public class TestApiClientFactoryManager extends
        AbstractApiClientFactoryManager
{

    private TestApiClientFactoryManager()
    {
    }

    // Initialization-on-demand holder.
    private static final class TestApiClientFactoryManagerHolder
    {
        private static final TestApiClientFactoryManager INSTANCE = new TestApiClientFactoryManager() {};
    }

    // Singleton method
    public static TestApiClientFactoryManager getInstance()
    {
        return TestApiClientFactoryManagerHolder.INSTANCE;
    }

}
