package restclient.factory.manager;



public class TestRestClientFactoryManager extends
        AbstractRestClientFactoryManager
{

    private TestRestClientFactoryManager()
    {
    }

    // Initialization-on-demand holder.
    private static final class TestRestClientFactoryManagerHolder
    {
        private static final TestRestClientFactoryManager INSTANCE = new TestRestClientFactoryManager() {};
    }

    // Singleton method
    public static TestRestClientFactoryManager getInstance()
    {
        return TestRestClientFactoryManagerHolder.INSTANCE;
    }

}
