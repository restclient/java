package restclient.common.mock;

import java.util.logging.Logger;

import restclient.common.impl.AbstractRestRequesOptiontBuilder;


public class MockRestRequestOptionBuilder extends AbstractRestRequesOptiontBuilder
{
    private static final Logger log = Logger.getLogger(MockRestRequestOptionBuilder.class.getName());
    private static final long serialVersionUID = 1L;

    public MockRestRequestOptionBuilder()
    {
    }


    
    @Override
    public String toString()
    {
        return "MockRestRequestOptionBuilder []";
    }

}
