package restclient.common.mock;

import java.util.logging.Logger;

import restclient.common.impl.AbstractCacheControlPolicy;


public class MockCacheControlPolicy extends AbstractCacheControlPolicy
{
    private static final Logger log = Logger.getLogger(MockCacheControlPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    
    @Override
    public String toString()
    {
        return "MockCacheControlPolicy [getCachControl()=" + getCachControl()
                + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
                + ", toString()=" + super.toString() + "]";
    }

    
}
