package restclient.common.mock;

import java.util.logging.Logger;

import restclient.common.impl.AbstractResourceUrlBuilder;


// Mock implementation.
public class MockResourceUrlBuilder extends AbstractResourceUrlBuilder
{
    private static final Logger log = Logger.getLogger(MockResourceUrlBuilder.class.getName());
    private static final long serialVersionUID = 1L;

    public MockResourceUrlBuilder(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }


    
    @Override
    public String toString()
    {
        return "MockResourceUrlBuilder [getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getParentResourceBaseUrl()="
                + getParentResourceBaseUrl() + ", getParentResourceId()="
                + getParentResourceId() + ", getResourceName()="
                + getResourceName() + ", getSuffix()=" + getSuffix()
                + ", getResourcePostUrl()=" + getResourcePostUrl() + "]";
    }


}
