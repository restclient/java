package restclient.common.mock;

import java.util.logging.Logger;

import restclient.common.impl.AbstractRestRequestOption;


public class MockRestRequestOption extends AbstractRestRequestOption
{
    private static final Logger log = Logger.getLogger(MockRestRequestOption.class.getName());
    private static final long serialVersionUID = 1L;

    public MockRestRequestOption()
    {
    }


    
    @Override
    public String toString()
    {
        return "MockRestRequestOption [getMethod()=" + getMethod()
                + ", getConnectTimeout()=" + getConnectTimeout()
                + ", getHeaders()=" + getHeaders() + "]";
    }

}
