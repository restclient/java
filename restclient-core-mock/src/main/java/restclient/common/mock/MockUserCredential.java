package restclient.common.mock;

import java.util.Set;
import java.util.logging.Logger;

import restclient.credential.impl.AbstractUserCredential;


public class MockUserCredential extends AbstractUserCredential
{
    private static final Logger log = Logger.getLogger(MockUserCredential.class.getName());
    private static final long serialVersionUID = 1L;

    public MockUserCredential()
    {
        super();
    }

    public MockUserCredential(boolean authRequred)
    {
        super(authRequred);
    }
    public MockUserCredential(boolean authRequred, String user, String userId)
    {
        super(authRequred, user, userId);
    }
    public MockUserCredential(boolean authRequired, String user, String userId,
            String authMethod)
    {
        super(authRequired, user, userId, authMethod);
    }
    public MockUserCredential(boolean authRequired, String user, String userId,
            String authMethod, String authToken, String authSecret)
    {
        super(authRequired, user, userId, authMethod, authToken, authSecret);
    }
    public MockUserCredential(boolean authRequired, String user, String userId,
            String authMethod, String authToken, String authSecret,
            Set<String> dataScopes)
    {
        super(authRequired, user, userId, authMethod, authToken, authSecret, dataScopes);
    }
    public MockUserCredential(boolean authRequired, String user, String userId,
            String authMethod, String authToken, String authSecret,
            Set<String> dataScopes, Long expirationTime, Long refreshedTime)
    {
        super(authRequired, user, userId, authMethod, authToken, authSecret,
                dataScopes, expirationTime, refreshedTime);
    }


    
    @Override
    public String toString()
    {
        return "MockUserCredential [isAuthRequired()=" + isAuthRequired()
                + ", getUser()=" + getUser() + ", getUserId()=" + getUserId()
                + ", getAuthMethod()=" + getAuthMethod() + ", getAuthToken()="
                + getAuthToken() + ", getAuthSecret()=" + getAuthSecret()
                + ", getDataScopes()=" + getDataScopes()
                + ", getExpirationTime()=" + getExpirationTime()
                + ", getRefreshedTime()=" + getRefreshedTime() + "]";
    }


}
