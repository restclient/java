package restclient.common.mock;

import java.util.logging.Logger;

import restclient.common.impl.AbstractAuthRefreshPolicy;
import restclient.common.impl.AbstractAutoRedirectPolicy;


public class MockAutoRedirectPolicy extends AbstractAutoRedirectPolicy
{
    private static final Logger log = Logger.getLogger(AbstractAuthRefreshPolicy.class.getName());
    private static final long serialVersionUID = 1L;



    @Override
    public String toString()
    {
        return "MockAutoRedirectPolicy [isAutoFollowPrimaryChoice()="
                + isAutoFollowPrimaryChoice() + ", isAutoRedirect()="
                + isAutoRedirect() + "]";
    }

    
}
