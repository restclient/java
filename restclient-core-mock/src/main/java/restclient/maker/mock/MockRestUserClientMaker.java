package restclient.maker.mock;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.maker.RestUserClientMaker;
import restclient.mock.MockRestServiceClient;


// Mock factory.
public class MockRestUserClientMaker implements RestUserClientMaker
{
    private static final Logger log = Logger.getLogger(MockRestUserClientMaker.class.getName());

    
    protected MockRestUserClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class MockRestUserClientMakerHolder
    {
        private static final MockRestUserClientMaker INSTANCE = new MockRestUserClientMaker();
    }

    // Singleton method
    public static MockRestUserClientMaker getInstance()
    {
        return MockRestUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new MockRestServiceClient(resourceBaseUrl);
    }



    @Override
    public String toString()
    {
        return "MockRestUserClientMaker []";
    }

    
}
