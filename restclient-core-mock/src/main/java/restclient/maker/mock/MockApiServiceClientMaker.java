package restclient.maker.mock;

import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.CrudMethodFilter;
import restclient.common.DataAccessClient;
import restclient.common.RequestRetryPolicy;
import restclient.common.mock.MockAuthRefreshPolicy;
import restclient.common.mock.MockAutoRedirectPolicy;
import restclient.common.mock.MockCacheControlPolicy;
import restclient.common.mock.MockClientCachePolicy;
import restclient.common.mock.MockCrudMethodFilter;
import restclient.common.mock.MockDataAccessClient;
import restclient.common.mock.MockRequestRetryPolicy;
import restclient.maker.ApiServiceClientMaker;
import restclient.mock.MockRestServiceClient;


// Mock factory.
public class MockApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MockApiServiceClientMaker.class.getName());


    protected MockApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class MockApiServiceClientMakerHolder
    {
        private static final MockApiServiceClientMaker INSTANCE = new MockApiServiceClientMaker();
    }

    // Singleton method
    public static MockApiServiceClientMaker getInstance()
    {
        return MockApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        if(log.isLoggable(Level.INFO)) log.info("makeRestClient() called!!! with  resourceBaseUrl = " + resourceBaseUrl);
        return new MockRestServiceClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new MockCrudMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new MockDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new MockAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new MockRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new MockClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new MockCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new MockAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockApiServiceClientMaker []";
    }

    
}
