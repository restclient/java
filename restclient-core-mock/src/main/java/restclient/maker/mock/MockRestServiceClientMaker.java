package restclient.maker.mock;

import java.util.logging.Logger;

import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.DataAccessClient;
import restclient.common.HttpMethodFilter;
import restclient.common.RequestRetryPolicy;
import restclient.common.ResourceUrlBuilder;
import restclient.common.mock.MockAuthRefreshPolicy;
import restclient.common.mock.MockAutoRedirectPolicy;
import restclient.common.mock.MockCacheControlPolicy;
import restclient.common.mock.MockClientCachePolicy;
import restclient.common.mock.MockDataAccessClient;
import restclient.common.mock.MockHttpMethodFilter;
import restclient.common.mock.MockRequestRetryPolicy;
import restclient.common.mock.MockResourceUrlBuilder;
import restclient.maker.RestServiceClientMaker;


// Mock factory.
public class MockRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MockRestServiceClientMaker.class.getName());


    protected MockRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class MockRestServiceClientMakerHolder
    {
        private static final MockRestServiceClientMaker INSTANCE = new MockRestServiceClientMaker();
    }

    // Singleton method
    public static MockRestServiceClientMaker getInstance()
    {
        return MockRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new MockResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new MockHttpMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new MockDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new MockAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new MockRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new MockClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new MockCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new MockAutoRedirectPolicy();
    }


    
    @Override
    public String toString()
    {
        return "MockRestServiceClientMaker []";
    }


}
