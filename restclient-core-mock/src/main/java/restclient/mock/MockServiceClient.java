package restclient.mock;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.ServiceClient;
import restclient.credential.ClientCredential;
import restclient.impl.AbstractServiceClient;
import restclient.proxy.DecoratedServiceClient;


// "Mock" object
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockServiceClient extends AbstractServiceClient implements DecoratedServiceClient, ClientCredential, Serializable
{
    private static final Logger log = Logger.getLogger(MockServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final ServiceClient decoratedClient;


    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockServiceClient()
    {
        this((ClientCredential) null);
    }

    public MockServiceClient(ClientCredential clientCredential)
    {
        this(null, clientCredential);
    }
    public MockServiceClient(ServiceClient decoratedClient)
    {
        this(decoratedClient, null);
    }
    private MockServiceClient(ServiceClient decoratedClient, ClientCredential clientCredential)
    {
        super(clientCredential);
        this.decoratedClient = decoratedClient;
    }


    
    
    @Override
    public String toString()
    {
        return "MockServiceClient [decoratedClient=" + decoratedClient
                + ", getClientKey()=" + getClientKey() + ", getClientSecret()="
                + getClientSecret() + ", getClientCredential()="
                + getClientCredential() + "]";
    }

    
}
