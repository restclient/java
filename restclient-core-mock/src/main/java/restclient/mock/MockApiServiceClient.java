package restclient.mock;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.RestApiException;
import restclient.credential.UserCredential;
import restclient.impl.AbstractApiServiceClient;
import restclient.maker.ApiServiceClientMaker;
import restclient.maker.mock.MockApiServiceClientMaker;
import restclient.proxy.DecoratedApiServiceClient;


// "Mock" object.
// We have a very strange "dual" implementation.
//    We use either inheritance or decoration, based on how the object is constructed.
public class MockApiServiceClient extends AbstractApiServiceClient implements DecoratedApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(MockApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final ApiServiceClient decoratedClient;


    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockApiServiceClient(ApiServiceClient decoratedClient)
    {
        this(decoratedClient, (decoratedClient != null) ? decoratedClient.getResourceBaseUrl() : null);
    }
    public MockApiServiceClient(String resourceBaseUrl)
    {
        this(null, resourceBaseUrl);
    }
    private MockApiServiceClient(ApiServiceClient decoratedClient, String resourceBaseUrl)
    {
        super(resourceBaseUrl);
        this.decoratedClient = decoratedClient;
    }

    
    // Factory methods

    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        log.info("MockApiServiceClient.makeApiServiceClientMaker() called!!!!");
        return MockApiServiceClientMaker.getInstance();
    }


    // Override methods.
    //   Note the unusual "dual" delegation.

    @Override
    public Object get(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockApiServiceClient.get(): credential = " + credential + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.get(credential, id);
        } else {
            return super.get(credential, id);
        }
    }

    @Override
    public List<Object> list(UserCredential credential,
            Map<String, Object> params) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockApiServiceClient.list(): credential = " + credential + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.list(credential, params);
        } else {
            return super.list(credential, params);
        }
    }

    @Override
    public List<String> keys(UserCredential credential,
            Map<String, Object> params) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockApiServiceClient.keys(): credential = " + credential + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.keys(credential, params);
        } else {
            return super.keys(credential, params);
        }
    }

    @Override
    public Object create(UserCredential credential, Object inputData)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockApiServiceClient.create(): credential = " + credential + "; inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.create(credential, inputData);
        } else {
            return super.create(credential, inputData);
        }
    }

    @Override
    public Object create(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockApiServiceClient.create(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.create(credential, inputData, id);
        } else {
            return super.create(credential, inputData, id);
        }
    }

    @Override
    public Object update(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockApiServiceClient.update(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.update(credential, inputData, id);
        } else {
            return super.update(credential, inputData, id);
        }
    }

    @Override
    public Object modify(UserCredential credential, Object partialData,
            String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockApiServiceClient.modify(): credential = " + credential + "; partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.modify(credential, partialData, id);
        } else {
            return super.modify(credential, partialData, id);
        }
    }

    @Override
    public boolean delete(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockApiServiceClient.delete(): credential = " + credential + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.delete(credential, id);
        } else {
            return super.delete(credential, id);
        }
    }

    @Override
    public int delete(UserCredential credential, Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockApiServiceClient.delete(): credential = " + credential + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(credential, params);
        } else {
            return super.delete(credential, params);
        }
    }

    
    
    @Override
    public String toString()
    {
        return "MockApiServiceClient [decoratedClient=" + decoratedClient
                + ", getRestServiceClient()=" + getRestServiceClient()
                + ", getCrudMethodFilter()=" + getCrudMethodFilter()
                + ", getListResponseType()=" + getListResponseType()
                + ", getResourceBaseUrl()=" + getResourceBaseUrl()
                + ", getClientCredential()=" + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }


}
