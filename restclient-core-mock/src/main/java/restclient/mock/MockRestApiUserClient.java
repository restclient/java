package restclient.mock;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.RestApiException;
import restclient.RestApiServiceClient;
import restclient.RestApiUserClient;
import restclient.impl.AbstractRestApiUserClient;
import restclient.proxy.DecoratedRestApiUserClient;


// "Mock" object.
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockRestApiUserClient extends AbstractRestApiUserClient implements DecoratedRestApiUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(MockRestApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final RestApiUserClient decoratedClient;


    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    
    public MockRestApiUserClient(RestApiServiceClient restApiServiceClient)
    {
        this(null, restApiServiceClient);
    }
    public MockRestApiUserClient(RestApiUserClient decoratedClient)
    {
        this(decoratedClient, null);
    }
    private MockRestApiUserClient(RestApiUserClient decoratedClient, RestApiServiceClient restApiServiceClient)
    {
        super(restApiServiceClient);
        this.decoratedClient = decoratedClient;
    }



    // Override methods.
    //   Note the unusual "dual" delegation.

    @Override
    public Map<String, Object> get(String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.get(): id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.get(id, params);
        } else {
            return super.get(id, params);
        }
    }

    @Override
    public Map<String, Object> post(Object inputData)
            throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.post(): inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.post(inputData);
        } else {
            return super.post(inputData);
        }
    }

    @Override
    public Map<String, Object> put(Object inputData,
            String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.put(): inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.put(inputData, id);
        } else {
            return super.put(inputData, id);
        }
    }

    @Override
    public Map<String, Object> patch(Object partialData, String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.patch(): partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.patch(partialData, id);
        } else {
            return super.patch(partialData, id);
        }
    }

    @Override
    public Map<String, Object> delete(String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.delete(): id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(id, params);
        } else {
            return super.delete(id, params);
        }
    }


    @Override
    public Object get(String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.get(): id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.get(id);
        } else {
            return super.get(id);
        }
    }
    @Override
    public List<Object> list(Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.list(): params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.list(params);
        } else {
            return super.list(params);
        }
    }
    @Override
    public List<String> keys(Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.keys(): params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.keys(params);
        } else {
            return super.keys(params);
        }
    }
    @Override
    public Object create(Object inputData) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.create(): inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.create(inputData);
        } else {
            return super.create(inputData);
        }
    }
    @Override
    public Object create(Object inputData, String id) throws RestApiException,
            IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.create(): inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.create(inputData, id);
        } else {
            return super.create(inputData, id);
        }
    }
    @Override
    public Object update(Object inputData, String id) throws RestApiException,
            IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.update(): inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.update(inputData, id);
        } else {
            return super.update(inputData, id);
        }
    }
    @Override
    public Object modify(Object partialData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.modify(): partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.modify(partialData, id);
        } else {
            return super.modify(partialData, id);
        }
    }
    @Override
    public boolean delete(String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.delete(): id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.delete(id);
        } else {
            return super.delete(id);
        }
    }
    @Override
    public int delete(Map<String, Object> params) throws RestApiException,
            IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiUserClient.delete(): params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(params);
        } else {
            return super.delete(params);
        }
    }

    
    
    @Override
    public String toString()
    {
        return "MockRestApiUserClient [decoratedClient=" + decoratedClient
                + ", getRestApiServiceClient()=" + getRestApiServiceClient()
                + ", getUserCredential()=" + getUserCredential()
                + ", getResourceBaseUrl()=" + getResourceBaseUrl()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
