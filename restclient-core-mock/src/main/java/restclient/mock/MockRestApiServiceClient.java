package restclient.mock;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.RestApiException;
import restclient.RestApiServiceClient;
import restclient.credential.UserCredential;
import restclient.impl.AbstractRestApiServiceClient;
import restclient.proxy.DecoratedRestApiServiceClient;


// "Mock" object.
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockRestApiServiceClient extends AbstractRestApiServiceClient implements DecoratedRestApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(MockRestApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final RestApiServiceClient decoratedClient;


    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockRestApiServiceClient()
    {
        this(null);
    }
    private MockRestApiServiceClient(RestApiServiceClient decoratedClient)
    {
        this.decoratedClient = decoratedClient;
    }


    // Override methods.
    //   Note the unusual "dual" delegation.

    @Override
    public Map<String, Object> get(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.get(): credential = " + credential + "; id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.get(credential, id, params);
        } else {
            return super.get(credential, id, params);
        }
    }

    @Override
    public Map<String, Object> post(UserCredential credential, Object inputData)
            throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.post(): credential = " + credential + "; inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.post(credential, inputData);
        } else {
            return super.post(credential, inputData);
        }
    }

    @Override
    public Map<String, Object> put(UserCredential credential, Object inputData,
            String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.put(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.put(credential, inputData, id);
        } else {
            return super.put(credential, inputData, id);
        }
    }

    @Override
    public Map<String, Object> patch(UserCredential credential,
            Object partialData, String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.patch(): credential = " + credential + "; partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.patch(credential, partialData, id);
        } else {
            return super.patch(credential, partialData, id);
        }
    }

    @Override
    public Map<String, Object> delete(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.delete(): credential = " + credential + "; id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(credential, id, params);
        } else {
            return super.delete(credential, id, params);
        }
    }




    @Override
    public Object get(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.get(): credential = " + credential + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.get(credential, id);
        } else {
            return super.get(credential, id);
        }
    }

    @Override
    public List<Object> list(UserCredential credential,
            Map<String, Object> params) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.list(): credential = " + credential + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.list(credential, params);
        } else {
            return super.list(credential, params);
        }
    }

    @Override
    public List<String> keys(UserCredential credential,
            Map<String, Object> params) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.keys(): credential = " + credential + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.keys(credential, params);
        } else {
            return super.keys(credential, params);
        }
    }

    @Override
    public Object create(UserCredential credential, Object inputData)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.create(): credential = " + credential + "; inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.create(credential, inputData);
        } else {
            return super.create(credential, inputData);
        }
    }

    @Override
    public Object create(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.create(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.create(credential, inputData, id);
        } else {
            return super.create(credential, inputData, id);
        }
    }

    @Override
    public Object update(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.update(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.update(credential, inputData, id);
        } else {
            return super.update(credential, inputData, id);
        }
    }

    @Override
    public Object modify(UserCredential credential, Object partialData,
            String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.modify(): credential = " + credential + "; partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.modify(credential, partialData, id);
        } else {
            return super.modify(credential, partialData, id);
        }
    }

    @Override
    public boolean delete(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.delete(): credential = " + credential + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.delete(credential, id);
        } else {
            return super.delete(credential, id);
        }
    }

    @Override
    public int delete(UserCredential credential, Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestApiServiceClient.delete(): credential = " + credential + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(credential, params);
        } else {
            return super.delete(credential, params);
        }
    }

    
    @Override
    public String toString()
    {
        return "MockRestApiServiceClient [decoratedClient=" + decoratedClient
                + ", getRestServiceClient()=" + getRestServiceClient()
                + ", getApiServiceClient()=" + getApiServiceClient()
                + ", getResourceBaseUrl()=" + getResourceBaseUrl()
                + ", getClientCredential()=" + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + "]";
    }


}
