package restclient.mock;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.common.ResourceUrlBuilder;
import restclient.core.ContentFormat;
import restclient.core.HttpMethod;
import restclient.core.StatusCode;
import restclient.credential.UserCredential;
import restclient.credential.impl.AbstractUserCredential;
import restclient.impl.AbstractRestServiceClient;
import restclient.maker.RestServiceClientMaker;
import restclient.maker.mock.MockRestServiceClientMaker;
import restclient.proxy.DecoratedRestServiceClient;
import restclient.util.ResponseUtil;


// "Mock" object.
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockRestServiceClient extends AbstractRestServiceClient implements DecoratedRestServiceClient, ResourceUrlBuilder, Serializable
{
    private static final Logger log = Logger.getLogger(MockRestServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final RestServiceClient decoratedClient;

    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockRestServiceClient(RestServiceClient decoratedClient)
    {
        this(decoratedClient, (decoratedClient != null) ? decoratedClient.getResourceBaseUrl() : null);
    }
    public MockRestServiceClient(String resourceBaseUrl)
    {
        this(null, resourceBaseUrl);
    }
//    public MockRestServiceClient(ResourceUrlBuilder resourceUrlBuilder)
//    {
//        super(resourceUrlBuilder);
//    }
    private MockRestServiceClient(RestServiceClient decoratedClient, String resourceBaseUrl)
    {
        super(resourceBaseUrl);
        this.decoratedClient = decoratedClient;
    }


    
    
    
    @Override
    protected void init()
    {
        super.init();

        // Set the default values.
        setAuthCredentialRequired(false);  // ???
        super.setDefaultAuthCredential(new AbstractUserCredential() {});
        setRequestFormat(ContentFormat.JSON);
        setResponseFormat(ContentFormat.JSON);
        setTimeoutSeconds(10);   // ???
//        setFollowRedirect(false);
//        setMaxFollow(0);
        // ...
    }


    // Factory methods.

    @Override
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return MockRestServiceClientMaker.getInstance();
    }

    
    
    // The "main" implementation of the framework (literally)
    // We need to override this....

    @Override
    protected Map<String, Object> process(String method,
            UserCredential userCredential, Object inputData, String id,
            Map<String, Object> params, boolean retrying) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestServiceClient.get(): method = " + method + "; userCredential = " + userCredential + "; inputData" + inputData + "; id = " + id + "; params = " + params);

        // temporary
        String endpointUrl = null;
        int statusCode = 0;
        Object payload = null;
        String location = null;
        String errorCode = null;
        String errorMessage = null;
        // errorCode = String.valueOf(statusCode);
        // String errorMessage = "Processing failed: statusCode = " + statusCode + " for endpointUrl, " + endpointUrl;
        Map<String, Object> response = null;
        // switch(method) {
        // case HttpMethod.GET:
        if(method.equals(HttpMethod.GET)) {
            endpointUrl = getResourceGetUrl(id, params);
            statusCode = StatusCode.OK;

            if(id != null && !id.isEmpty()) {
                Map<String,Object> obj1 = new HashMap<String,Object>();
                obj1.put("name", "value");
                payload = obj1;
            } else {
                List<Map<String,Object>> list1 = new ArrayList<Map<String,Object>>();
                Map<String,Object> o1 = new HashMap<String,Object>();
                o1.put("name1", "value1");
                o1.put("name1a", "value1a");
                list1.add(o1);
                Map<String,Object> o2 = new HashMap<String,Object>();
                o2.put("name2", "value2");
                o2.put("name2a", "value2a");
                list1.add(o2);
                payload = list1;
            }

        //     break;
        // case HttpMethod.POST:
        } else if(method.equals(HttpMethod.POST)) {
            endpointUrl = getResourcePostUrl();
            statusCode = StatusCode.OK;

            Map<String,Object> obj2 = new HashMap<String,Object>();
            obj2.put("name", "value");
            payload = obj2;

        //     break;
        // case HttpMethod.PUT:
        } else if(method.equals(HttpMethod.PUT)) {
            endpointUrl = getResourcePutUrl(id);
            statusCode = StatusCode.OK;

            Map<String,Object> obj3 = new HashMap<String,Object>();
            obj3.put("name", "value");
            payload = obj3;

        //     break;
        // case HttpMethod.PATCH:
        } else if(method.equals(HttpMethod.PATCH)) {
            endpointUrl = getResourcePatchUrl(id);
            statusCode = StatusCode.OK;

            Map<String,Object> obj4 = new HashMap<String,Object>();
            obj4.put("name", "value");
            payload = obj4;

        //     break;
        // case HttpMethod.DELETE:
        } else if(method.equals(HttpMethod.DELETE)) {
            endpointUrl = getResourceDeleteUrl(id, params);
            statusCode = StatusCode.OK;

            if(id != null && !id.isEmpty()) {
                payload = true;
            } else {
                payload = 10;
            }
            
        //     break;
        // default:
        } else {
            // error
            // what to do?                
        }

        response = ResponseUtil.buildResponse(endpointUrl, statusCode, payload, location);
        // response = ResponseUtil.buildResponse(endpointUrl, statusCode, errorCode, errorMessage);

        return response;  
    }

    
    // Override methods.
    //   Note the unusual "dual" delegation.

    @Override
    public Map<String, Object> get(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestServiceClient.get(): credential = " + credential + "; id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.get(credential, id, params);
        } else {
            return super.get(credential, id, params);
        }
    }

    @Override
    public Map<String, Object> post(UserCredential credential, Object inputData)
            throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestServiceClient.post(): credential = " + credential + "; inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.post(credential, inputData);
        } else {
            return super.post(credential, inputData);
        }
    }

    @Override
    public Map<String, Object> put(UserCredential credential, Object inputData,
            String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestServiceClient.put(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.put(credential, inputData, id);
        } else {
            return super.put(credential, inputData, id);
        }
    }

    @Override
    public Map<String, Object> patch(UserCredential credential,
            Object partialData, String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestServiceClient.patch(): credential = " + credential + "; partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.patch(credential, partialData, id);
        } else {
            return super.patch(credential, partialData, id);
        }
    }

    @Override
    public Map<String, Object> delete(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestServiceClient.delete(): credential = " + credential + "; id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(credential, id, params);
        } else {
            return super.delete(credential, id, params);
        }
    }

    
    
    @Override
    public String toString()
    {
        return "MockRestServiceClient [decoratedClient=" + decoratedClient
                + ", getResourceUrlBuilder()=" + getResourceUrlBuilder()
                + ", getHttpMethodFilter()=" + getHttpMethodFilter()
                + ", getResourceBaseUrl()=" + getResourceBaseUrl()
                + ", getResourcePostUrl()=" + getResourcePostUrl()
                + ", isAuthCredentialRequired()=" + isAuthCredentialRequired()
                + ", getDefaultAuthCredential()=" + getDefaultAuthCredential()
                + ", getClientCredential()=" + getClientCredential()
                + ", getRequiredScopes()=" + getRequiredScopes()
                + ", getRequestFormat()=" + getRequestFormat()
                + ", getResponseFormat()=" + getResponseFormat()
                + ", getTimeoutSeconds()=" + getTimeoutSeconds()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
