package restclient.mock;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.UserClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.mock.MockAuthRefreshPolicy;
import restclient.credential.UserCredential;
import restclient.impl.AbstractUserClient;
import restclient.proxy.DecoratedUserClient;


// "Mock" object
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockUserClient extends AbstractUserClient implements DecoratedUserClient, UserCredential, Serializable
{
    private static final Logger log = Logger.getLogger(MockUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final UserClient decoratedClient;


    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockUserClient()
    {
        this((UserCredential) null);
    }

    public MockUserClient(UserCredential userCredential)
    {
        this(null, userCredential);
    }
    public MockUserClient(UserClient decoratedClient)
    {
        this(decoratedClient, null);
    }
    private MockUserClient(UserClient decoratedClient, UserCredential userCredential)
    {
        super(userCredential);
        this.decoratedClient = decoratedClient;
    }

    
    // Factory method.
    
    @Override
    protected AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new MockAuthRefreshPolicy();
    }


    
    
    
    @Override
    public String toString()
    {
        return "MockUserClient [decoratedClient=" + decoratedClient
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", isAuthRequired()=" + isAuthRequired() + ", getUser()="
                + getUser() + ", getUserId()=" + getUserId()
                + ", getAuthMethod()=" + getAuthMethod() + ", getAuthToken()="
                + getAuthToken() + ", getAuthSecret()=" + getAuthSecret()
                + ", getDataScopes()=" + getDataScopes()
                + ", getExpirationTime()=" + getExpirationTime()
                + ", getRefreshedTime()=" + getRefreshedTime()
                + ", getUserCredential()=" + getUserCredential()
                + ", refreshUserCredential()=" + refreshUserCredential() + "]";
    }


}
