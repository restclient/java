package restclient.factory.mock;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ResourceClient;
import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.CrudMethodFilter;
import restclient.common.DataAccessClient;
import restclient.common.RequestRetryPolicy;
import restclient.factory.ApiServiceClientFactory;
import restclient.factory.ApiUserClientFactory;
import restclient.factory.ClientFactory;
import restclient.maker.ApiServiceClientMaker;
import restclient.maker.mock.MockApiServiceClientMaker;
import restclient.mock.MockApiServiceClient;


public class MockApiServiceClientFactory implements ApiServiceClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(MockApiServiceClientFactory.class.getName());

    // Mock factory.
    private ApiServiceClientMaker apiServiceClientMaker;

    protected MockApiServiceClientFactory()
    {
        apiServiceClientMaker = makeApiServiceClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class MockApiServiceClientFactoryHolder
    {
        private static final MockApiServiceClientFactory INSTANCE = new MockApiServiceClientFactory() {};
    }

    // Singleton method
    public static MockApiServiceClientFactory getInstance()
    {
        return MockApiServiceClientFactoryHolder.INSTANCE;
    }

    
    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiServiceClient(resourceBaseUrl);
    }

    @Override
    public ApiUserClientFactory createApiUserClientFactory()
    {
        return makeApiUserClientFactory();
    }


    // Factory methods

    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new MockApiServiceClient(resourceBaseUrl);
    }
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return MockApiServiceClientMaker.getInstance();
    }
    protected ApiUserClientFactory makeApiUserClientFactory()
    {
        return MockApiUserClientFactory.getInstance();
    }


    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return apiServiceClientMaker.makeRestClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return apiServiceClientMaker.makeCrudMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return apiServiceClientMaker.makeDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return apiServiceClientMaker.makeAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return apiServiceClientMaker.makeRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return apiServiceClientMaker.makeClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return apiServiceClientMaker.makeCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return apiServiceClientMaker.makeAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockApiServiceClientFactory [apiServiceClientMaker="
                + apiServiceClientMaker + "]";
    }


}
