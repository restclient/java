package restclient.factory.mock;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ApiUserClient;
import restclient.ResourceClient;
import restclient.factory.ApiUserClientFactory;
import restclient.factory.ClientFactory;
import restclient.maker.ApiUserClientMaker;
import restclient.maker.mock.MockApiUserClientMaker;
import restclient.mock.MockApiUserClient;


public class MockApiUserClientFactory implements ApiUserClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(MockApiUserClientFactory.class.getName());


    // Mock factory.
    private ApiUserClientMaker apiUserClientMaker;

    protected MockApiUserClientFactory()
    {
        apiUserClientMaker = makeApiUserClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class MockApiUserClientFactoryHolder
    {
        private static final MockApiUserClientFactory INSTANCE = new MockApiUserClientFactory() {};
    }

    // Singleton method
    public static MockApiUserClientFactory getInstance()
    {
        return MockApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new MockApiUserClient(resourceBaseUrl) {};
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return MockApiUserClientMaker.getInstance();
    }



    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiUserClient(resourceBaseUrl);
    }

    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return apiUserClientMaker.makeServiceClient(resourceBaseUrl);
    }



    @Override
    public String toString()
    {
        return "MockApiUserClientFactory [apiUserClientMaker="
                + apiUserClientMaker + "]";
    }


}
