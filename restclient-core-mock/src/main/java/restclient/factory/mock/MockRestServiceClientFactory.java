package restclient.factory.mock;

import java.util.logging.Logger;

import restclient.ResourceClient;
import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.DataAccessClient;
import restclient.common.HttpMethodFilter;
import restclient.common.RequestRetryPolicy;
import restclient.common.ResourceUrlBuilder;
import restclient.factory.ClientFactory;
import restclient.factory.RestServiceClientFactory;
import restclient.factory.RestUserClientFactory;
import restclient.maker.RestServiceClientMaker;
import restclient.maker.mock.MockRestServiceClientMaker;
import restclient.mock.MockRestServiceClient;


public class MockRestServiceClientFactory implements RestServiceClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(MockRestServiceClientFactory.class.getName());

    // Mock factory.
    private RestServiceClientMaker restServiceClientMaker;

    protected MockRestServiceClientFactory()
    {
        restServiceClientMaker = makeRestServiceClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class MockRestServiceClientFactoryHolder
    {
        private static final MockRestServiceClientFactory INSTANCE = new MockRestServiceClientFactory() {};
    }

    // Singleton method
    public static MockRestServiceClientFactory getInstance()
    {
        return MockRestServiceClientFactoryHolder.INSTANCE;
    }

    
    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestServiceClient(resourceBaseUrl);
    }

    @Override
    public RestUserClientFactory createRestUserClientFactory()
    {
        return makeRestUserClientFactory();
    }


    // Factory methods

    protected RestServiceClient makeRestServiceClient(String resourceBaseUrl)
    {
        return new MockRestServiceClient(resourceBaseUrl);
    }
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return MockRestServiceClientMaker.getInstance();
    }
    protected RestUserClientFactory makeRestUserClientFactory()
    {
        return MockRestUserClientFactory.getInstance();
    }


    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return restServiceClientMaker.makeResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return restServiceClientMaker.makeHttpMethodFilter();
    }


    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return restServiceClientMaker.makeDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return restServiceClientMaker.makeAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return restServiceClientMaker.makeRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return restServiceClientMaker.makeClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return restServiceClientMaker.makeCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return restServiceClientMaker.makeAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockRestServiceClientFactory [restServiceClientMaker="
                + restServiceClientMaker + "]";
    }

    
}
