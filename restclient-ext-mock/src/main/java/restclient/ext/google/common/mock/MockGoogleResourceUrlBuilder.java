package restclient.ext.google.common.mock;

import java.util.logging.Logger;

import restclient.common.impl.AbstractResourceUrlBuilder;


// MockGoogle implementation.
public class MockGoogleResourceUrlBuilder extends AbstractResourceUrlBuilder
{
    private static final Logger log = Logger.getLogger(MockGoogleResourceUrlBuilder.class.getName());

    public MockGoogleResourceUrlBuilder(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }


    
    @Override
    public String toString()
    {
        return "MockGoogleResourceUrlBuilder [getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getParentResourceBaseUrl()="
                + getParentResourceBaseUrl() + ", getParentResourceId()="
                + getParentResourceId() + ", getResourceName()="
                + getResourceName() + ", getSuffix()=" + getSuffix()
                + ", getResourcePostUrl()=" + getResourcePostUrl() + "]";
    }


}
