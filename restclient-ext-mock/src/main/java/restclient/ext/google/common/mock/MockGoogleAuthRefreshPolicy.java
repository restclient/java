package restclient.ext.google.common.mock;

import java.util.logging.Logger;

import restclient.common.impl.AbstractAuthRefreshPolicy;


public class MockGoogleAuthRefreshPolicy extends AbstractAuthRefreshPolicy
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(MockGoogleAuthRefreshPolicy.class.getName());

    
    public MockGoogleAuthRefreshPolicy()
    {
        // ...
    }



    @Override
    public String toString()
    {
        return "MockGoogleAuthRefreshPolicy [getAuthRefreshHandler()="
                + getAuthRefreshHandler() + "]";
    }


 

}
