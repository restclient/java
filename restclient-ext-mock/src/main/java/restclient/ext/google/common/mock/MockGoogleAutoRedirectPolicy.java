package restclient.ext.google.common.mock;

import java.util.logging.Logger;

import restclient.common.impl.AbstractAuthRefreshPolicy;
import restclient.common.impl.AbstractAutoRedirectPolicy;


public class MockGoogleAutoRedirectPolicy extends AbstractAutoRedirectPolicy
{
    private static final Logger log = Logger.getLogger(AbstractAuthRefreshPolicy.class.getName());


    
    @Override
    public String toString()
    {
        return "MockGoogleAutoRedirectPolicy [isAutoFollowPrimaryChoice()="
                + isAutoFollowPrimaryChoice() + ", isAutoRedirect()="
                + isAutoRedirect() + "]";
    }

    
}
