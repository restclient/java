package restclient.ext.google.maker.mock;

import java.util.logging.Logger;

import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.DataAccessClient;
import restclient.common.HttpMethodFilter;
import restclient.common.RequestRetryPolicy;
import restclient.common.ResourceUrlBuilder;
import restclient.common.mock.MockDataAccessClient;
import restclient.common.mock.MockHttpMethodFilter;
import restclient.ext.google.common.GoogleResourceUrlBuilder;
import restclient.ext.google.common.mock.MockGoogleAuthRefreshPolicy;
import restclient.ext.google.common.mock.MockGoogleAutoRedirectPolicy;
import restclient.ext.google.common.mock.MockGoogleCacheControlPolicy;
import restclient.ext.google.common.mock.MockGoogleClientCachePolicy;
import restclient.ext.google.common.mock.MockGoogleRequestRetryPolicy;
import restclient.maker.RestServiceClientMaker;


// GoogleMock factory.
public class MockGoogleRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MockGoogleRestServiceClientMaker.class.getName());


    protected MockGoogleRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class GoogleMockRestServiceClientMakerHolder
    {
        private static final MockGoogleRestServiceClientMaker INSTANCE = new MockGoogleRestServiceClientMaker();
    }

    // Singleton method
    public static MockGoogleRestServiceClientMaker getInstance()
    {
        return GoogleMockRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new GoogleResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new MockHttpMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new MockDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new MockGoogleAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new MockGoogleRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new MockGoogleClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new MockGoogleCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new MockGoogleAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockGoogleRestServiceClientMaker []";
    }


}
