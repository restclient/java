package restclient.ext.google.maker.mock;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.ext.google.maker.GoogleRestUserClientMaker;
import restclient.ext.google.mock.MockGoogleRestServiceClient;
import restclient.maker.RestUserClientMaker;


// GoogleMock factory.
public class MockGoogleRestUserClientMaker implements RestUserClientMaker
{
    private static final Logger log = Logger.getLogger(MockGoogleRestUserClientMaker.class.getName());

    
    protected MockGoogleRestUserClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class GoogleMockRestUserClientMakerHolder
    {
        private static final MockGoogleRestUserClientMaker INSTANCE = new MockGoogleRestUserClientMaker();
    }

    // Singleton method
    public static MockGoogleRestUserClientMaker getInstance()
    {
        return GoogleMockRestUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleRestServiceClient(resourceBaseUrl);
    }



    @Override
    public String toString()
    {
        return "MockGoogleRestUserClientMaker []";
    }

}
