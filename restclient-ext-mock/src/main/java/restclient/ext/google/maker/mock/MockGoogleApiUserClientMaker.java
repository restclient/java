package restclient.ext.google.maker.mock;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ext.google.maker.GoogleApiUserClientMaker;
import restclient.ext.google.mock.MockGoogleApiServiceClient;
import restclient.maker.ApiUserClientMaker;


// Abstract factory.
public class MockGoogleApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(MockGoogleApiUserClientMaker.class.getName());


    protected MockGoogleApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static final class GoogleMockApiUserClientMakerHolder
    {
        private static final MockGoogleApiUserClientMaker INSTANCE = new MockGoogleApiUserClientMaker();
    }

    // Singleton method
    public static MockGoogleApiUserClientMaker getInstance()
    {
        return GoogleMockApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleApiServiceClient(resourceBaseUrl);
    }



    @Override
    public String toString()
    {
        return "MockGoogleApiUserClientMaker []";
    }

    
}
