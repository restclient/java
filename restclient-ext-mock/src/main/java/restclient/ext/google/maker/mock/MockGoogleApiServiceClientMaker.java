package restclient.ext.google.maker.mock;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.CrudMethodFilter;
import restclient.common.DataAccessClient;
import restclient.common.RequestRetryPolicy;
import restclient.common.mock.MockCrudMethodFilter;
import restclient.common.mock.MockDataAccessClient;
import restclient.ext.google.common.mock.MockGoogleAuthRefreshPolicy;
import restclient.ext.google.common.mock.MockGoogleAutoRedirectPolicy;
import restclient.ext.google.common.mock.MockGoogleCacheControlPolicy;
import restclient.ext.google.common.mock.MockGoogleClientCachePolicy;
import restclient.ext.google.common.mock.MockGoogleRequestRetryPolicy;
import restclient.ext.google.mock.MockGoogleRestServiceClient;
import restclient.maker.ApiServiceClientMaker;


// Mock factory.
public class MockGoogleApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MockGoogleApiServiceClientMaker.class.getName());


    protected MockGoogleApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class MockGoogleMockApiServiceClientMakerHolder
    {
        private static final MockGoogleApiServiceClientMaker INSTANCE = new MockGoogleApiServiceClientMaker();
    }

    // Singleton method
    public static MockGoogleApiServiceClientMaker getInstance()
    {
        return MockGoogleMockApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new MockGoogleRestServiceClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new MockCrudMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new MockDataAccessClient();
    }
    
    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new MockGoogleAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new MockGoogleRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new MockGoogleClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new MockGoogleCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new MockGoogleAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockGoogleApiServiceClientMaker []";
    }

    
}
