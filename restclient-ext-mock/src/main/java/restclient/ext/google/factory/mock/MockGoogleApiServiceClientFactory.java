package restclient.ext.google.factory.mock;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ext.google.GoogleApiServiceClient;
import restclient.ext.google.factory.GoogleApiServiceClientFactory;
import restclient.ext.google.factory.GoogleApiUserClientFactory;
import restclient.ext.google.maker.mock.MockGoogleApiServiceClientMaker;
import restclient.ext.google.mock.MockGoogleApiServiceClient;
import restclient.factory.ApiUserClientFactory;
import restclient.factory.mock.MockApiServiceClientFactory;
import restclient.maker.ApiServiceClientMaker;


public class MockGoogleApiServiceClientFactory extends MockApiServiceClientFactory implements GoogleApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(MockGoogleApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleMockApiServiceClientFactoryHolder
    {
        private static final MockGoogleApiServiceClientFactory INSTANCE = new MockGoogleApiServiceClientFactory();
    }

    // Singleton method
    public static MockGoogleApiServiceClientFactory getInstance()
    {
        return GoogleMockApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleApiServiceClient(resourceBaseUrl);
    }
    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return MockGoogleApiServiceClientMaker.getInstance();
    }
    @Override
    protected ApiUserClientFactory makeApiUserClientFactory()
    {
        return makeGoogleApiUserClientFactory();
    }
    protected GoogleApiUserClientFactory makeGoogleApiUserClientFactory()
    {
        return MockGoogleApiUserClientFactory.getInstance();
    }


    @Override
    public GoogleApiServiceClient createGoogleApiServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleApiServiceClient(resourceBaseUrl);
    }


//    @Override
//    public GoogleApiUserClientFactory createGoogleApiUserClientFactory()
//    {
//        return makeGoogleApiUserClientFactory();
//    }


    @Override
    public String toString()
    {
        return "MockGoogleApiServiceClientFactory []";
    }


}
