package restclient.ext.google.factory.mock;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ApiUserClient;
import restclient.credential.UserCredential;
import restclient.ext.google.GoogleApiUserClient;
import restclient.ext.google.factory.GoogleApiUserClientFactory;
import restclient.ext.google.maker.mock.MockGoogleApiUserClientMaker;
import restclient.ext.google.mock.MockGoogleApiUserClient;
import restclient.factory.mock.MockApiUserClientFactory;
import restclient.maker.ApiUserClientMaker;


public class MockGoogleApiUserClientFactory extends MockApiUserClientFactory implements GoogleApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(MockGoogleApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleMockApiUserClientFactoryHolder
    {
        private static final MockGoogleApiUserClientFactory INSTANCE = new MockGoogleApiUserClientFactory();
    }

    // Singleton method
    public static MockGoogleApiUserClientFactory getInstance()
    {
        return GoogleMockApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new MockGoogleApiUserClient(resourceBaseUrl);
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return MockGoogleApiUserClientMaker.getInstance();
    }


    @Override
    public GoogleApiUserClient createGoogleApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        return new MockGoogleApiUserClient(resourceBaseUrl, userCredential);
    }

    @Override
    public GoogleApiUserClient createGoogleApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        return new MockGoogleApiUserClient(apiServiceClient, userCredential);
    }


    @Override
    public String toString()
    {
        return "MockGoogleApiUserClientFactory []";
    }

    
}
