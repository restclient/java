package restclient.ext.google.factory.mock;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.ext.google.GoogleRestServiceClient;
import restclient.ext.google.factory.GoogleRestServiceClientFactory;
import restclient.ext.google.factory.GoogleRestUserClientFactory;
import restclient.ext.google.maker.GoogleRestServiceClientMaker;
import restclient.ext.google.mock.MockGoogleRestServiceClient;
import restclient.factory.RestUserClientFactory;
import restclient.factory.impl.AbstractRestServiceClientFactory;
import restclient.maker.RestServiceClientMaker;


public class MockGoogleRestServiceClientFactory extends AbstractRestServiceClientFactory implements GoogleRestServiceClientFactory
{
    private static final Logger log = Logger.getLogger(MockGoogleRestServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleRestServiceClientFactoryHolder
    {
        private static final MockGoogleRestServiceClientFactory INSTANCE = new MockGoogleRestServiceClientFactory();
    }

    // Singleton method
    public static MockGoogleRestServiceClientFactory getInstance()
    {
        return GoogleRestServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected RestServiceClient makeRestServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleRestServiceClient(resourceBaseUrl);
    }
    @Override
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return GoogleRestServiceClientMaker.getInstance();
    }
    @Override
    protected RestUserClientFactory makeRestUserClientFactory()
    {
        return makeGoogleRestUserClientFactory();
    }
    protected GoogleRestUserClientFactory makeGoogleRestUserClientFactory()
    {
        return MockGoogleRestUserClientFactory.getInstance();
    }

 
    @Override
    public GoogleRestServiceClient createGoogleRestServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleRestServiceClient(resourceBaseUrl);
    }


//    @Override
//    public GoogleRestUserClientFactory createGoogleRestUserClientFactory()
//    {
//        return makeGoogleRestUserClientFactory();
//    }


    @Override
    public String toString()
    {
        return "MockGoogleRestServiceClientFactory []";
    }


}
