package restclient.ext.google.mock;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.core.ContentFormat;
import restclient.credential.UserCredential;
import restclient.credential.impl.AbstractUserCredential;
import restclient.ext.google.maker.mock.MockGoogleRestServiceClientMaker;
import restclient.ext.google.proxy.DecoratedGoogleRestServiceClient;
import restclient.maker.RestServiceClientMaker;
import restclient.mock.MockRestServiceClient;


// "Mock" object.
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockGoogleRestServiceClient extends MockRestServiceClient implements DecoratedGoogleRestServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(MockGoogleRestServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final MockRestServiceClient decoratedClient;

    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockGoogleRestServiceClient(MockRestServiceClient decoratedClient)
    {
        this(decoratedClient, (decoratedClient != null) ? decoratedClient.getResourceBaseUrl() : null);
    }
    public MockGoogleRestServiceClient(String resourceBaseUrl)
    {
        this(null, resourceBaseUrl);
    }
//    public MockGoogleRestServiceClient(ResourceUrlBuilder resourceUrlBuilder)
//    {
//        super(resourceUrlBuilder);
//    }
    private MockGoogleRestServiceClient(MockRestServiceClient decoratedClient, String resourceBaseUrl)
    {
        super(resourceBaseUrl);
        this.decoratedClient = decoratedClient;
    }


    @Override
    protected void init()
    {
        super.init();

        // Set the default values.
        setAuthCredentialRequired(false);  // ???
        super.setDefaultAuthCredential(new AbstractUserCredential() {});
        setRequestFormat(ContentFormat.JSON);
        setResponseFormat(ContentFormat.JSON);
        setTimeoutSeconds(10);   // ???
//        setFollowRedirect(false);
//        setMaxFollow(0);
        // ...
    }


    // Factory methods.

    @Override
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return MockGoogleRestServiceClientMaker.getInstance();
    }


    // Override methods.
    //   Note the unusual "dual" delegation.

    @Override
    public Map<String, Object> get(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestServiceClient.get(): credential = " + credential + "; id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.get(credential, id, params);
        } else {
            return super.get(credential, id, params);
        }
    }

    @Override
    public Map<String, Object> post(UserCredential credential, Object inputData)
            throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestServiceClient.post(): credential = " + credential + "; inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.post(credential, inputData);
        } else {
            return super.post(credential, inputData);
        }
    }

    @Override
    public Map<String, Object> put(UserCredential credential, Object inputData,
            String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestServiceClient.put(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.put(credential, inputData, id);
        } else {
            return super.put(credential, inputData, id);
        }
    }

    @Override
    public Map<String, Object> patch(UserCredential credential,
            Object partialData, String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestServiceClient.patch(): credential = " + credential + "; partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.patch(credential, partialData, id);
        } else {
            return super.patch(credential, partialData, id);
        }
    }

    @Override
    public Map<String, Object> delete(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestServiceClient.delete(): credential = " + credential + "; id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(credential, id, params);
        } else {
            return super.delete(credential, id, params);
        }
    }

    
    
    @Override
    public String toString()
    {
        return "MockGoogleRestServiceClient [decoratedClient="
                + decoratedClient + ", getResourceUrlBuilder()="
                + getResourceUrlBuilder() + ", getHttpMethodFilter()="
                + getHttpMethodFilter() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getResourcePostUrl()="
                + getResourcePostUrl() + ", isAuthCredentialRequired()="
                + isAuthCredentialRequired() + ", getDefaultAuthCredential()="
                + getDefaultAuthCredential() + ", getClientCredential()="
                + getClientCredential() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getRequestFormat()="
                + getRequestFormat() + ", getResponseFormat()="
                + getResponseFormat() + ", getTimeoutSeconds()="
                + getTimeoutSeconds() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + "]";
    }


}
