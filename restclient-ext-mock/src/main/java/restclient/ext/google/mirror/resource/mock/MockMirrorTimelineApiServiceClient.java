package restclient.ext.google.mirror.resource.mock;

import java.util.logging.Logger;

import restclient.ext.google.mirror.mock.MockMirrorApiServiceClient;
import restclient.ext.google.mirror.proxy.DecoratedMirrorApiServiceClient;
import restclient.ext.google.mirror.resource.impl.BaseMirrorTimelineApiServiceClient;


// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockMirrorTimelineApiServiceClient extends MockMirrorApiServiceClient implements DecoratedMirrorApiServiceClient
{
    private static final Logger log = Logger.getLogger(MockMirrorTimelineApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public MockMirrorTimelineApiServiceClient()
    {
        super(BaseMirrorTimelineApiServiceClient.TIMELINE_RESOURCE_BASE_URL);
    }



    @Override
    public String toString()
    {
        return "MockMirrorTimelineApiServiceClient [getRestServiceClient()="
                + getRestServiceClient() + ", getCrudMethodFilter()="
                + getCrudMethodFilter() + ", getListResponseType()="
                + getListResponseType() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }
    
    
    
    
}
