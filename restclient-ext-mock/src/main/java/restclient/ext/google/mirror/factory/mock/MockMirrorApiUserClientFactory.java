package restclient.ext.google.mirror.factory.mock;

import java.util.logging.Logger;

import restclient.ApiUserClient;
import restclient.credential.UserCredential;
import restclient.ext.google.factory.mock.MockGoogleApiUserClientFactory;
import restclient.ext.google.mirror.MirrorApiServiceClient;
import restclient.ext.google.mirror.MirrorApiUserClient;
import restclient.ext.google.mirror.factory.MirrorApiUserClientFactory;
import restclient.ext.google.mirror.maker.mock.MockMirrorApiUserClientMaker;
import restclient.ext.google.mirror.mock.MockMirrorApiUserClient;
import restclient.ext.google.mirror.resource.mock.MockMirrorContactApiUserClient;
import restclient.ext.google.mirror.resource.mock.MockMirrorLocationApiUserClient;
import restclient.ext.google.mirror.resource.mock.MockMirrorSubscriptionApiUserClient;
import restclient.ext.google.mirror.resource.mock.MockMirrorTimelineApiUserClient;
import restclient.ext.google.mirror.resource.mock.MockMirrorTimelineAttachmentApiUserClient;
import restclient.maker.ApiUserClientMaker;


public class MockMirrorApiUserClientFactory extends MockGoogleApiUserClientFactory implements MirrorApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(MockMirrorApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class MirrorMockApiUserClientFactoryHolder
    {
        private static final MockMirrorApiUserClientFactory INSTANCE = new MockMirrorApiUserClientFactory();
    }

    // Singleton method
    public static MockMirrorApiUserClientFactory getInstance()
    {
        return MirrorMockApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new MockMirrorApiUserClient(resourceBaseUrl);
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return MockMirrorApiUserClientMaker.getInstance();
    }

    
    // Resource create methods

    @Override
    public MirrorApiUserClient createMirrorContactApiUserClient(UserCredential userCredential)
    {
        return new MockMirrorContactApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorLocationApiUserClient(UserCredential userCredential)
    {
        return new MockMirrorLocationApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorSubscriptionApiUserClient(UserCredential userCredential)
    {
        return new MockMirrorSubscriptionApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineApiUserClient(UserCredential userCredential)
    {
        return new MockMirrorTimelineApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineAttachmentApiUserClient(String timelineItemId, UserCredential userCredential)
    {
        return new MockMirrorTimelineAttachmentApiUserClient(timelineItemId, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorContactApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorContactApiUserClient(serviceClient, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorLocationApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorLocationApiUserClient(serviceClient, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorSubscriptionApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorSubscriptionApiUserClient(serviceClient, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorTimelineApiUserClient(serviceClient, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineAttachmentApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorTimelineAttachmentApiUserClient(serviceClient, userCredential);
    }



    @Override
    public String toString()
    {
        return "MockMirrorApiUserClientFactory []";
    }

    
}
