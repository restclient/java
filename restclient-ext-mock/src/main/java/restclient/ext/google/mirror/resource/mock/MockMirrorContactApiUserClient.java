package restclient.ext.google.mirror.resource.mock;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.credential.UserCredential;
import restclient.ext.google.mirror.mock.MockMirrorApiUserClient;
import restclient.ext.google.mirror.proxy.DecoratedMirrorApiUserClient;
import restclient.ext.google.mirror.resource.impl.BaseMirrorContactApiServiceClient;


// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockMirrorContactApiUserClient extends MockMirrorApiUserClient implements DecoratedMirrorApiUserClient
{
    private static final Logger log = Logger.getLogger(MockMirrorContactApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public MockMirrorContactApiUserClient()
    {
        this((UserCredential) null);
    }
    public MockMirrorContactApiUserClient(UserCredential userCredential)
    {
        super(BaseMirrorContactApiServiceClient.CONTACT_RESOURCE_BASE_URL, userCredential);
    }

    public MockMirrorContactApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public MockMirrorContactApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    
    @Override
    public String toString()
    {
        return "MockMirrorContactApiUserClient [getApiServiceClient()="
                + getApiServiceClient() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
