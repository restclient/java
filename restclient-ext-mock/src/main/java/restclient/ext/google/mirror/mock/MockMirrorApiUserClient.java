package restclient.ext.google.mirror.mock;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.RestApiException;
import restclient.credential.UserCredential;
import restclient.ext.google.mirror.maker.mock.MockMirrorApiUserClientMaker;
import restclient.ext.google.mirror.proxy.DecoratedMirrorApiUserClient;
import restclient.ext.google.mock.MockGoogleApiUserClient;
import restclient.maker.ApiUserClientMaker;
import restclient.mock.MockApiUserClient;
import restclient.mock.MockRestUserClient;


// Base class for all MirrorMock API resources.
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockMirrorApiUserClient extends MockGoogleApiUserClient implements DecoratedMirrorApiUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(MockMirrorApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    private final MockGoogleApiUserClient decoratedClient;


    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockMirrorApiUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public MockMirrorApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        super(resourceBaseUrl, userCredential);
        this.decoratedClient = null;
    }

    public MockMirrorApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public MockMirrorApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
        this.decoratedClient = null;
    }

    public MockMirrorApiUserClient(MockGoogleApiUserClient decoratedClient)
    {
        super((String) null);
        this.decoratedClient = decoratedClient;
    }

    

    // Factory methods

    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return MockMirrorApiUserClientMaker.getInstance();
    }



    // Override methods.
    //   Note the unusual "dual" delegation.

    @Override
    public Object get(String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockMirrorApiUserClient.get(): id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.get(id);
        } else {
            return super.get(id);
        }
    }
    @Override
    public List<Object> list(Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockMirrorApiUserClient.list(): params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.list(params);
        } else {
            return super.list(params);
        }
    }
    @Override
    public List<String> keys(Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockMirrorApiUserClient.keys(): params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.keys(params);
        } else {
            return super.keys(params);
        }
    }
    @Override
    public Object create(Object inputData) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockMirrorApiUserClient.create(): inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.create(inputData);
        } else {
            return super.create(inputData);
        }
    }
    @Override
    public Object create(Object inputData, String id) throws RestApiException,
            IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockMirrorApiUserClient.create(): inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.create(inputData, id);
        } else {
            return super.create(inputData, id);
        }
    }
    @Override
    public Object update(Object inputData, String id) throws RestApiException,
            IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockMirrorApiUserClient.update(): inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.update(inputData, id);
        } else {
            return super.update(inputData, id);
        }
    }
    @Override
    public Object modify(Object partialData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockMirrorApiUserClient.modify(): partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.modify(partialData, id);
        } else {
            return super.modify(partialData, id);
        }
    }
    @Override
    public boolean delete(String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockMirrorApiUserClient.delete(): id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.delete(id);
        } else {
            return super.delete(id);
        }
    }
    @Override
    public int delete(Map<String, Object> params) throws RestApiException,
            IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockMirrorApiUserClient.delete(): params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(params);
        } else {
            return super.delete(params);
        }
    }

    
    
    @Override
    public String toString()
    {
        return "MockMirrorApiUserClient [decoratedClient=" + decoratedClient
                + ", getApiServiceClient()=" + getApiServiceClient()
                + ", getUserCredential()=" + getUserCredential()
                + ", isAccessAllowed()=" + isAccessAllowed()
                + ", getResourceBaseUrl()=" + getResourceBaseUrl()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
