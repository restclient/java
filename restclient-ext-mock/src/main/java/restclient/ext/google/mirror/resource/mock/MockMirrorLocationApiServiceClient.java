package restclient.ext.google.mirror.resource.mock;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

import restclient.RestApiException;
import restclient.credential.UserCredential;
import restclient.exception.MethodNotAllowedRaException;
import restclient.ext.google.mirror.mock.MockMirrorApiServiceClient;
import restclient.ext.google.mirror.proxy.DecoratedMirrorApiServiceClient;
import restclient.ext.google.mirror.resource.impl.BaseMirrorLocationApiServiceClient;


// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockMirrorLocationApiServiceClient extends MockMirrorApiServiceClient implements DecoratedMirrorApiServiceClient
{
    private static final Logger log = Logger.getLogger(MockMirrorLocationApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public MockMirrorLocationApiServiceClient()
    {
        super(BaseMirrorLocationApiServiceClient.LOCATION_RESOURCE_BASE_URL);
    }

    
    // temporary

    @Override
    public Object create(UserCredential credential, Object inputData)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }

    @Override
    public Object create(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }

    @Override
    public Object update(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }

    @Override
    public boolean delete(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }

    @Override
    public int delete(UserCredential credential, Map<String, Object> params)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }



    @Override
    public String toString()
    {
        return "MockMirrorLocationApiServiceClient [getRestServiceClient()="
                + getRestServiceClient() + ", getCrudMethodFilter()="
                + getCrudMethodFilter() + ", getListResponseType()="
                + getListResponseType() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }

    
}
