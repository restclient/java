package restclient.ext.google.mirror.resource.mock;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.credential.UserCredential;
import restclient.ext.google.mirror.mock.MockMirrorApiUserClient;
import restclient.ext.google.mirror.proxy.DecoratedMirrorApiUserClient;
import restclient.ext.google.mirror.resource.impl.BaseMirrorSubscriptionApiServiceClient;


// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockMirrorSubscriptionApiUserClient extends MockMirrorApiUserClient implements DecoratedMirrorApiUserClient
{
    private static final Logger log = Logger.getLogger(MockMirrorSubscriptionApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    
    public MockMirrorSubscriptionApiUserClient()
    {
        this((UserCredential) null);
    }
    public MockMirrorSubscriptionApiUserClient(UserCredential userCredential)
    {
        super(BaseMirrorSubscriptionApiServiceClient.SUBSCRIPTION_RESOURCE_BASE_URL, userCredential);
    }

    public MockMirrorSubscriptionApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public MockMirrorSubscriptionApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    
    @Override
    public String toString()
    {
        return "MockMirrorSubscriptionApiUserClient [getApiServiceClient()="
                + getApiServiceClient() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
