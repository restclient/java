package restclient.ext;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ restclient.ext.google.factory.AllTests.class, restclient.ext.google.mirror.factory.manager.AllTests.class, restclient.ext.google.mirror.resource.AllTests.class })
public class AllTests
{

}
