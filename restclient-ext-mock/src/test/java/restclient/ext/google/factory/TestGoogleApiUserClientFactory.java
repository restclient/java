package restclient.ext.google.factory;

import java.io.IOException;

import restclient.ApiServiceClient;
import restclient.RestApiException;
import restclient.credential.UserCredential;
import restclient.ext.google.GoogleApiUserClient;
import restclient.ext.google.factory.mock.MockGoogleApiUserClientFactory;
import restclient.ext.google.mock.MockGoogleApiUserClient;
import restclient.mock.MockApiUserClient;


public class TestGoogleApiUserClientFactory extends
        MockGoogleApiUserClientFactory implements GoogleApiUserClientFactory
{

    public TestGoogleApiUserClientFactory()
    {
    }

    
    @Override
    public GoogleApiUserClient createGoogleApiUserClient(
            String resourceBaseUrl, UserCredential userCredential)
    {
        GoogleApiUserClient googleApiUserClient = super.createGoogleApiUserClient(resourceBaseUrl, userCredential);
        
        MockGoogleApiUserClient mockGoogleApiUserClient = new MockGoogleApiUserClient((MockApiUserClient) googleApiUserClient) {

            private static final long serialVersionUID = 1L;

            @Override
            public Object get(String id) throws RestApiException, IOException
            {
                // TBD: ...
                return super.get(id);
            }
            
        };
        
        return mockGoogleApiUserClient;
    }

    @Override
    public GoogleApiUserClient createGoogleApiUserClient(
            ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        // TODO Auto-generated method stub
        GoogleApiUserClient googleApiUserClient = super.createGoogleApiUserClient(apiServiceClient, userCredential);
        
        MockGoogleApiUserClient mockGoogleApiUserClient = new MockGoogleApiUserClient((MockApiUserClient) googleApiUserClient) {

            private static final long serialVersionUID = 1L;

            @Override
            public Object get(String id) throws RestApiException, IOException
            {
                // TBD: ...
                return super.get(id);
            }
            
        };
        
        return mockGoogleApiUserClient;
    }

    
}
