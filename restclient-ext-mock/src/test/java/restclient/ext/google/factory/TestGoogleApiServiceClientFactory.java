package restclient.ext.google.factory;

import java.io.IOException;

import restclient.ResourceClient;
import restclient.RestApiException;
import restclient.credential.UserCredential;
import restclient.ext.google.GoogleApiServiceClient;
import restclient.ext.google.factory.mock.MockGoogleApiServiceClientFactory;
import restclient.ext.google.mock.MockGoogleApiServiceClient;
import restclient.mock.MockApiServiceClient;



public class TestGoogleApiServiceClientFactory extends
        MockGoogleApiServiceClientFactory implements
        GoogleApiServiceClientFactory
{

    public TestGoogleApiServiceClientFactory()
    {
    }

    @Override
    public GoogleApiServiceClient createGoogleApiServiceClient(String resourceBaseUrl)
    {

        GoogleApiServiceClient googleApiServiceClient = super.createGoogleApiServiceClient(resourceBaseUrl);
        
        MockGoogleApiServiceClient mockGoogleApiServiceClient = new MockGoogleApiServiceClient((MockApiServiceClient) googleApiServiceClient) {

            private static final long serialVersionUID = 1L;

            @Override
            public Object get(UserCredential credential, String id)
                    throws RestApiException, IOException
            {
                // TBD: ...
                return super.get(credential, id);
            }
            
        };
        
        return mockGoogleApiServiceClient;
    }
    
    
}
