package restclient.ext.google.factory;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ GoogleApiServiceClientFactoryTest.class,
    GoogleApiUserClientFactoryTest.class })
public class AllTests
{

}
