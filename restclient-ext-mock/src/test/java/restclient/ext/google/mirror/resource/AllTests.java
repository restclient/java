package restclient.ext.google.mirror.resource;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MirrorContactApiUserClientTest.class,
    MirrorLocationApiUserClientTest.class,
        MirrorSubscriptionApiUserClientTest.class,
        MirrorTimelineApiUserClientTest.class,
        MirrorTimelineAttachmentApiUserClientTest.class })
public class AllTests
{

}
