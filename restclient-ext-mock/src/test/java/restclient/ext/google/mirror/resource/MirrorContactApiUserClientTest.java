package restclient.ext.google.mirror.resource;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import restclient.RestApiException;
import restclient.common.mock.MockUserCredential;
import restclient.credential.UserCredential;
import restclient.ext.google.mirror.MirrorApiUserClient;
import restclient.ext.google.mirror.factory.MirrorApiServiceClientFactory;
import restclient.ext.google.mirror.factory.MirrorApiUserClientFactory;
import restclient.ext.google.mirror.factory.manager.AbstractMirrorApiClientFactoryManager;
import restclient.ext.google.mirror.factory.mock.MockMirrorApiServiceClientFactory;


// Get OAuth2 access token from here: https://developers.google.com/oauthplayground/
// Scope:
// https://developers.google.com/glass/v1/reference/contacts#resource
public class MirrorContactApiUserClientTest
{
    private static final Logger log = Logger.getLogger(MirrorContactApiUserClientTest.class.getName());
    
    private MirrorApiUserClient contactApiUserClient;



    @Before
    public void setUp() throws Exception
    {
        AbstractMirrorApiClientFactoryManager abstractApiClientFactoryManager = AbstractMirrorApiClientFactoryManager.getInstance();
        
        MirrorApiServiceClientFactory mirrorApiServiceClientFactory = (MirrorApiServiceClientFactory) MockMirrorApiServiceClientFactory.getInstance();
        abstractApiClientFactoryManager.setMirrorApiClientFactories(mirrorApiServiceClientFactory);
        
        MirrorApiUserClientFactory mirrorApiUserClientFactory = abstractApiClientFactoryManager.getMirrorApiUserClientFactory();
        
        UserCredential userCredential = new MockUserCredential();
        contactApiUserClient = mirrorApiUserClientFactory.createMirrorContactApiUserClient(userCredential);
        // System.out.println("contactApiUserClient = " + contactApiUserClient);

        
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGet()
    {
        try {
            String id = "abc";
            Object contact = contactApiUserClient.get(id);
            System.out.println("contact = " + contact);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testList()
    {
        try {
            List<Object> list = contactApiUserClient.list(null);
            System.out.println("list = " + list);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // @Test
    public void testKeys()
    {
        try {
            List<String> keys = contactApiUserClient.keys(null);
            System.out.println("keys = " + keys);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCreateObject()
    {
        try {
            Object inputData = null;
            Object contact = contactApiUserClient.create(inputData);
            System.out.println("contact = " + contact);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdate()
    {
        try {
            Object inputData = null;
            String id = "abc";
            Object contact = contactApiUserClient.update(inputData, id);
            System.out.println("contact = " + contact);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteString()
    {
        try {
            String id = "abc";
            Object suc = contactApiUserClient.delete(id);
            System.out.println("suc = " + suc);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDeleteMapOfStringObject()
    {
        try {
            String id = null;
            Map<String,Object> params = new HashMap<String,Object>();
            Object count = contactApiUserClient.delete(params);
            System.out.println("count = " + count);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
