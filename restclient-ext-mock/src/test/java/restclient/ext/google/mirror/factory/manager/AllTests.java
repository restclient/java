package restclient.ext.google.mirror.factory.manager;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AbstractMirrorApiClientFactoryManagerTest.class })
public class AllTests
{

}
