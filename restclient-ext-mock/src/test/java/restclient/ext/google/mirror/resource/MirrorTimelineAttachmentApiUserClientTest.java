package restclient.ext.google.mirror.resource;

import static org.junit.Assert.fail;

import java.util.Set;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import restclient.core.AuthMethod;
import restclient.credential.UserCredential;
import restclient.credential.impl.AbstractUserCredential;
import restclient.ext.google.mirror.resource.impl.BaseMirrorTimelineAttachmentApiUserClient;


// Get OAuth2 access token from here: https://developers.google.com/oauthplayground/
// Scope:
// https://developers.google.com/glass/v1/reference/timeline/attachments#resource
public class MirrorTimelineAttachmentApiUserClientTest
{
    private static final Logger log = Logger.getLogger(MirrorTimelineAttachmentApiUserClientTest.class.getName());

    private BaseMirrorTimelineAttachmentApiUserClient timelineAttachmentApiUserClient;

    @Before
    public void setUp() throws Exception
    {
        String parentResourceId = "xxx";    // ???
        // String parentResourceId = "MTIxOTAxOTk1MTYwMDQ0MjA2NTY6NDQ0NTMwMTUyOjA";
        timelineAttachmentApiUserClient = new BaseMirrorTimelineAttachmentApiUserClient(parentResourceId);
        
        String user = "ab";
        String userId = "123";
        String authToken = "ya29.AHES6ZQNJ1TWlg40_qrIJ92CLzgG8oB4IDXacVSPrahBYehJ0rcfCZ5M";
        Set<String> dataScopes = null;
        Long expirationTime = null;
        UserCredential authCredential = new AbstractUserCredential(true, user, userId, AuthMethod.BEARER, authToken, null, dataScopes, expirationTime, null) {};
        timelineAttachmentApiUserClient.setUserCredential(authCredential);
        
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGet()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testList()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testKeys()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testCreateObject()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testCreateObjectString()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testUpdate()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testDeleteString()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testDeleteMapOfStringObject()
    {
        // fail("Not yet implemented");
    }

}
