package restclient.impl;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import restclient.RestServiceClient;
import restclient.core.AuthMethod;
import restclient.core.ContentFormat;
import restclient.credential.UserCredential;
import restclient.credential.impl.AbstractUserCredential;
import restclient.helper.ClientDebugHelper;



// -Djava.util.logging.config.file=logging.properties
public class AbstractRestServiceClientTest
{
    // private static final String resourceBaseUrl = "http://apiyard8.appspot.com/v1/counterThrees";   // beta
    // private static final String resourceBaseUrl = "http://apiyard1.appspot.com/v1/counterOnes";     // No auth
    // private static final String resourceBaseUrl = "http://apiyard5.appspot.com/v1/counterOnes";     // HTTP Basic
    private static final String resourceBaseUrl = "http://apiyard4.appspot.com/v1/counterOnes";     // OAuth2 bearer token. 
    private static final int timeoutSeconds = 17;
    private RestServiceClient restServiceClient;

    private UserCredential authCredential;


    @Before
    public void setUp() throws Exception
    {
        
        // temporary
        ClientDebugHelper.getInstance().setTraceEnabled(true);
        // ....
        
        restServiceClient = new AbstractRestServiceClient(resourceBaseUrl) {};
        ((AbstractRestServiceClient) restServiceClient).setAuthCredentialRequired(false);
        ((AbstractRestServiceClient) restServiceClient).setRequestFormat(ContentFormat.JSON);
        ((AbstractRestServiceClient) restServiceClient).setResponseFormat(ContentFormat.JSON);
        ((AbstractRestServiceClient) restServiceClient).setTimeoutSeconds(timeoutSeconds);
        
        ((AbstractRestServiceClient) restServiceClient).setAuthCredentialRequired(true);

        // authCredential = null;
        authCredential = new AbstractUserCredential() {
            private static final long serialVersionUID = 1L;
            
            protected void init()
            {
                super.init();
                setAuthRequired(true);
//                setAuthMethod(AuthMethod.BASIC);
//                setAuthKey("testuser");
//                // setAuthToken("wrongpass");
//                setAuthToken("testpass");
                setAuthMethod(AuthMethod.BEARER);
                setAuthToken("universalaccesstokenfortesting");
                // setAuthToken("wrongtoken");
                setExpirationTime(1376700000000L);
            }
        };

    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGetResourceBaseUrl()
    {
        String url = restServiceClient.getResourceBaseUrl();
        assertEquals(resourceBaseUrl, url);
    }

    @Test
    public void testGetResourceGetUrl()
    {
        String id = "123";
        String expectedUrl1 = resourceBaseUrl + "/" + id;
        String url1 = ((AbstractRestServiceClient) restServiceClient).getResourceGetUrl(id, null);
        assertEquals(expectedUrl1, url1);

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("count", 3);
        String expectedUrl2 = resourceBaseUrl + "?count=3";
        String url2 = ((AbstractRestServiceClient) restServiceClient).getResourceGetUrl(null, params);
        assertEquals(expectedUrl2, url2);

    }

    @Test
    public void testGetResourcePostUrl()
    {
        String expectedUrl = resourceBaseUrl;
        String url = ((AbstractRestServiceClient) restServiceClient).getResourcePostUrl();
        assertEquals(expectedUrl, url);
    }

    @Test
    public void testGetResourcePutUrl()
    {
        String id = "abc";
        String expectedUrl = resourceBaseUrl + "/" + id;
        String url = ((AbstractRestServiceClient) restServiceClient).getResourcePutUrl(id);
        assertEquals(expectedUrl, url);
    }

    @Test
    public void testGetResourceDeleteUrl()
    {
        String id = "xyz";
        String expectedUrl = resourceBaseUrl + "/" + id;
        String url = ((AbstractRestServiceClient) restServiceClient).getResourceDeleteUrl(id, null);
        assertEquals(expectedUrl, url);
    }

    @Test
    public void testGetInputFormat()
    {
        String inputFormat = ((AbstractRestServiceClient) restServiceClient).getRequestFormat();
        assertEquals(ContentFormat.JSON, inputFormat);
    }

    @Test
    public void testGetOutputFormat()
    {
        String outputFormat = ((AbstractRestServiceClient) restServiceClient).getResponseFormat();
        assertEquals(ContentFormat.JSON, outputFormat);
    }

    @Test
    public void testGetTimeoutSeconds()
    {
        int t = ((AbstractRestServiceClient) restServiceClient).getTimeoutSeconds();
        assertEquals(timeoutSeconds, t);
    }

    @Test
    public void testGet()
    {
        String id = "f63561b1-2a78-4758-82e4-6c45605925b3";
        
        Map<String,Object> response = null;
        try {
            response = restServiceClient.get(authCredential, id, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("response (1) = " + response);

        Map<String,Object> params = new HashMap<String,Object>();
        params.put("count", 3);
        try {
            response = restServiceClient.get(authCredential, null, params);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("response (2) = " + response);

    }

    @Test
    public void testPost()
    {
        Map<String,Object> response = null;
        Map<String,Object> inputData = new HashMap<String,Object>();
        inputData.put("user", "8fce61e4-b565-4537-9bf1-508a58e9aed6");
        inputData.put("name", "test counter instance 5");
        inputData.put("counter", 10);
        inputData.put("extra", "blah blah 5");
//        Map<String,String> option = new HashMap<>();
//        option.put("key", "k1");
//        option.put("value", "v1");
//        inputData.put("option", option);
//        // List<Object> attributes = new ArrayList<>();
//        List<Map<String,String>> attributes = new ArrayList<>();
//        Map<String,String> attr1 = new HashMap<>();
//        attr1.put("key", "k a1");
//        attr1.put("value", "v a1");
//        attributes.add(attr1);
//        Map<String,String> attr2 = new HashMap<>();
//        attr2.put("key", "k a2");
//        attr2.put("value", "v a2");
//        attributes.add(attr2);
//        inputData.put("attributes", attributes);
        
        try {
            response = restServiceClient.post(authCredential, inputData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("response (A) = " + response);

        
    }

    @Test
    public void testPut()
    {
        Map<String,Object> response = null;
        Map<String,Object> inputData = new HashMap<String,Object>();
        String id = "f63561b1-2a78-4758-82e4-6c45605925b3";
        inputData.put("user", "8fce61e4-b565-4537-9bf1-508a58e9aed6");
        inputData.put("guid", id);
        inputData.put("name", "test counter instance 10 modified");
        inputData.put("counter", 100);
        inputData.put("extra", "blah blah x 100");
//        Map<String,String> option = new HashMap<>();
//        option.put("key", "k1 mod");
//        option.put("value", "v1 mod");
//        inputData.put("option", option);
//        // List<Object> attributes = new ArrayList<>();
//        List<Map<String,String>> attributes = new ArrayList<>();
//        Map<String,String> attr1 = new HashMap<>();
//        attr1.put("key", "k a1 mod");
//        attr1.put("value", "v a1 mod");
//        attributes.add(attr1);
//        Map<String,String> attr2 = new HashMap<>();
//        attr2.put("key", "k a2 mod");
//        attr2.put("value", "v a2 mod");
//        attributes.add(attr2);
//        inputData.put("attributes", attributes);

        try {
            response = restServiceClient.put(authCredential, inputData, id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("response (U) = " + response);
    }

    @Test
    public void testDelete()
    {
        Map<String,Object> response = null;
        String id = "2eb671cc-f248-4a2e-a767-7f804a3d6995";
        
        try {
            response = restServiceClient.delete(authCredential, id, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("response (X) = " + response);

    }

}
