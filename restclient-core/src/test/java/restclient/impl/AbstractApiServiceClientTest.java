package restclient.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class AbstractApiServiceClientTest
{

    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGet()
    {
    }

    @Test
    public void testList()
    {
    }

    @Test
    public void testKeys()
    {
    }

    @Test
    public void testCreateAuthCredentialObject()
    {
    }

    @Test
    public void testCreateAuthCredentialObjectString()
    {
    }

    @Test
    public void testUpdate()
    {
    }

    @Test
    public void testDeleteAuthCredentialString()
    {
    }

    @Test
    public void testDeleteAuthCredentialMapOfStringObject()
    {
    }

}
