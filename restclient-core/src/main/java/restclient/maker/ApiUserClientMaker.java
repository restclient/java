package restclient.maker;

import restclient.ApiServiceClient;
import restclient.ApiUserClient;
import restclient.RestUserClient;


public interface ApiUserClientMaker
{
    // Factory methods
//    RestUserClient makeRestClient(String resourceBaseUrl);
    ApiServiceClient makeServiceClient(String resourceBaseUrl);
    // ApiServiceClient makeServiceClient(String parentResourceBaseUrl, String parentResourceId, String resourceName);

}
