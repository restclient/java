package restclient.maker;

import restclient.RestServiceClient;
import restclient.RestUserClient;


public interface RestUserClientMaker
{
    // Factory method
    RestServiceClient makeServiceClient(String resourceBaseUrl);
    // RestServiceClient makeServiceClient(String parentResourceBaseUrl, String parentResourceId, String resourceName);

}
