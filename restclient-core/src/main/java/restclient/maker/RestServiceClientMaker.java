package restclient.maker;

import restclient.common.DataAccessClient;
import restclient.common.HttpMethodFilter;
import restclient.common.ResourceUrlBuilder;


public interface RestServiceClientMaker extends ClientPolicyMaker
{
    // Factory methods

    ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl);
    // ResourceUrlBuilder makeResourceUrlBuilder(String parentResourceBaseUrl, String parentResourceId, String resourceName);

    HttpMethodFilter makeHttpMethodFilter();
    DataAccessClient makeDataAccessClient();

}
