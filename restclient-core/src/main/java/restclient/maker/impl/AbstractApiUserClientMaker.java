package restclient.maker.impl;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.impl.AbstractApiServiceClient;
import restclient.maker.ApiUserClientMaker;


// Abstract factory.
public abstract class AbstractApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(AbstractApiUserClientMaker.class.getName());


    protected AbstractApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static final class AbstractApiUserClientMakerHolder
    {
        private static final AbstractApiUserClientMaker INSTANCE = new AbstractApiUserClientMaker() {};
    }

    // Singleton method
    public static AbstractApiUserClientMaker getInstance()
    {
        return AbstractApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new AbstractApiServiceClient(resourceBaseUrl) {};
    }


    @Override
    public String toString()
    {
        return "AbstractApiUserClientMaker []";
    }


}
