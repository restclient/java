package restclient.maker.impl;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.impl.AbstractRestServiceClient;
import restclient.maker.RestUserClientMaker;


// Abstract factory.
public abstract class AbstractRestUserClientMaker implements RestUserClientMaker
{
    private static final Logger log = Logger.getLogger(AbstractRestUserClientMaker.class.getName());

    
    protected AbstractRestUserClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class AbstractRestUserClientMakerHolder
    {
        private static final AbstractRestUserClientMaker INSTANCE = new AbstractRestUserClientMaker() {};
    }

    // Singleton method
    public static AbstractRestUserClientMaker getInstance()
    {
        return AbstractRestUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new AbstractRestServiceClient(resourceBaseUrl) {};
    }


    @Override
    public String toString()
    {
        return "AbstractRestUserClientMaker []";
    }


}
