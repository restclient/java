package restclient.maker.impl;

import java.util.logging.Logger;

import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.DataAccessClient;
import restclient.common.HttpMethodFilter;
import restclient.common.RequestRetryPolicy;
import restclient.common.ResourceUrlBuilder;
import restclient.common.impl.AbstractAuthRefreshPolicy;
import restclient.common.impl.AbstractAutoRedirectPolicy;
import restclient.common.impl.AbstractCacheControlPolicy;
import restclient.common.impl.AbstractClientCachePolicy;
import restclient.common.impl.AbstractHttpMethodFilter;
import restclient.common.impl.AbstractRequestRetryPolicy;
import restclient.common.impl.AbstractResourceUrlBuilder;
import restclient.impl.AbstractDataAccessClient;
import restclient.maker.RestServiceClientMaker;


// Abstract factory.
public abstract class AbstractRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(AbstractRestServiceClientMaker.class.getName());


    protected AbstractRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class AbstractRestServiceClientMakerHolder
    {
        private static final AbstractRestServiceClientMaker INSTANCE = new AbstractRestServiceClientMaker() {};
    }

    // Singleton method
    public static AbstractRestServiceClientMaker getInstance()
    {
        return AbstractRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new AbstractResourceUrlBuilder(resourceBaseUrl) {};
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new AbstractHttpMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new AbstractAuthRefreshPolicy() {};
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new AbstractRequestRetryPolicy() {};
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new AbstractClientCachePolicy() {};
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new AbstractCacheControlPolicy() {};
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new AbstractAutoRedirectPolicy() {};
    }


    @Override
    public String toString()
    {
        return "AbstractRestServiceClientMaker []";
    }



}
