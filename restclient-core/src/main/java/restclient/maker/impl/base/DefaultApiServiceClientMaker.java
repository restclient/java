package restclient.maker.impl.base;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.CrudMethodFilter;
import restclient.common.DataAccessClient;
import restclient.common.RequestRetryPolicy;
import restclient.common.impl.base.DefaultAuthRefreshPolicy;
import restclient.common.impl.base.DefaultAutoRedirectPolicy;
import restclient.common.impl.base.DefaultCacheControlPolicy;
import restclient.common.impl.base.DefaultClientCachePolicy;
import restclient.common.impl.base.DefaultCrudMethodFilter;
import restclient.common.impl.base.DefaultRequestRetryPolicy;
import restclient.impl.base.DefaultDataAccessClient;
import restclient.impl.base.DefaultRestServiceClient;
import restclient.maker.ApiServiceClientMaker;


// Default factory.
public final class DefaultApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(DefaultApiServiceClientMaker.class.getName());


    protected DefaultApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class DefaultApiServiceClientMakerHolder
    {
        private static final DefaultApiServiceClientMaker INSTANCE = new DefaultApiServiceClientMaker();
    }

    // Singleton method
    public static DefaultApiServiceClientMaker getInstance()
    {
        return DefaultApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new DefaultRestServiceClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new DefaultCrudMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new DefaultDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new DefaultAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new DefaultRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new DefaultClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new DefaultCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new DefaultAutoRedirectPolicy();
    }

}
