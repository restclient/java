package restclient.maker.impl.base;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.impl.base.DefaultApiServiceClient;
import restclient.maker.ApiUserClientMaker;


// Default factory.
public final class DefaultApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(DefaultApiUserClientMaker.class.getName());


    protected DefaultApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static final class DefaultApiUserClientMakerHolder
    {
        private static final DefaultApiUserClientMaker INSTANCE = new DefaultApiUserClientMaker();
    }

    // Singleton method
    public static DefaultApiUserClientMaker getInstance()
    {
        return DefaultApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new DefaultApiServiceClient(resourceBaseUrl);
    }

    
}
