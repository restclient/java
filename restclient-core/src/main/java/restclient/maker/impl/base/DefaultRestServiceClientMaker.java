package restclient.maker.impl.base;

import java.util.logging.Logger;

import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.DataAccessClient;
import restclient.common.HttpMethodFilter;
import restclient.common.RequestRetryPolicy;
import restclient.common.ResourceUrlBuilder;
import restclient.common.impl.base.DefaultAuthRefreshPolicy;
import restclient.common.impl.base.DefaultAutoRedirectPolicy;
import restclient.common.impl.base.DefaultCacheControlPolicy;
import restclient.common.impl.base.DefaultClientCachePolicy;
import restclient.common.impl.base.DefaultHttpMethodFilter;
import restclient.common.impl.base.DefaultRequestRetryPolicy;
import restclient.common.impl.base.DefaultResourceUrlBuilder;
import restclient.impl.base.DefaultDataAccessClient;
import restclient.maker.RestServiceClientMaker;


// Default factory.
public final class DefaultRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(DefaultRestServiceClientMaker.class.getName());


    protected DefaultRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class DefaultRestServiceClientMakerHolder
    {
        private static final DefaultRestServiceClientMaker INSTANCE = new DefaultRestServiceClientMaker();
    }

    // Singleton method
    public static DefaultRestServiceClientMaker getInstance()
    {
        return DefaultRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new DefaultResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new DefaultHttpMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new DefaultDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new DefaultAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new DefaultRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new DefaultClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new DefaultCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new DefaultAutoRedirectPolicy();
    }


}
