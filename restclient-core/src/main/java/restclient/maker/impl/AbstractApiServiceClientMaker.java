package restclient.maker.impl;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.CrudMethodFilter;
import restclient.common.DataAccessClient;
import restclient.common.RequestRetryPolicy;
import restclient.common.impl.AbstractAuthRefreshPolicy;
import restclient.common.impl.AbstractAutoRedirectPolicy;
import restclient.common.impl.AbstractCacheControlPolicy;
import restclient.common.impl.AbstractClientCachePolicy;
import restclient.common.impl.AbstractCrudMethodFilter;
import restclient.common.impl.AbstractRequestRetryPolicy;
import restclient.impl.AbstractDataAccessClient;
import restclient.impl.AbstractRestServiceClient;
import restclient.maker.ApiServiceClientMaker;


// Abstract factory.
public abstract class AbstractApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(AbstractApiServiceClientMaker.class.getName());


    protected AbstractApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class AbstractApiServiceClientMakerHolder
    {
        private static final AbstractApiServiceClientMaker INSTANCE = new AbstractApiServiceClientMaker() {};
    }

    // Singleton method
    public static AbstractApiServiceClientMaker getInstance()
    {
        return AbstractApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new AbstractRestServiceClient(resourceBaseUrl) {};
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new AbstractCrudMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new AbstractAuthRefreshPolicy() {};
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new AbstractRequestRetryPolicy() {};
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new AbstractClientCachePolicy() {};
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new AbstractCacheControlPolicy() {};
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new AbstractAutoRedirectPolicy() {};
    }


    @Override
    public String toString()
    {
        return "AbstractApiServiceClientMaker []";
    }


}
