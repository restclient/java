package restclient.maker;

import restclient.RestServiceClient;
import restclient.common.CrudMethodFilter;
import restclient.common.DataAccessClient;


public interface ApiServiceClientMaker extends ClientPolicyMaker
{
    // Factory methods

    RestServiceClient makeRestClient(String resourceBaseUrl);
    // RestServiceClient makeRestClient(String parentResourceBaseUrl, String parentResourceId, String resourceName);

    CrudMethodFilter makeCrudMethodFilter();
    DataAccessClient makeDataAccessClient();

}
