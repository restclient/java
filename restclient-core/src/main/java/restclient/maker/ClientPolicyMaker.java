package restclient.maker;

import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.RequestRetryPolicy;


public interface ClientPolicyMaker
{
    // Factory methods

    AuthRefreshPolicy makeAuthRefreshPolicy();
    RequestRetryPolicy makeRequestRetryPolicy();
    ClientCachePolicy makeClientCachePolicy();
    CacheControlPolicy makeCacheControlPolicy();
    AutoRedirectPolicy makeAutoRedirectPolicy();

}
