package restclient.factory;

import restclient.maker.ApiServiceClientMaker;


public interface ApiServiceClientFactory extends ClientFactory, ApiServiceClientMaker
{
    // ????
    // ApiServiceClient createClient();

    // ???
    ApiUserClientFactory createApiUserClientFactory();

}
