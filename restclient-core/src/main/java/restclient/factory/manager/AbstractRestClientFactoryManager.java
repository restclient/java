package restclient.factory.manager;

import java.util.logging.Logger;

import restclient.factory.RestServiceClientFactory;
import restclient.factory.RestUserClientFactory;
import restclient.factory.impl.base.DefaultRestServiceClientFactory;


/**
 * This manager class uses the "Abstract Factory" pattern.
 * Client can inherit from this class to return a proper concrete factory(s).
 */
public abstract class AbstractRestClientFactoryManager
{
    private static final Logger log = Logger.getLogger(AbstractRestClientFactoryManager.class.getName());

    private RestServiceClientFactory restServiceClientFactory;
    private RestUserClientFactory restUserClientFactory;
    
    protected AbstractRestClientFactoryManager() 
    {
        restServiceClientFactory = DefaultRestServiceClientFactory.getInstance();
        // restUserClientFactory = DefaultRestUserClientFactory.getInstance();
        restUserClientFactory = restServiceClientFactory.createRestUserClientFactory();
    }

    // Initialization-on-demand holder.
    private static final class AbstractRestClientFactoryManagerHolder
    {
        private static final AbstractRestClientFactoryManager INSTANCE = new AbstractRestClientFactoryManager() {};
    }

    // Singleton method
    public static AbstractRestClientFactoryManager getInstance()
    {
        return AbstractRestClientFactoryManagerHolder.INSTANCE;
    }

    // Note:
    // Client should override these methods, or the constructor, to return appropriate concrete factories.
    
    // Returns a service client factory.
    public RestServiceClientFactory getRestServiceClientFactory()
    {
        return restServiceClientFactory;
    }

    // Returns a user client factory.
    public RestUserClientFactory getRestUserClientFactory()
    {
        return restUserClientFactory;
    }


    // TBD:
    // Setters to inject factories, if needed.
    // ....
    
    public void setRestClientFactories(RestServiceClientFactory restServiceClientFactory)
    {
        setRestClientFactories(restServiceClientFactory, null);
    }
    public void setRestClientFactories(RestServiceClientFactory restServiceClientFactory, RestUserClientFactory restUserClientFactory)
    {
        if(restServiceClientFactory != null) {
            this.restServiceClientFactory = restServiceClientFactory;
            if(restUserClientFactory != null) {
                this.restUserClientFactory = restUserClientFactory;
            } else {
                this.restUserClientFactory = restServiceClientFactory.createRestUserClientFactory();
            }
        } else {
            // ???
            log.info("Input restServiceClientFactory is null. Both restServiceClientFactory and restUserClientFactory will be ignored.");
        }

    }


}
