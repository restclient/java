package restclient.factory;

import restclient.maker.RestUserClientMaker;


public interface RestUserClientFactory extends ClientFactory, RestUserClientMaker
{
    // ????
    // RestUserClient createClient();

    // ???
    // RestServiceClientFactory createRestServiceClientFactory();
    
}
