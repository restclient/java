package restclient.factory;

import restclient.ResourceClient;


public interface ClientFactory
{
    // All "factory" can create a client of a concrete type.
    // Follows the "factory method" design pattern.
    ResourceClient createClient(String resourceBaseUrl);
    
}
