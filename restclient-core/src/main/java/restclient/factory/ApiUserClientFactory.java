package restclient.factory;

import restclient.maker.ApiUserClientMaker;


public interface ApiUserClientFactory extends ClientFactory, ApiUserClientMaker
{
    // ????
    // ApiUserClient createClient();

    // ???
    // ApiServiceClientFactory createApiServiceClientFactory();

}
