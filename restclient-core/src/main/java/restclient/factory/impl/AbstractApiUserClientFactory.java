package restclient.factory.impl;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ApiUserClient;
import restclient.ResourceClient;
import restclient.factory.ApiUserClientFactory;
import restclient.factory.ClientFactory;
import restclient.impl.AbstractApiUserClient;
import restclient.maker.ApiUserClientMaker;
import restclient.maker.impl.AbstractApiUserClientMaker;


public abstract class AbstractApiUserClientFactory implements ApiUserClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(AbstractApiUserClientFactory.class.getName());


    // Abstract factory.
    private ApiUserClientMaker apiUserClientMaker;

    protected AbstractApiUserClientFactory()
    {
        apiUserClientMaker = makeApiUserClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class AbstractApiUserClientFactoryHolder
    {
        private static final AbstractApiUserClientFactory INSTANCE = new AbstractApiUserClientFactory() {};
    }

    // Singleton method
    public static AbstractApiUserClientFactory getInstance()
    {
        return AbstractApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new AbstractApiUserClient(resourceBaseUrl) {};
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return AbstractApiUserClientMaker.getInstance();
    }



    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiUserClient(resourceBaseUrl);
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return apiUserClientMaker.makeServiceClient(resourceBaseUrl);
    }


    @Override
    public String toString()
    {
        return "AbstractApiUserClientFactory [apiUserClientMaker="
                + apiUserClientMaker + "]";
    }


}
