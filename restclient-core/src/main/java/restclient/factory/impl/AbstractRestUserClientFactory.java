package restclient.factory.impl;

import java.util.logging.Logger;

import restclient.ResourceClient;
import restclient.RestServiceClient;
import restclient.RestUserClient;
import restclient.factory.ClientFactory;
import restclient.factory.RestUserClientFactory;
import restclient.impl.AbstractRestUserClient;
import restclient.maker.RestUserClientMaker;
import restclient.maker.impl.AbstractRestUserClientMaker;


public abstract class AbstractRestUserClientFactory implements RestUserClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(AbstractRestUserClientFactory.class.getName());


    // Abstract factory.
    private RestUserClientMaker restUserClientMaker;

    protected AbstractRestUserClientFactory()
    {
        restUserClientMaker = makeRestUserClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class AbstractRestUserClientFactoryHolder
    {
        private static final AbstractRestUserClientFactory INSTANCE = new AbstractRestUserClientFactory() {};
    }

    // Singleton method
    public static AbstractRestUserClientFactory getInstance()
    {
        return AbstractRestUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected RestUserClient makeRestUserClient(String resourceBaseUrl)
    {
        return new AbstractRestUserClient(resourceBaseUrl) {};
    }
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return AbstractRestUserClientMaker.getInstance();
    }


    
    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestUserClient(resourceBaseUrl);
    }

    @Override
    public RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return restUserClientMaker.makeServiceClient(resourceBaseUrl);
    }


    @Override
    public String toString()
    {
        return "AbstractRestUserClientFactory [restUserClientMaker="
                + restUserClientMaker + "]";
    }

 
}
