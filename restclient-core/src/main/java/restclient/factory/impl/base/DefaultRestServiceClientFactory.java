package restclient.factory.impl.base;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.factory.impl.AbstractRestServiceClientFactory;
import restclient.impl.base.DefaultRestServiceClient;
import restclient.maker.RestServiceClientMaker;
import restclient.maker.impl.base.DefaultRestServiceClientMaker;


public final class DefaultRestServiceClientFactory extends
        AbstractRestServiceClientFactory
{
    private static final Logger log = Logger.getLogger(DefaultRestServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class DefaultRestServiceClientFactoryHolder
    {
        private static final DefaultRestServiceClientFactory INSTANCE = new DefaultRestServiceClientFactory();
    }

    // Singleton method
    public static DefaultRestServiceClientFactory getInstance()
    {
        return DefaultRestServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected RestServiceClient makeRestServiceClient(String resourceBaseUrl)
    {
        return new DefaultRestServiceClient(resourceBaseUrl);
    }
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return DefaultRestServiceClientMaker.getInstance();
    }

    
}
