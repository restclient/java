package restclient.factory.impl.base;

import java.util.logging.Logger;

import restclient.RestUserClient;
import restclient.factory.impl.AbstractRestUserClientFactory;
import restclient.impl.base.DefaultRestUserClient;
import restclient.maker.RestUserClientMaker;
import restclient.maker.impl.base.DefaultRestUserClientMaker;


public final class DefaultRestUserClientFactory extends
        AbstractRestUserClientFactory
{
    private static final Logger log = Logger.getLogger(DefaultRestUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class DefaultRestUserClientFactoryHolder
    {
        private static final DefaultRestUserClientFactory INSTANCE = new DefaultRestUserClientFactory();
    }

    // Singleton method
    public static DefaultRestUserClientFactory getInstance()
    {
        return DefaultRestUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected RestUserClient makeRestUserClient(String resourceBaseUrl)
    {
        return new DefaultRestUserClient(resourceBaseUrl);
    }
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return DefaultRestUserClientMaker.getInstance();
    }

    
}
