package restclient.factory.impl.base;

import java.util.logging.Logger;

import restclient.ApiUserClient;
import restclient.factory.impl.AbstractApiUserClientFactory;
import restclient.impl.base.DefaultApiUserClient;
import restclient.maker.ApiUserClientMaker;
import restclient.maker.impl.base.DefaultApiUserClientMaker;


public final class DefaultApiUserClientFactory extends
        AbstractApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(DefaultApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class DefaultApiUserClientFactoryHolder
    {
        private static final DefaultApiUserClientFactory INSTANCE = new DefaultApiUserClientFactory();
    }

    // Singleton method
    public static DefaultApiUserClientFactory getInstance()
    {
        return DefaultApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new DefaultApiUserClient(resourceBaseUrl);
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return DefaultApiUserClientMaker.getInstance();
    }

    
}
