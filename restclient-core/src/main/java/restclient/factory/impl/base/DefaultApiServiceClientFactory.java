package restclient.factory.impl.base;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.factory.impl.AbstractApiServiceClientFactory;
import restclient.impl.base.DefaultApiServiceClient;
import restclient.maker.ApiServiceClientMaker;
import restclient.maker.impl.base.DefaultApiServiceClientMaker;


public final class DefaultApiServiceClientFactory extends
        AbstractApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(DefaultApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class DefaultApiServiceClientFactoryHolder
    {
        private static final DefaultApiServiceClientFactory INSTANCE = new DefaultApiServiceClientFactory();
    }

    // Singleton method
    public static DefaultApiServiceClientFactory getInstance()
    {
        return DefaultApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new DefaultApiServiceClient(resourceBaseUrl);
    }
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return DefaultApiServiceClientMaker.getInstance();
    }

    
}
