package restclient.factory.impl;

import java.util.logging.Logger;

import restclient.ResourceClient;
import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.DataAccessClient;
import restclient.common.HttpMethodFilter;
import restclient.common.RequestRetryPolicy;
import restclient.common.ResourceUrlBuilder;
import restclient.factory.ClientFactory;
import restclient.factory.RestServiceClientFactory;
import restclient.factory.RestUserClientFactory;
import restclient.impl.AbstractRestServiceClient;
import restclient.maker.RestServiceClientMaker;
import restclient.maker.impl.AbstractRestServiceClientMaker;


public abstract class AbstractRestServiceClientFactory implements RestServiceClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(AbstractRestServiceClientFactory.class.getName());

    // Abstract factory.
    private RestServiceClientMaker restServiceClientMaker;

    protected AbstractRestServiceClientFactory()
    {
        restServiceClientMaker = makeRestServiceClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class AbstractRestServiceClientFactoryHolder
    {
        private static final AbstractRestServiceClientFactory INSTANCE = new AbstractRestServiceClientFactory() {};
    }

    // Singleton method
    public static AbstractRestServiceClientFactory getInstance()
    {
        return AbstractRestServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected RestServiceClient makeRestServiceClient(String resourceBaseUrl)
    {
        return new AbstractRestServiceClient(resourceBaseUrl) {};
    }
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return AbstractRestServiceClientMaker.getInstance();
    }
    protected RestUserClientFactory makeRestUserClientFactory()
    {
        return AbstractRestUserClientFactory.getInstance();
    }


    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestServiceClient(resourceBaseUrl);
    }

    @Override
    public RestUserClientFactory createRestUserClientFactory()
    {
        return makeRestUserClientFactory();
    }


    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return restServiceClientMaker.makeResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return restServiceClientMaker.makeHttpMethodFilter();
    }


    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return restServiceClientMaker.makeDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return restServiceClientMaker.makeAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return restServiceClientMaker.makeRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return restServiceClientMaker.makeClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return restServiceClientMaker.makeCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return restServiceClientMaker.makeAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "AbstractRestServiceClientFactory [restServiceClientMaker="
                + restServiceClientMaker + "]";
    }


}
