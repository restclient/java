package restclient.factory;

import restclient.maker.RestServiceClientMaker;



public interface RestServiceClientFactory extends ClientFactory, RestServiceClientMaker
{
    // Factory method
    // RestServiceClient createClient();

    // ???
    RestUserClientFactory createRestUserClientFactory();

}
