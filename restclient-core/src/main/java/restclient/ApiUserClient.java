package restclient;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import restclient.common.CrudMethodFilter;


/**
 * API Client. User-specific.
 *     (That is, these instances are not shared among different users.)
 *
 */
public interface ApiUserClient extends ResourceUserClient, ApiClient, CrudMethodFilter
{
    // Returns the "embedded" apiServiceClient.
    // ApiServiceClient getApiServiceClient();

    /**
     * Returns the resource specified by the id.
     * @param id Unique id of the resource.
     * @return The resource/object.
     * @throws RestApiException
     * @throws IOException
     */
    Object get(String id) throws RestApiException, IOException;

    /**
     * Returns the list of objects/resources specified by the params.
     * @param params Selection filter.
     * @return The list of resource instances.
     * @throws RestApiException
     * @throws IOException
     */
    List<Object> list(Map<String,Object> params) throws RestApiException, IOException;

    /**
     * Returns the list of keys of the resources filtered by the params.
     * @param params Selection filter.
     * @return The list of keys/ids of the resources selected by the params.
     * @throws RestApiException
     * @throws IOException
     */
    List<String> keys(Map<String,Object> params) throws RestApiException, IOException;

    /**
     * Creates a new instance of the given resource, as defined by the inputData.
     * @param inputData A representation of the resource instance to be created.
     * @return The newly created resource object.
     * @throws RestApiException
     * @throws IOException
     */
    Object create(Object inputData) throws RestApiException, IOException;

    /**
     * Creates a new instance of the given resource, as defined by the inputData and id.
     * @param inputData A representation of the resource instance to be created.
     * @param id The unique resource id.
     * @return The newly created resource object.
     * @throws RestApiException
     * @throws IOException
     */
    Object create(Object inputData, String id) throws RestApiException, IOException;

    /**
     * Updates the resource instance, as defined by the inputData and id.
     * @param inputData A new resource instance.
     * @param id The resource id.
     * @return The updated resource object.
     * @throws RestApiException
     * @throws IOException
     */
    Object update(Object inputData, String id) throws RestApiException, IOException;

    /**
     * Note: Partial update/PATCH support...
     *       Not all Web services support PATCH.
     * (Not fully implemented yet....)
     * Updates the resource instance, as defined by the partialtData and id.
     * @param partialtData A "partial" representation of the new resource instance.
     * @param id The resource id.
     * @return The modified resource object. (TBD: Are we returning the partial object? Or, the full object?)
     * @throws RestApiException
     * @throws IOException
     */
    Object modify(Object partialtData, String id) throws RestApiException, IOException;

    /**
     * Deletes the resource instance specified by id.
     * @param id The resource id.
     * @return true if the operation is successful.
     * @throws RestApiException
     * @throws IOException
     */
    boolean delete(String id) throws RestApiException, IOException;

    /**
     * Deletes the resource instances specified by the params.
     * (Not fully implemented yet....)
     * @param params
     * @return The number of resource objects successfully deleted. (TBD: Is it possible to do that in general? Just return boolean?)
     * @throws RestApiException
     * @throws IOException
     */
    int delete(Map<String,Object> params) throws RestApiException, IOException;

}
