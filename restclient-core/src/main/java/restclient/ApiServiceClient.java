package restclient;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import restclient.common.CrudMethodFilter;
import restclient.credential.UserCredential;


/**
 * "High-level" API (compared to that of "REST Client").
 * This API uses CRUD verbs. (On the other hand, REST client uses HTTP method verbs.)
 *
 */
public interface ApiServiceClient extends ResourceServiceClient, ApiClient, CrudMethodFilter
{
    // TBD: 
    //      Throw RestApiException ?????
    // ..

    /**
     * 
     * @param credential
     * @param id
     * @return
     * @throws RestApiException TODO
     * @throws IOException TODO
     */
    Object get(UserCredential credential, String id) throws RestApiException, IOException;

    /**
     * 
     * @param credential
     * @param params
     * @return
     * @throws RestApiException TODO
     * @throws IOException TODO
     */
    List<Object> list(UserCredential credential, Map<String,Object> params) throws RestApiException, IOException;
    
    /**
     * 
     * @param credential
     * @param params
     * @return
     * @throws RestApiException TODO
     * @throws IOException TODO
     */
    List<String> keys(UserCredential credential, Map<String,Object> params) throws RestApiException, IOException;
    
    /**
     * 
     * @param credential
     * @param inputData
     * @return
     * @throws RestApiException TODO
     * @throws IOException TODO
     */
    Object create(UserCredential credential, Object inputData) throws RestApiException, IOException;
    
    /**
     * 
     * @param credential
     * @param inputData
     * @param id
     * @return
     * @throws RestApiException TODO
     * @throws IOException TODO
     */
    Object create(UserCredential credential, Object inputData, String id) throws RestApiException, IOException;
    
    /**
     * 
     * @param credential
     * @param inputData
     * @param id
     * @return
     * @throws RestApiException TODO
     * @throws IOException TODO
     */
    Object update(UserCredential credential, Object inputData, String id) throws RestApiException, IOException;
    
    /**
     * Partial update/PATCH support
     * 
     * @param credential
     * @param partialData
     * @param id
     * @return
     * @throws RestApiException
     * @throws IOException
     */
    Object modify(UserCredential credential, Object partialData, String id) throws RestApiException, IOException;
    
    /**
     * 
     * @param credential
     * @param id
     * @return
     * @throws RestApiException TODO
     * @throws IOException TODO
     */
    boolean delete(UserCredential credential, String id) throws RestApiException, IOException;

    /**
     * 
     * @param credential
     * @param params
     * @return
     * @throws RestApiException TODO
     * @throws IOException TODO
     */
    int delete(UserCredential credential, Map<String,Object> params) throws RestApiException, IOException;
}
