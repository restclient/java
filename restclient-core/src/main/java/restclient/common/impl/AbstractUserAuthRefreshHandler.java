package restclient.common.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.common.UserAuthRefreshHandler;
import restclient.credential.UserCredential;


// Place holder...
public abstract class AbstractUserAuthRefreshHandler implements UserAuthRefreshHandler, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractAuthRefreshPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    public AbstractUserAuthRefreshHandler()
    {
    }

    @Override
    public boolean isImeplemented()
    {
        return false;
    }

    @Override
    public UserCredential refreshAuthToken(UserCredential userCredential)
    {
        // TODO:
        // return userCredential;
        return null;
    }


    @Override
    public String toString()
    {
        return "AbstractUserAuthRefreshHandler []";
    }

}
