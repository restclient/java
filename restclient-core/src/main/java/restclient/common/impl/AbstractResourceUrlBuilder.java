package restclient.common.impl;

import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.common.ResourceUrlBuilder;
import restclient.util.ResourceUrlUtil;


// Default implementation.
// Base URL: http://base/
// GET URL: http://base/id, http://base/id?k=v, or http://base/?k=v
// POST URL: http://base/
// PUT URL: http://base/id
// DELETE URL: http://base/id, or http://base/?k=v
public abstract class AbstractResourceUrlBuilder implements ResourceUrlBuilder, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractResourceUrlBuilder.class.getName());
    private static final long serialVersionUID = 1L;

    // This "defines" a resource.
    private String resourceBaseUrl;

    // ???
    // This triplet can be used to set resourceBaseUrl.
    // All three need to be specified.
    private String parentResourceBaseUrl;
    private String parentResourceId;
    private String resourceName;
    // private boolean hasParent;     // This is set to true, if this triplet is used.

    // ???
    // If this is non-null/empty, then the suffix is added at the end (with ".").
    private String suffix;


    public AbstractResourceUrlBuilder(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public AbstractResourceUrlBuilder(String resourceBaseUrl, String suffix)
    {
        super();
        if(resourceBaseUrl != null) {
            // TBD: Validation?
            this.resourceBaseUrl = resourceBaseUrl;
        } else {
            // What to do???
            log.warning("Invalid resourceBaseUrl.");
            this.resourceBaseUrl = "/";     // ?????            
        }

        // set/reset the flag
        // this.hasParent = false;
        
        this.suffix = suffix;

        // TBD:
        init();
    }

    public AbstractResourceUrlBuilder(String parentResourceBaseUrl, String parentResourceId, String resourceName)
    {
        this(parentResourceBaseUrl, parentResourceId, resourceName, null);
    }
    public AbstractResourceUrlBuilder(String parentResourceBaseUrl, String parentResourceId, String resourceName, String suffix)
    {
        super();
    
        // TBD:
        // Parent's parent's parent's parent.... ???
        // ...
        
        // TBD: validation?
        // parentResourceId can be null, but not the others.
        this.parentResourceBaseUrl = parentResourceBaseUrl;
        this.parentResourceId = parentResourceId;
        this.resourceName = resourceName;

        // set/reset the flag
        // this.hasParent = true;
        this.resourceBaseUrl = buildBaseUrl(parentResourceBaseUrl, parentResourceId, resourceName);

        // TBD:
        init();
    }

    
    protected void init()
    {
        // Place holder
    }


    // It's kind of cumbersome,
    // but if you want the baseUrl to end with "/", then include it in resourceName.
    public static String buildBaseUrl(String parentResourceBaseUrl, String parentResourceId, String resourceName)
    {
        // Hack
        String url1 = ResourceUrlUtil.buildUrl(parentResourceBaseUrl, parentResourceId);
        String url2 = ResourceUrlUtil.buildUrl(url1, resourceName);
        return url2;
    }
    

    
    @Override
    public String getResourceBaseUrl()
    {
        return resourceBaseUrl;
    }
    public void setResourceBaseUrl(String resourceBaseUrl)
    {
        this.resourceBaseUrl = resourceBaseUrl;

        // set/reset the flag
        // this.hasParent = false;
        
        // TBD:
        // once resourceBaseUrl is set explicitly,
        // {parentResourceBaseUrl, parentResourceId, resourceName} triplet can no longer be used...
        // How to do this????
    }


    public String getParentResourceBaseUrl()
    {
        return parentResourceBaseUrl;
    }
    public void setParentResourceBaseUrl(String parentResourceBaseUrl)
    {
        this.parentResourceBaseUrl = parentResourceBaseUrl;

        // set/reset the flag
        // this.hasParent = true;
        this.resourceBaseUrl = buildBaseUrl(parentResourceBaseUrl, parentResourceId, resourceName);
    }

    public String getParentResourceId()
    {
        return parentResourceId;
    }
    public void setParentResourceId(String parentResourceId)
    {
        this.parentResourceId = parentResourceId;

        // set/reset the flag
        // this.hasParent = true;
        this.resourceBaseUrl = buildBaseUrl(parentResourceBaseUrl, parentResourceId, resourceName);
    }

    public String getResourceName()
    {
        return resourceName;
    }
    public void setResourceName(String resourceName)
    {
        this.resourceName = resourceName;

        // set/reset the flag
        // this.hasParent = true;
        this.resourceBaseUrl = buildBaseUrl(parentResourceBaseUrl, parentResourceId, resourceName);
    }

    public String getSuffix()
    {
        return suffix;
    }
    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
    }


    // "Main" methods
    
    public String getResourceGetUrl(String id)
    {
        return getResourceGetUrl(id, null);
    }
    public String getResourceGetUrl(Map<String,Object> params)
    {
        return getResourceGetUrl(null, params);
    }
    @Override
    public String getResourceGetUrl(String id, Map<String,Object> params)
    {
        return buildResourceGetUrl(resourceBaseUrl, id, params, suffix);
    }
    public static String buildResourceGetUrl(String baseUrl, String id, Map<String,Object> params, String sufx)
    {
        String url = null;
        url = ResourceUrlUtil.buildUrl(baseUrl, id, sufx, params);
        if(log.isLoggable(Level.FINER)) log.finer("url = " + url);
        return url;
    }

    @Override
    public String getResourcePostUrl()
    {
        return buildResourcePostUrl(resourceBaseUrl, suffix);
    }
    public static String buildResourcePostUrl(String baseUrl, String sufx)
    {
        String url = ResourceUrlUtil.buildUrl(baseUrl, null, sufx);
        if(log.isLoggable(Level.FINER)) log.finer("url = " + url);
        return url;
    }

    @Override
    public String getResourcePutUrl(String id)
    {
        return buildResourcePutUrl(resourceBaseUrl, id, suffix);
    }
    public static String buildResourcePutUrl(String baseUrl, String id, String sufx)
    {
        String url = ResourceUrlUtil.buildUrl(baseUrl, id, sufx);
        if(log.isLoggable(Level.FINER)) log.finer("url = " + url);
        return url;
    }

    @Override
    public String getResourcePatchUrl(String id)
    {
        return buildResourcePatchUrl(resourceBaseUrl, id, suffix);
    }
    public static String buildResourcePatchUrl(String baseUrl, String id, String sufx)
    {
        String url = ResourceUrlUtil.buildUrl(baseUrl, id, sufx);
        if(log.isLoggable(Level.FINER)) log.finer("url = " + url);
        return url;
    }

    public String getResourceDeleteUrl(String id)
    {
        return getResourceDeleteUrl(id, null);
    }
    public String getResourceDeleteUrl(Map<String,Object> params)
    {
        return getResourceDeleteUrl(null, params);
    }
    @Override
    public String getResourceDeleteUrl(String id, Map<String,Object> params)
    {
        return buildResourceDeleteUrl(resourceBaseUrl, id, params, suffix);
    }
    public static String buildResourceDeleteUrl(String baseUrl, String id, Map<String,Object> params, String sufx)
    {
        String url = null;
        // Id or params, but not both for delete().
        // url = ResourceUrlUtil.buildUrl(resourceBaseUrl, id, params);
        if(id != null) {
            url = ResourceUrlUtil.buildUrl(baseUrl, id, sufx);
        } else {
            url = ResourceUrlUtil.buildUrl(baseUrl, null, sufx, params);
        }
        if(log.isLoggable(Level.FINER)) log.finer("url = " + url);
        return url;
    }

    
    @Override
    public String toString()
    {
        return "AbstractResourceUrlBuilder [resourceBaseUrl=" + resourceBaseUrl
                + ", parentResourceBaseUrl=" + parentResourceBaseUrl
                + ", parentResourceId=" + parentResourceId + ", resourceName="
                + resourceName + ", suffix=" + suffix + "]";
    }

}
