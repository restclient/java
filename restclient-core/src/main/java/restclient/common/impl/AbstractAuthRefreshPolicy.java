package restclient.common.impl;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.RestApiException;
import restclient.common.AuthRefreshPolicy;
import restclient.common.UserAuthRefreshHandler;
import restclient.common.exception.RestApiPolicyException;
import restclient.credential.UserCredential;


// Auth Refresh policy
public abstract class AbstractAuthRefreshPolicy implements AuthRefreshPolicy, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractAuthRefreshPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    private boolean refreshBeforeRequest;
    private boolean refreshIfFails;
    private int futureMarginSecs;
    
    private UserAuthRefreshHandler authRefreshHandler;


    public AbstractAuthRefreshPolicy() 
    {
        refreshBeforeRequest = false;
        refreshIfFails = false;
        futureMarginSecs = 0;    // ???
        
        // authRefreshHandler = null;    // ??
        authRefreshHandler = makeUserAuthRefreshHandler();

        // TBD:
        init();
    }
    
    protected void init()
    {
        // Place holder
    }
    
    
    // Factory method.
    // Is this necessary???
    protected UserAuthRefreshHandler makeUserAuthRefreshHandler()
    {
        return new AbstractUserAuthRefreshHandler() {};
    }


    @Override
    public boolean isRefreshBeforeRequest()
    {
        return refreshBeforeRequest;
    }
    public void setRefreshBeforeRequest(boolean refreshBeforeRequest)
    {
        this.refreshBeforeRequest = refreshBeforeRequest;
    }


    @Override
    public boolean isRefreshIfFails()
    {
        return refreshIfFails;
    }
    public void setRefreshIfFails(boolean refreshIfFails)
    {
        this.refreshIfFails = refreshIfFails;
    }


    @Override
    public int getFutureMarginSeconds()
    {
        return futureMarginSecs;
    }
    public void setFutureMarginSeconds(int futureMarginSecs)
    {
        this.futureMarginSecs = futureMarginSecs;
    }


    @Override
    public UserAuthRefreshHandler getAuthRefreshHandler()
    {
        return authRefreshHandler;
    }
    // @Override
    public void setAuthRefreshHandler(UserAuthRefreshHandler authRefreshHandler)
    {
        this.authRefreshHandler = authRefreshHandler;
    }


    @Override
    public boolean isPerformable()
    {
        if(authRefreshHandler == null || authRefreshHandler.isImeplemented() == false) {
            return false;
        }
        return true;
    }

    @Override
    public Object perform(Object... args) throws RestApiException
    {
        log.fine("BEGIN: perform()");

        // Refresh userAuth....
        // ...
        
        if(authRefreshHandler == null) {
            log.info("authRefreshHandler is null. perform() not performed.");
            // return null;
            throw new RestApiPolicyException("authRefreshHandler is null. perform() not performed.");
        }
        if(authRefreshHandler.isImeplemented() == false) {
            log.info("authRefreshHandler is not implemented. perform() not performed.");
            // return null;
            throw new RestApiPolicyException("authRefreshHandler is not implemented. perform() not performed.");
        }
        if(args.length == 0) {
            // Can this happen???
            log.info("No input arg provided. perform() not performed.");
            // return null;
            throw new RestApiPolicyException("No input arg provided. perform() not performed.");
        }
        if(args[0] == null || ! (args[0] instanceof UserCredential)) {
            log.info("Input arg is null or an invalid type. perform() not performed.");
            // return null;
            throw new RestApiPolicyException("Input arg is null or an invalid type. perform() not performed.");
        }
        
        UserCredential userCredential = (UserCredential) args[0];

        // String accessToken = userCredential.getAuthToken();
        Long tokenExpirationTime = userCredential.getExpirationTime();  
        if(tokenExpirationTime == null) {
            tokenExpirationTime = 0L;     // ???
        }

        Long refreshedTokenExpirationTime = null;
        UserCredential refresedhUserCredential = authRefreshHandler.refreshAuthToken(userCredential);
        if(refresedhUserCredential != null) {
            refreshedTokenExpirationTime = refresedhUserCredential.getExpirationTime();
        } else {
            log.warning("Failed to refresh the user credential.");
            throw new RestApiPolicyException("Failed to refresh the user credential.");
        }
//        if(refreshedTokenExpirationTime != null && refreshedTokenExpirationTime > tokenExpirationTime) {
//            // Refreshed.
//            String newAccessToken = null;
//            if(refresedhUserCredential != null) {
//                newAccessToken = refresedhUserCredential.getAuthToken();
//            }
//            if(newAccessToken != null && !newAccessToken.isEmpty()) {
//                if(log.isLoggable(Level.INFO)) log.info("Access token refreshed. New expiration time = " + refreshedTokenExpirationTime);
//                // Use the new access token.
//                // accessToken = newAccessToken;   
//            } else {
//                // This cannot happen.
//                // Just use the old/current accessToken.
//                log.warning("User credential refreshed. But, failed to get the new access token.");
//            }
//        } else {
//            // Not refreshed.
//            log.info("User credential is not refreshed.");
//        }

        if(log.isLoggable(Level.FINER)) log.finer("userCredential refreshed: " + refresedhUserCredential);
        return refresedhUserCredential;
    }


    @Override
    public String toString()
    {
        return "AbstractAuthRefreshPolicy [refreshBeforeRequest="
                + refreshBeforeRequest + ", refreshIfFails=" + refreshIfFails
                + ", futureMarginSecs=" + futureMarginSecs
                + ", authRefreshHandler=" + authRefreshHandler + "]";
    }


}
