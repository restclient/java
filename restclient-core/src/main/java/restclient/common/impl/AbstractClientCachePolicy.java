package restclient.common.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.common.ClientCachePolicy;


public abstract class AbstractClientCachePolicy implements ClientCachePolicy, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractClientCachePolicy.class.getName());
    private static final long serialVersionUID = 1L;

    private boolean cacheEnabled;
    private int cacheLifetime;      // In seconds.


    public AbstractClientCachePolicy()
    {
        cacheEnabled = false;
        cacheLifetime = 0;

        // TBD:
        init();
    }

    protected void init()
    {
        // Place holder
    }


    @Override
    public boolean isCacheEnabled()
    {
        return cacheEnabled;
    }
    public void setCacheEnabled(boolean cacheEnabled)
    {
        this.cacheEnabled = cacheEnabled;
    }

    @Override
    public int getCacheLifetime()
    {
        return cacheLifetime;
    }
    public void setCacheLifetime(int cacheLifetime)
    {
        this.cacheLifetime = cacheLifetime;
    }


    @Override
    public Object get(String id)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean put(String id, Object object)
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean clear(String id)
    {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public String toString()
    {
        return "AbstractClientCachePolicy [cacheEnabled=" + cacheEnabled
                + ", cacheLifetime=" + cacheLifetime + "]";
    }


}
