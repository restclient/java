package restclient.common.impl;

import java.io.Serializable;
import java.net.URI;
import java.util.Map;
import java.util.logging.Logger;

import restclient.common.AuthenticationPolicy;
import restclient.credential.AuthCredential;


public abstract class AbstractAuthenticationtPolicy implements AuthenticationPolicy, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractAuthenticationtPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    // Basic, Digest, OAuth, 2LO, OAuth2, etc... 
    private String authMethod;
    
    // header, url-encoded single part form, or query string. 
    // For now, just use header ????
    private String authTransmissionType;


    public AbstractAuthenticationtPolicy(String authMethod, String authTransmissionType)
    {
        super();
        this.authMethod = authMethod;
        this.authTransmissionType = authTransmissionType;
        
        // TBD:
        init();
    }


    protected void init()
    {
        // Place holder
    }


    @Override
    public String getAuthMethod()
    {
        return authMethod;
    }
    public void setAuthMethod(String authMethod)
    {
        this.authMethod = authMethod;
    }

    @Override
    public String getAuthTransmissionType()
    {
        return authTransmissionType;
    }
    public void setAuthTransmissionType(String authTransmissionType)
    {
        this.authTransmissionType = authTransmissionType;
    }


    @Override
    public abstract String generateAuthorizationString(AuthCredential authCredential, String httpMethod, URI baseURI, Map<String, String[]> requestParams);

    
}
