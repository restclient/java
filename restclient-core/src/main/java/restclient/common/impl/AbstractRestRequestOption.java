package restclient.common.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import restclient.common.RestRequestOption;


public abstract class AbstractRestRequestOption implements RestRequestOption, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractRestRequestOption.class.getName());
    private static final long serialVersionUID = 1L;

    private String method;
    private int connectTimeout;
    Map<String,String> headers;
    
    public AbstractRestRequestOption()
    {
        headers = new HashMap<String,String>();

        // TBD:
        init();
    }
    
    protected void init()
    {
        // Place holder
    }

    @Override
    public String getMethod()
    {
        return method;
    }
    public void setMethod(String method)
    {
        this.method = method;
    }

    @Override
    public int getConnectTimeout()
    {
        return connectTimeout;
    }
    public void setConnectTimeout(int connectTimeout)
    {
        this.connectTimeout = connectTimeout;
    }

    @Override
    public Map<String, String> getHeaders()
    {
        return headers;
    }
    public void setHeaders(Map<String, String> headers)
    {
        this.headers = headers;
    }


    @Override
    public String toString()
    {
        return "AbstractRestRequestOption [method=" + method
                + ", connectTimeout=" + connectTimeout + ", headers=" + headers
                + "]";
    }

}
