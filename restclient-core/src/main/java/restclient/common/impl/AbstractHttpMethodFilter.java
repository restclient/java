package restclient.common.impl;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import restclient.common.HttpMethodFilter;
import restclient.core.HttpMethod;


public abstract class AbstractHttpMethodFilter extends AbstractMethodFilter implements HttpMethodFilter, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractHttpMethodFilter.class.getName());
    private static final long serialVersionUID = 1L;
    
    public AbstractHttpMethodFilter()
    {
        super();

        getMethodSet().add(HttpMethod.GET);
        getMethodSet().add(HttpMethod.POST);
        getMethodSet().add(HttpMethod.PUT);
        getMethodSet().add(HttpMethod.DELETE);
    }


    @Override
    public String toString()
    {
        return "AbstractHttpMethodFilter []";
    }


}
