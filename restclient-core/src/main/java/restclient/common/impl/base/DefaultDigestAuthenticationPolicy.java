package restclient.common.impl.base;

import java.net.URI;
import java.util.Map;
import java.util.logging.Logger;

import restclient.common.impl.AbstractAuthenticationtPolicy;
import restclient.credential.AuthCredential;


public final class DefaultDigestAuthenticationPolicy extends AbstractAuthenticationtPolicy
{
    private static final Logger log = Logger.getLogger(DefaultDigestAuthenticationPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultDigestAuthenticationPolicy(String authMethod, String authTransmissionType)
    {
        super(authMethod, authTransmissionType);
    }

    @Override
    public String generateAuthorizationString(AuthCredential authCredential, String httpMethod, URI baseURI, Map<String, String[]> requestParams)
    {
        // TBD:
        return null;
    }

    

}
