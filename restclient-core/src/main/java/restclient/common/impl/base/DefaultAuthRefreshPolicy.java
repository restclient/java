package restclient.common.impl.base;

import java.util.logging.Logger;

import restclient.common.UserAuthRefreshHandler;
import restclient.common.impl.AbstractAuthRefreshPolicy;


public final class DefaultAuthRefreshPolicy extends AbstractAuthRefreshPolicy
{
    private static final Logger log = Logger.getLogger(DefaultAuthRefreshPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    
    public DefaultAuthRefreshPolicy()
    {
    }

    
    // Factory method.
    protected UserAuthRefreshHandler makeUserAuthRefreshHandler()
    {
        return new DefaultUserAuthRefreshHandler();
    }


}
