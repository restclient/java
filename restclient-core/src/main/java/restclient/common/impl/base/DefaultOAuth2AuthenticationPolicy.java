package restclient.common.impl.base;

import java.net.URI;
import java.util.Map;
import java.util.logging.Logger;

import restclient.common.impl.AbstractAuthenticationtPolicy;
import restclient.credential.AuthCredential;


public final class DefaultOAuth2AuthenticationPolicy extends AbstractAuthenticationtPolicy
{
    private static final Logger log = Logger.getLogger(DefaultOAuth2AuthenticationPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultOAuth2AuthenticationPolicy(String authMethod, String authTransmissionType)
    {
        super(authMethod, authTransmissionType);
    }

    @Override
    public String generateAuthorizationString(AuthCredential authCredential, String httpMethod, URI baseURI, Map<String, String[]> requestParams)
    {
        // TBD:
        return null;
    }

    

}
