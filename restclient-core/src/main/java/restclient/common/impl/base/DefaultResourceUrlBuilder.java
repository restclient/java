package restclient.common.impl.base;

import java.util.logging.Logger;

import restclient.common.impl.AbstractResourceUrlBuilder;


// Default implementation.
public final class DefaultResourceUrlBuilder extends AbstractResourceUrlBuilder
{
    private static final Logger log = Logger.getLogger(DefaultResourceUrlBuilder.class.getName());

    public DefaultResourceUrlBuilder(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }


}
