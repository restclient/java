package restclient.common;

import java.util.Set;

import restclient.credential.DataAccessCredential;


public interface DataAccessClient
{
    // TBD:
    Set<String> getRequiredScopes();
    boolean requiresScope(String scope);
    boolean isAccessAllowed(DataAccessCredential dataAccessCredential);
    // ....
}
