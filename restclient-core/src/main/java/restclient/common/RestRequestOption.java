package restclient.common;

import java.util.Map;


// Not being used.
public interface RestRequestOption
{
    String getMethod();
    int getConnectTimeout();
    Map<String,String> getHeaders();
    // etc...
}
