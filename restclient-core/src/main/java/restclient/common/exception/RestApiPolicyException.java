package restclient.common.exception;

import restclient.RestApiException;

public class RestApiPolicyException extends RestApiException
{

    public RestApiPolicyException()
    {
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(String message)
    {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(String message, String resource)
    {
        super(message, resource);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(String message, String resource,
            int responseCode)
    {
        super(message, resource, responseCode);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(String message, String resource,
            int responseCode, String redirectLocation)
    {
        super(message, resource, responseCode, redirectLocation);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(String message, Throwable cause)
    {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(String message, Throwable cause,
            String resource)
    {
        super(message, cause, resource);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(String message, Throwable cause,
            String resource, int responseCode)
    {
        super(message, cause, resource, responseCode);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(String message, Throwable cause,
            String resource, int responseCode, String redirectLocation)
    {
        super(message, cause, resource, responseCode, redirectLocation);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(Throwable cause)
    {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(Throwable cause, String resource)
    {
        super(cause, resource);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(Throwable cause, String resource,
            int responseCode)
    {
        super(cause, resource, responseCode);
        // TODO Auto-generated constructor stub
    }

    public RestApiPolicyException(Throwable cause, String resource,
            int responseCode, String redirectLocation)
    {
        super(cause, resource, responseCode, redirectLocation);
        // TODO Auto-generated constructor stub
    }

}
