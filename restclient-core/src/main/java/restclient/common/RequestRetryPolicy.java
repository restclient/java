package restclient.common;


public interface RequestRetryPolicy extends WebClientPolicy
{
    boolean isRetryIfFails();
    int getMaxRetryCount();
    
    
}
