package restclient.common;


public interface MethodFilter
{
    // Returns false if the method is not supported.
    // true does not mean the method is supported by the implementation.
    // The underlying implementation may still throw a "not allowed/not supported/not implemented" exception.
    // Consider MethodFilder a filter/mask.
    boolean isMethodSupported(String methodName);
}
