package restclient.common;


public interface AuthRefreshPolicy extends WebClientPolicy, Performable
{
    boolean isRefreshBeforeRequest();
    boolean isRefreshIfFails();
    int getFutureMarginSeconds();

    UserAuthRefreshHandler getAuthRefreshHandler();
    // void setAuthRefreshHandler(UserAuthRefreshHandler userAuthRefreshHandler);
    
    // TBD:
    // UserCredential refreshAuthToken(UserCredential userCredential);
}
