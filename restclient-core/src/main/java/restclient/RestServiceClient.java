package restclient;

import java.io.IOException;
import java.util.Map;

import restclient.common.HttpMethodFilter;
import restclient.credential.UserCredential;


/**
 * REST client.
 * No method specific options.
 * All connection-related settings should be stored in a RestServiceClient object,
 * and they are used for all (subsequent) calls, get(), post(), put(), and delete().
 * All methods return a map, containing
 * "statusCode": statusCode,
 * "payload" resource (in json) for get, post, and put. boolean for delete.
 */
public interface RestServiceClient extends ResourceServiceClient, RestClient, HttpMethodFilter
{
    // TBD: 
    //      Throw RestApiException ?????
    // ..

    /**
     * Both id and params cannot be null.
     * It supports two use cases:
     * (1) If id is specified, then it returns the resource specified by id.
     *     Params should not normally be used in this case.
     * (2) If id is not specified, then params is required.
     *     It returns the collection of resources specified by the params.
     * If neither id nor params is specified, then it's an error.
     * 
     * @param credential Auth credential of a user needed to access the resource on behalf of the user.
     * @param id Resource id. 
     * @param params URL query parameters to select a collection.
     * @return Returns a single resource or a collection.
     * @throws IOException TODO
     */
    Map<String, Object> get(UserCredential credential, String id, Map<String,Object> params) throws IOException;

    /**
     * Returns the new resource created by the given input. 
     *   The inputData should be a complete representation of the resource.
     * 
     * @param credential Auth credential of a user needed to access the resource on behalf of the user.
     * @param inputData The object to be created.
     * @return The new resource.
     * @throws IOException TODO
     */
    Map<String, Object> post(UserCredential credential, Object inputData) throws IOException;

    /**
     * Returns the resource created/updated by the given input. 
     * If the resource exists for the given id, the resource is updated.
     * Otherwise, a new resource is created.
     * 
     * @param credential Auth credential of a user needed to access the resource on behalf of the user.
     * @param inputData The new resource.
     * @param id Resource id.
     * @return The new created/updated resource.
     * @throws IOException TODO
     */
    Map<String, Object> put(UserCredential credential, Object inputData, String id) throws IOException;

    // TBD:
    // partial update.
    Map<String, Object> patch(UserCredential credential, Object partialData, String id) throws IOException;

    /**
     * Both id and params cannot be null.
     * It supports two use cases:
     * (1) If id is specified, then it deletes the resource specified by id.
     *     Params should not normally be used in this case.
     * (2) If id is not specified, then params is required.
     *     It deletes the collection of all resources specified by the params.
     * If neither id nor params is specified, then it's an error.
     * 
     * @param credential Auth credential of a user needed to access the resource on behalf of the user.
     * @param id Resource id.
     * @param params URL query parameters to select a collection.
     * @return boolean variable to indicate the success/failure.
     * @throws IOException TODO
     */
    Map<String, Object> delete(UserCredential credential, String id, Map<String,Object> params) throws IOException;    
}
