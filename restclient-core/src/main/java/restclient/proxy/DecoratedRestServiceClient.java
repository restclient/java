package restclient.proxy;

import restclient.RestServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedRestServiceClient extends RestServiceClient
{
    // No need for API to return the decorated client.
    // RestServiceClient getDecoratedClient();
}
