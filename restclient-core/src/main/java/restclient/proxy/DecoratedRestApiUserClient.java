package restclient.proxy;

import restclient.RestApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedRestApiUserClient extends RestApiUserClient
{
    // No need for API to return the decorated client.
    // RestApiUserClient getDecoratedClient();
}
