package restclient.proxy;

import restclient.ApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedApiServiceClient extends ApiServiceClient
{
    // No need for API to return the decorated client.
    // ApiServiceClient getDecoratedClient();
}
