package restclient.proxy;

import restclient.ServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedServiceClient extends ServiceClient
{
    // No need for API to return the decorated client.
    // ServiceClient getDecoratedClient();
}
