package restclient.proxy;

import restclient.RestUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedRestUserClient extends RestUserClient
{
    // No need for API to return the decorated client.
    // RestUserClient getDecoratedClient();
}
