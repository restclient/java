package restclient;

import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.RequestRetryPolicy;


// Base interface for "http" based client.
public interface FlexibleWebClient extends WebClient
{
    void setAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy);
    void setRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy);
    void setClientCachePolicy(ClientCachePolicy clientCachePolicy);
    void setCacheControlPolicy(CacheControlPolicy cacheControlPolicy);
    void setAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy);
    // etc...
    
}
