package restclient;

import restclient.credential.ClientCredential;


/**
 * Service specific client object instance.
 * A "service client" can be used across multiple users.
 */
public interface ServiceClient
{
    /**
     * Returns the ClientCredential associated with this ServiceClient
     * @return ClientCredential
     */
    ClientCredential getClientCredential();
}
