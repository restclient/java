package restclient.exception;

import restclient.RestApiException;
import restclient.core.StatusCode;


// TBD.
public class BadRequestRaException extends RestApiException
{
    private static final long serialVersionUID = 1L;

    public BadRequestRaException() 
    {
        this((String) null);
    }
    public BadRequestRaException(String message) 
    {
        this(message, (String) null);
    }
    public BadRequestRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.BAD_REQUEST);
    }
    public BadRequestRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public BadRequestRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public BadRequestRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.BAD_REQUEST);
    }
    public BadRequestRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public BadRequestRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public BadRequestRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.BAD_REQUEST);
    }
    public BadRequestRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
