package restclient.exception;

import restclient.core.StatusCode;


// TBD.
public class FoundElsewhereRaException extends RestApiRedirectException
{
    private static final long serialVersionUID = 1L;

    public FoundElsewhereRaException() 
    {
        this((String) null);
    }
    public FoundElsewhereRaException(String message) 
    {
        this(message, (String) null);
    }
    public FoundElsewhereRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.FOUND);
    }
    public FoundElsewhereRaException(String message, String resource, int responseCode) 
    {
        this(message, resource, responseCode, null);
    }
    public FoundElsewhereRaException(String message, String resource, int responseCode, String redirectLocation) 
    {
        super(message, resource, responseCode, redirectLocation);
    }
    public FoundElsewhereRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public FoundElsewhereRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.FOUND);
    }
    public FoundElsewhereRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        this(message, cause, resource, responseCode, null);
    }
    public FoundElsewhereRaException(String message, Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(message, cause, resource, responseCode, redirectLocation);
    }
    public FoundElsewhereRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public FoundElsewhereRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.FOUND);
    }
    public FoundElsewhereRaException(Throwable cause, String resource, int responseCode) 
    {
        this(cause, resource, responseCode, null);
    }
    public FoundElsewhereRaException(Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(cause, resource, responseCode, redirectLocation);
    }

}
