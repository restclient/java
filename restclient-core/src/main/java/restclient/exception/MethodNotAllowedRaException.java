package restclient.exception;

import restclient.RestApiException;
import restclient.core.StatusCode;


// TBD.
public class MethodNotAllowedRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public MethodNotAllowedRaException() 
    {
        this((String) null);
    }
    public MethodNotAllowedRaException(String message) 
    {
        this(message, (String) null);
    }
    public MethodNotAllowedRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.METHOD_NOT_ALLOWED);
    }
    public MethodNotAllowedRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public MethodNotAllowedRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public MethodNotAllowedRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.METHOD_NOT_ALLOWED);
    }
    public MethodNotAllowedRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public MethodNotAllowedRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public MethodNotAllowedRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.METHOD_NOT_ALLOWED);
    }
    public MethodNotAllowedRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
