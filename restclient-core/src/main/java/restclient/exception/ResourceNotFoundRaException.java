package restclient.exception;

import restclient.RestApiException;
import restclient.core.StatusCode;


// TBD.
public class ResourceNotFoundRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public ResourceNotFoundRaException() 
    {
        this((String) null);
    }
    public ResourceNotFoundRaException(String message) 
    {
        this(message, (String) null);
    }
    public ResourceNotFoundRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.NOT_FOUND);
    }
    public ResourceNotFoundRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public ResourceNotFoundRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public ResourceNotFoundRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.NOT_FOUND);
    }
    public ResourceNotFoundRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public ResourceNotFoundRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public ResourceNotFoundRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.NOT_FOUND);
    }
    public ResourceNotFoundRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
