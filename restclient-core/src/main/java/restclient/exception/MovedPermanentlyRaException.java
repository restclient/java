package restclient.exception;

import restclient.RestApiException;
import restclient.core.StatusCode;


// TBD.
public class MovedPermanentlyRaException extends RestApiRedirectException
{
    private static final long serialVersionUID = 1L;

    public MovedPermanentlyRaException() 
    {
        this((String) null);
    }
    public MovedPermanentlyRaException(String message) 
    {
        this(message, (String) null);
    }
    public MovedPermanentlyRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.MOVED_PERMANENTLY);
    }
    public MovedPermanentlyRaException(String message, String resource, int responseCode) 
    {
        this(message, resource, responseCode, null);
    }
    public MovedPermanentlyRaException(String message, String resource, int responseCode, String redirectLocation) 
    {
        super(message, resource, responseCode, redirectLocation);
    }
    public MovedPermanentlyRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public MovedPermanentlyRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.MOVED_PERMANENTLY);
    }
    public MovedPermanentlyRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        this(message, cause, resource, responseCode, null);
    }
    public MovedPermanentlyRaException(String message, Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(message, cause, resource, responseCode, redirectLocation);
    }
    public MovedPermanentlyRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public MovedPermanentlyRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.MOVED_PERMANENTLY);
    }
    public MovedPermanentlyRaException(Throwable cause, String resource, int responseCode) 
    {
        this(cause, resource, responseCode, null);
    }
    public MovedPermanentlyRaException(Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(cause, resource, responseCode, redirectLocation);
    }

}
