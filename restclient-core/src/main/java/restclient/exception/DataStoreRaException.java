package restclient.exception;

import restclient.core.StatusCode;


// TBD.
public class DataStoreRaException extends RestApiServerException
{
    private static final long serialVersionUID = 1L;

    public DataStoreRaException() 
    {
        this((String) null);
    }
    public DataStoreRaException(String message) 
    {
        this(message, (String) null);
    }
    public DataStoreRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.INTERNAL_SERVER_ERROR);
    }
    public DataStoreRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public DataStoreRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public DataStoreRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.INTERNAL_SERVER_ERROR);
    }
    public DataStoreRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public DataStoreRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public DataStoreRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.INTERNAL_SERVER_ERROR);
    }
    public DataStoreRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
