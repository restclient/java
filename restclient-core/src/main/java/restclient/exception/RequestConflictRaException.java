package restclient.exception;

import restclient.RestApiException;
import restclient.core.StatusCode;


// TBD.
public class RequestConflictRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public RequestConflictRaException() 
    {
        this((String) null);
    }
    public RequestConflictRaException(String message) 
    {
        this(message, (String) null);
    }
    public RequestConflictRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.CONFLICT);
    }
    public RequestConflictRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public RequestConflictRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public RequestConflictRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.CONFLICT);
    }
    public RequestConflictRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public RequestConflictRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public RequestConflictRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.CONFLICT);
    }
    public RequestConflictRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
