package restclient.exception;

import restclient.RestApiException;
import restclient.core.StatusCode;


// TBD.
public class UnsupportedMediaTypeRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public UnsupportedMediaTypeRaException() 
    {
        this((String) null);
    }
    public UnsupportedMediaTypeRaException(String message) 
    {
        this(message, (String) null);
    }
    public UnsupportedMediaTypeRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.UNSUPPORTED_MEDIA_TYPE);
    }
    public UnsupportedMediaTypeRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public UnsupportedMediaTypeRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public UnsupportedMediaTypeRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.UNSUPPORTED_MEDIA_TYPE);
    }
    public UnsupportedMediaTypeRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public UnsupportedMediaTypeRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public UnsupportedMediaTypeRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.UNSUPPORTED_MEDIA_TYPE);
    }
    public UnsupportedMediaTypeRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
