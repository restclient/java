package restclient.exception;

import restclient.RestApiException;
import restclient.core.StatusCode;


// TBD.
public class ResourceGoneRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public ResourceGoneRaException() 
    {
        this((String) null);
    }
    public ResourceGoneRaException(String message) 
    {
        this(message, (String) null);
    }
    public ResourceGoneRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.GONE);
    }
    public ResourceGoneRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public ResourceGoneRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public ResourceGoneRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.GONE);
    }
    public ResourceGoneRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public ResourceGoneRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public ResourceGoneRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.GONE);
    }
    public ResourceGoneRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
