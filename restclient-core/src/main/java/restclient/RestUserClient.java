package restclient;

import java.io.IOException;
import java.util.Map;

import restclient.common.HttpMethodFilter;
import restclient.credential.UserCredential;


/**
 * REST Client. User-specific.
 *     (That is, these instances are not shared among different users.)
 *
 */
public interface RestUserClient extends ResourceUserClient, RestClient, HttpMethodFilter
{
    // TBD: 
    //      Throw RestApiException ?????
    // ..

    // Is this part of the interface?
    // Or, it's just an implementation detail.
    // RestServiceClient getRestServiceClient();

    /**
     * Both id and params cannot be null.
     * It supports two use cases:
     * (1) If id is specified, then it returns the resource specified by id.
     *     Params should not normally be used in this case.
     * (2) If id is not specified, then params is required.
     *     It returns the collection of resources specified by the params.
     * If neither id nor params is specified, then it's an error.
     * 
     * @param id Resource id. 
     * @param params URL query parameters to select a collection.
     * @return Returns a single resource or a collection.
     * @throws IOException TODO
     */
    Map<String,Object> get(String id, Map<String,Object> params) throws IOException;

    /**
     * Returns the new resource created by the given input. 
     *   The inputData should be a complete representation of the resource.
     * 
     * @param inputData The object to be created.
     * @return The new resource.
     * @throws IOException TODO
     */
    Map<String,Object> post(Object inputData) throws IOException;

    /**
     * Returns the resource created/updated by the given input. 
     * If the resource exists for the given id, the resource is updated.
     * Otherwise, a new resource is created.
     * 
     * @param inputData The new resource.
     * @param id Resource id.
     * @return The new created/updated resource.
     * @throws IOException TODO
     */
    Map<String,Object> put(Object inputData, String id) throws IOException;

    // TBD:
    // partial update.
    Map<String, Object> patch(Object partialData, String id) throws IOException;

    /**
     * Both id and params cannot be null.
     * It supports two use cases:
     * (1) If id is specified, then it deletes the resource specified by id.
     *     Params should not normally be used in this case.
     * (2) If id is not specified, then params is required.
     *     It deletes the collection of all resources specified by the params.
     * If neither id nor params is specified, then it's an error.
     * 
     * @param id Resource id.
     * @param params URL query parameters to select a collection.
     * @return boolean variable to indicate the success/failure.
     * @throws IOException TODO
     */
    Map<String,Object> delete(String id, Map<String,Object> params) throws IOException;
}
