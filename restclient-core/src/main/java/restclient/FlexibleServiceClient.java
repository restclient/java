package restclient;

import restclient.credential.ClientCredential;


/**
 * Service specific client object instance.
 */
public interface FlexibleServiceClient extends ServiceClient
{
    void setClientCredential(ClientCredential clientCredential);
}
