package restclient;

import restclient.credential.UserCredential;


/**
 * User specific client object instance.
 */
public interface FlexibleUserClient extends UserClient
{
    void setUserCredential(UserCredential userCredential);
}
