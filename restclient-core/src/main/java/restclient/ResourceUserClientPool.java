package restclient;


public interface ResourceUserClientPool extends ResourceClient
{
    ResourceUserClient getUserClient(String userId);
    // ...
}
