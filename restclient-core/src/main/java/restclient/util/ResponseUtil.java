package restclient.util;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Note that we do not define a separate "Response" object.
 * Response is simply a map of { String -> Object } with a special structure at the top level.
 * Currently, we support a number of "predefined" top-level keys:
 *    "statusCode", "error", and "payload".
 * These are extensible. That is, as long as the caller/service agree, arbitrary key->value pairs can be added.
 * The caller will simply ignore the keys which it does not understand.
 * Note that, currently, we do not allow both error and payload in a single response
 *     (although we do not really have a way of enforcing it).
 * If both are included in a response, it should be treated as an error response and the payload should be ignored. 
 */
public final class ResponseUtil
{
    private static final Logger log = Logger.getLogger(ResponseUtil.class.getName());


    // temporary
    // "pre-defined" keys:
    
    /**
     * Required?
     * Int value of the response.statusCode.
     */
    public static final String KEY_STATUSCODE = "status";
    
    /**
     * Value is a generic object.
     * Could be a scalar value, or a Map of {String -> Object}, representing an object.
     */
    public static final String KEY_PAYLOAD = "payload";
    
//    /**
//     * In case the server needs to return some cardinal value.
//     * For example, the number of records deleted for the delete() request, etc.
//     */
//    public static final String KEY_COUNT = "count";

    /**
     * Request URL.
     * This is not really needed since the caller always have this information.
     * But, it can be convenient to have this in the response data structure at times...
     */
    public static final String KEY_RESOURCE = "resource";

    /**
     * HTTP Response header "Location".
     * To be used with 201 or other 3xx status codes.
     */
    public static final String KEY_LOCATION = "location";

    /**
     * The value should be a map with keys, code and message. 
     */
    public static final String KEY_ERROR = "error";

    /**
     * Int (or String) code.
     * For convenience, we treat code as String.
     */
    public static final String KEY_ERROR_CODE = "code";
    /**
     * Error message string.
     */
    public static final String KEY_ERROR_MESSAGE = "message";
    /**
     * Error stack trace, or other detailed error message.
     */
    public static final String KEY_ERROR_TRACE = "trace";
    
    // error excpetion stack trace???
    ///...

    // etc...
    
    
    private ResponseUtil() {}

    
    // TBD:
    // No need for builder. Response is simple enough.....
    private static final class ResponseBuilder {
        public ResponseBuilder add(String key, Object value) { return this; };
        public Map<String,Object> build() { return null; };
    };

    
    // Builds the "success" response.
    // payload can be a scalar value (e.g., string)
    // or an object (== Map of { String key -> Object }
    // it can be null. (If null, do not add it.)
    public static Map<String,Object> buildResponse(String resource, int statusCode, Object payload)
    {
        Map<String,Object> response =buildResponse(resource, statusCode, payload, null);
        return response;
    }
    public static Map<String,Object> buildResponse(String resource, int statusCode, Object payload, String location)
    {
        Map<String,Object> response = new LinkedHashMap<String,Object>();
        if(resource != null) {
            response.put(KEY_RESOURCE, resource);
        }
        response.put(KEY_STATUSCODE, statusCode);
        if(payload != null) {
            response.put(KEY_PAYLOAD, payload);
        }
        if(location != null) {
            response.put(KEY_LOCATION, location);
        }
        if(log.isLoggable(Level.FINE)) log.fine("response = " + response);
        return response;
    }
    // Builds the "failure" response.
    public static Map<String,Object> buildResponse(String resource, int statusCode, String errorCode, String errorMessage)
    {
        Map<String,Object> response = buildResponse(resource, statusCode, errorCode, errorMessage, null);
        return response;
    }
    public static Map<String,Object> buildResponse(String resource, int statusCode, String errorCode, String errorMessage, String errorTrace)
    {
        Map<String,Object> response = new LinkedHashMap<String,Object>();
        if(resource != null) {
            response.put(KEY_RESOURCE, resource);
        }
        response.put(KEY_STATUSCODE, statusCode);

        Map<String,String> error = new LinkedHashMap<String,String>();
        error.put(KEY_ERROR_CODE, errorCode);
        if(errorMessage != null) {
            error.put(KEY_ERROR_MESSAGE, errorMessage);
        }
        if(errorTrace != null) {
            error.put(KEY_ERROR_TRACE, errorTrace);
        }
        response.put(KEY_ERROR, error);

        if(log.isLoggable(Level.FINE)) log.fine("response = " + response);
        return response;
    }

    // No generic "parse" function.
    // The following returns each field from the top-level structure.
    
    // Returns the response status code.
    public static int getSatusCude(Map<String,Object> response)
    {
        int statusCode = 0;  // ???
        if(response != null && response.containsKey(KEY_STATUSCODE)) {
            Integer val = (Integer) response.get(KEY_STATUSCODE);
            if(val != null) {
                statusCode = val;
            } else {
                // ????
            }
        }
        if(statusCode == 0) {
            log.warning("Status code is not set.");            
        }
        return statusCode;
    }
    
    // Returns the error map.
    // It returns null if no error is found in the response.
    @SuppressWarnings("unchecked")
    public static Map<String,String> getError(Map<String,Object> response)
    {
        Map<String,String> error = null;
        if(response != null && response.containsKey(KEY_ERROR)) {
            Object val = response.get(KEY_ERROR);
            if(val instanceof Map<?,?>) {
                error = (Map<String,String>) val;
            } else {
                // ???
                if(log.isLoggable(Level.INFO)) log.info("Invalid error format: error = " + val);            
            }
        }
        return error;
    }

    // Returns the payload from the response, if exists.
    public static Object getPayload(Map<String,Object> response)
    {
        Object payload = null;
        if(response != null && response.containsKey(KEY_PAYLOAD)) {
            payload = response.get(KEY_PAYLOAD);
        }
        return payload;
    }

}
