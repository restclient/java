package restclient.util;

import java.util.logging.Level;
import java.util.logging.Logger;

//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.json.XML;


// Hack.
// Need a better implementation.
// Note: Take a look at this...
// http://stackoverflow.com/questions/818327/jaxb-how-should-i-marshal-complex-nested-data-structures
// ....
/* public */ final class XmlUtil
{
    private static final Logger log = Logger.getLogger(XmlUtil.class.getName());
    
    private XmlUtil() {}

    // TBD:
    public static Object parseXml(String xmlStr)
    {
        if(xmlStr == null) {
            log.warning("Input xmlStr is null.");
            return null;
        }
        if(log.isLoggable(Level.FINE)) log.fine("Input xmlStr = " + xmlStr);

        String jsonStr = XmlToJson(xmlStr);
        Object xmlObj = null;   // JsonUtil.parseJson(jsonStr);
        if(log.isLoggable(Level.FINE)) log.fine("Output xmlObj = " + xmlObj);

        return xmlObj;
    }

    public static String XmlToJson(String xmlStr) 
    {
        if(xmlStr == null) {
            return null;
        }
//        JSONObject jsonObj = XML.toJSONObject(xmlStr);
//        String jsonStr = jsonObj.toString();
//        return jsonStr;

        return null;
    }
        
    // TBD:
    public static String buildXml(Object xmlObj)
    {
        if(xmlObj == null) {
            log.warning("Input xmlObj is null.");
            return null;
        }
        if(log.isLoggable(Level.FINE)) log.fine("Input xmlObj = " + xmlObj);

        String jsonStr = null;   // = JsonUtil.buildJson(xmlObj);
        String xmlStr = JsonToXml(jsonStr);
        if(log.isLoggable(Level.FINE)) log.fine("Output xmlStr = " + xmlStr);

        return xmlStr;
    }

    public static String JsonToXml(String jsonStr) 
    {
        if(jsonStr == null) {
            return null;
        }
//        Object o = null;
//        if(jsonStr.startsWith("[")) {
//            o = new JSONArray(jsonStr);
//        } else if(jsonStr.startsWith("{")) {
//            o = new JSONObject(jsonStr);
//        } else {
//            // ???
//            String j = "{\"object\":" + jsonStr + "}";
//            o = new JSONObject(j);
//        }
//        String xmlStr = XML.toString(o);
//        return xmlStr;

        return null;
    }

    
}
