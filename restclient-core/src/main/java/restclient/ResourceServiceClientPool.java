package restclient;


public interface ResourceServiceClientPool extends WebClient
{
    ResourceServiceClient getServiceClient(String resourceBaseUrl);
    // ...
}
