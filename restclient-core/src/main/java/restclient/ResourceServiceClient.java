package restclient;

import restclient.common.DataAccessClient;



/**
 * Service specific client object instance.
 */
public interface ResourceServiceClient extends ResourceClient, ServiceClient, DataAccessClient
{
  
}
