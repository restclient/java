package restclient.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.FlexibleResourceClient;
import restclient.FlexibleServiceClient;
import restclient.RestApiException;
import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.CrudMethodFilter;
import restclient.common.RequestRetryPolicy;
import restclient.core.CrudMethod;
import restclient.core.ListResponseType;
import restclient.core.StatusCode;
import restclient.credential.ClientCredential;
import restclient.credential.DataAccessCredential;
import restclient.credential.UserCredential;
import restclient.exception.MethodNotAllowedRaException;
import restclient.exception.NotImplementedRaException;
import restclient.exception.RestApiClientException;
import restclient.exception.StatusExceptionMapper;
import restclient.maker.ApiServiceClientMaker;
import restclient.maker.impl.AbstractApiServiceClientMaker;
import restclient.util.ResponseUtil;


// Base class for provider-specific "api client".
// "API client" encapsulate various information/logic relevant to making remote api calls.
// Note: 
// An instance of ApiClient is used across multiple users.
//      --> Don't store any user-specific info in this class.
public abstract class AbstractApiServiceClient implements ApiServiceClient, FlexibleResourceClient, FlexibleServiceClient
{
    private static final Logger log = Logger.getLogger(AbstractApiServiceClient.class.getName());
    
    // TBD:
    // We use the "REST client" as an underlying implementation.
    private final RestServiceClient restServiceClient;
    // ...
    
    // Abstract factory.
    private final ApiServiceClientMaker apiServiceClientMaker;

    // which method is this client (actually, more for the server) supporting?
    private final CrudMethodFilter crudMethodFilter;

    // Different Web services return "list" output in a different format.
    private ListResponseType listResponseType;
    
    
    //
    // Note: these can be different from the corresponding fields in restServiceClient.
    private AuthRefreshPolicy authRefreshPolicy;
    private RequestRetryPolicy requestRetryPolicy;
    private ClientCachePolicy clientCachePolicy; 
    private CacheControlPolicy cacheControlPolicy;
    private AutoRedirectPolicy autoRedirectPolicy;
    // etc..

    
    public AbstractApiServiceClient(String resourceBaseUrl)
    {
        super();

        // Abstract factory.
        apiServiceClientMaker = makeApiServiceClientMaker();

        restServiceClient = makeRestClient(resourceBaseUrl);
        
//        // temporary
//        if(log.isLoggable(Level.INFO)) {
//            if(restServiceClient instanceof AbstractRestServiceClient) {
//                log.info("restServiceClient instanceof AbstractRestServiceClient");
//            }
//            if(restServiceClient instanceof MockRestServiceClient) {
//                log.info("restServiceClient instanceof MockRestServiceClient");
//            }
//            // ...
//        }
//        // temporary
        

        // Allows all methods, by default.
        // crudMethodFilter should not be null.
        crudMethodFilter = makeCrudMethodFilter();

        // By default, we use the same policies as those of restServiceClient.
        //    (Note: not values, we use the same references.)
        // If separate values are needed, the caller needs to explicitly set it with repective setters().
        authRefreshPolicy = makeAuthRefreshPolicy();
        requestRetryPolicy = makeRequestRetryPolicy();
        clientCachePolicy = makeClientCachePolicy();
        cacheControlPolicy = makeCacheControlPolicy();
        autoRedirectPolicy = makeAutoRedirectPolicy();

        init();
    }
    
//    public AbstractApiServiceClient(String resourceBaseUrl)
//    {
//        this(new AbstractResourceUrlBuilder(resourceBaseUrl) {});
//    }
//    public AbstractApiServiceClient(ResourceUrlBuilder resourceUrlBuilder) 
//    {
//        this((RestServiceClient) new AbstractRestServiceClient(resourceUrlBuilder) {});
//    }
//    public AbstractApiServiceClient(RestServiceClient restServiceClient)
//    {
//        super();
//
//        if(restServiceClient == null) {
//            this.restServiceClient = null;    // createRestClient(resourceBaseUrl);
//        } else {
//            this.restServiceClient = restServiceClient;
//        }
//        
//        init();
//    }
    
    protected void init()
    {

        // TBD: use factory method for this as well???
        listResponseType = ListResponseType.getDefaultListResponseType();
    }

    protected RestServiceClient getRestServiceClient()
    {
        return restServiceClient;
    }
    protected FlexibleResourceClient getFlexibleRestServiceClient()
    {
        return (FlexibleResourceClient) restServiceClient;
    }

    
    // Factory methods

    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return createApiServiceClientMaker();
    }
    private static ApiServiceClientMaker createApiServiceClientMaker()
    {
        return AbstractApiServiceClientMaker.getInstance();
    }

    protected RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return apiServiceClientMaker.makeRestClient(resourceBaseUrl);
    }

    protected CrudMethodFilter makeCrudMethodFilter()
    {
        return apiServiceClientMaker.makeCrudMethodFilter();
    }

    protected AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return apiServiceClientMaker.makeAuthRefreshPolicy();
    }
    protected RequestRetryPolicy makeRequestRetryPolicy()
    {
        return apiServiceClientMaker.makeRequestRetryPolicy();
    }
    protected ClientCachePolicy makeClientCachePolicy()
    {
        return apiServiceClientMaker.makeClientCachePolicy();
    }
    protected CacheControlPolicy makeCacheControlPolicy()
    {
        return apiServiceClientMaker.makeCacheControlPolicy();
    }
    protected AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return apiServiceClientMaker.makeAutoRedirectPolicy();
    }



    public CrudMethodFilter getCrudMethodFilter()
    {
        return crudMethodFilter;
    }
//    public void setCrudMethodFilter(CrudMethodFilter crudMethodFilter)
//    {
//        this.crudMethodFilter = crudMethodFilter;
//    }


    @Override
    public boolean isMethodSupported(String methodName)
    {
        return crudMethodFilter.isMethodSupported(methodName);
    }


    public ListResponseType getListResponseType()
    {
        return listResponseType;
    }
    public void setListResponseType(ListResponseType listResponseType)
    {
        this.listResponseType = listResponseType;
    }


    @Override
    public String getResourceBaseUrl()
    {
        return getRestServiceClient().getResourceBaseUrl();
    }
//    @Override
//    public void setResourceBaseUrl(String resourceBaseUrl)
//    {
//        getRestServiceClient().setResourceBaseUrl(resourceBaseUrl);
//    }


    @Override
    public ClientCredential getClientCredential()
    {
        return getRestServiceClient().getClientCredential();
    }
    public void setClientCredential(ClientCredential clientCredential)
    {
        ((AbstractRestServiceClient) getRestServiceClient()).setClientCredential(clientCredential);
    }


    public AuthRefreshPolicy getRestServiceAuthRefreshPolicy()
    {
        return getRestServiceClient().getAuthRefreshPolicy();
    }
    public void setRestServiceAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy)
    {
        getFlexibleRestServiceClient().setAuthRefreshPolicy(authRefreshPolicy);
    }

    public RequestRetryPolicy getRestServiceRequestRetryPolicy()
    {
        return getRestServiceClient().getRequestRetryPolicy();
    }
    public void setRestServiceRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy)
    {
        getFlexibleRestServiceClient().setRequestRetryPolicy(requestRetryPolicy);
    }

    public ClientCachePolicy getRestServiceClientCachePolicy()
    {
        return getRestServiceClient().getClientCachePolicy();
    }
    public void setRestServiceClientCachePolicy(ClientCachePolicy clientCachePolicy)
    {
        getFlexibleRestServiceClient().setClientCachePolicy(clientCachePolicy);
    }

    public CacheControlPolicy getRestServiceCacheControlPolicy()
    {
        return getRestServiceClient().getCacheControlPolicy();
    }
    public void setRestServiceCacheControlPolicy(CacheControlPolicy cacheControlPolicy)
    {
        getFlexibleRestServiceClient().setCacheControlPolicy(cacheControlPolicy);
    }

    public AutoRedirectPolicy getRestServiceAutoRedirectPolicy()
    {
        return getRestServiceClient().getAutoRedirectPolicy();
    }
    public void setRestServiceAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy)
    {
        getFlexibleRestServiceClient().setAutoRedirectPolicy(autoRedirectPolicy);
    }


    @Override
    public Set<String> getRequiredScopes()
    {
        return getRestServiceClient().getRequiredScopes();
    }

    @Override
    public boolean requiresScope(String scope)
    {
        return getRestServiceClient().requiresScope(scope);
    }

    @Override
    public boolean isAccessAllowed(DataAccessCredential dataAccessCredential)
    {
        return getRestServiceClient().isAccessAllowed(dataAccessCredential);
    }


    @Override
    public AuthRefreshPolicy getAuthRefreshPolicy()
    {
        return authRefreshPolicy;
    }
    @Override
    public void setAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy)
    {
        this.authRefreshPolicy = authRefreshPolicy;
    }

    @Override
    public RequestRetryPolicy getRequestRetryPolicy()
    {
        return requestRetryPolicy;
    }
    @Override
    public void setRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy)
    {
        this.requestRetryPolicy = requestRetryPolicy;
    }

    @Override
    public ClientCachePolicy getClientCachePolicy()
    {
        return clientCachePolicy;
    }
    @Override
    public void setClientCachePolicy(ClientCachePolicy clientCachePolicy)
    {
        this.clientCachePolicy = clientCachePolicy;
    }
    
    @Override
    public AutoRedirectPolicy getAutoRedirectPolicy()
    {
        return autoRedirectPolicy;
    }
    @Override
    public void setAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy)
    {
        this.autoRedirectPolicy = autoRedirectPolicy;
    }


    @Override
    public Object get(UserCredential credential, String id) throws RestApiException, IOException
    {
        Object object = null;   // Map<String,Object> ???
        if(crudMethodFilter.isMethodSupported(CrudMethod.GET_ITEM)) {
            Map<String,Object> response = null;
            if(id == null || id.isEmpty()) {
                // error.
                throw new RestApiClientException("Resource id is missing.");
            } else {
                response = getRestServiceClient().get(credential, id, null);
            }
            if(response != null) {
                int statusCode = (Integer) response.get(ResponseUtil.KEY_STATUSCODE);
                if(StatusCode.isSuccessful(statusCode)) {
                    Object payload = response.get(ResponseUtil.KEY_PAYLOAD);
                    object = payload;
                } else {
                    @SuppressWarnings("unchecked")
                    Map<String,String> error = (Map<String,String>) response.get(ResponseUtil.KEY_ERROR);
                    String resource = (String) response.get(ResponseUtil.KEY_RESOURCE);
                    String location = (String) response.get(ResponseUtil.KEY_LOCATION);
                    // Throw exception.
                    RestApiException exception = null;
                    if(error != null) {
                        String errorCode = error.get(ResponseUtil.KEY_ERROR_CODE);        // ???
                        String errorMessage = error.get(ResponseUtil.KEY_ERROR_MESSAGE);
                        exception = StatusExceptionMapper.buildException(errorMessage, null, resource, statusCode, location);
                    } else {
                        exception = new RestApiException("Unknown error: Failed to get() the resource for id = " + id);
                    }
                    throw exception;
                }
            } else {
                // ???
                throw new RestApiException("Failed to get() the resource for id = " + id);
            }
        } else {
            throw new MethodNotAllowedRaException("Method, " + CrudMethod.GET_ITEM + ", not supporeted.");
        }
        return object;
    }

    @Override
    public List<Object> list(UserCredential credential, Map<String, Object> params) throws RestApiException, IOException
    {
        List<Object> list = null;
        if(crudMethodFilter.isMethodSupported(CrudMethod.GET_LIST)) {
            Map<String,Object> response = getRestServiceClient().get(credential, null, params);
            if(response != null) {
                int statusCode = (Integer) response.get(ResponseUtil.KEY_STATUSCODE);
                if(StatusCode.isSuccessful(statusCode)) {
                    Object payload = response.get(ResponseUtil.KEY_PAYLOAD);
                    if(payload instanceof List<?>) {
                        @SuppressWarnings("unchecked")
                        List<Object> objectList = (List<Object>) payload;
                        list = objectList;
                    } else {
                        log.info("Was expecting a list type response. Invalid type returned.");
                        // What to do???
                        if(listResponseType != null) {
                            if(listResponseType.equals(ListResponseType.LIST)) {
                                // This is error.
                                log.warning("The response should have been a list. Invalid type returned.");
                            } else {
                                // assert listResponseType.getType() == ListResponseType.TYPE_MAP;
                                if(payload instanceof Map<?,?>) {
                                    String listKey = listResponseType.getListKey();
                                    @SuppressWarnings("unchecked")
                                    Map<String,Object> objectListMap = (Map<String,Object>) payload;
                                    if(objectListMap != null) {
                                        @SuppressWarnings("unchecked")
                                        List<Object> objectList = (List<Object>) objectListMap.get(listKey);
                                        list = objectList;
                                    }
                                } else {
                                    // This is error. Can this happen????
                                    log.warning("The response is neither a map nor a list. Unrecognized type returned.");
                                }
                            }
                        }
                        if(list == null) {
                            // Last resort....
                            List<Object> objectList = new ArrayList<Object>(Arrays.asList(payload));
                            list = objectList;
                        }
                    }                
                } else {
                    @SuppressWarnings("unchecked")
                    Map<String,String> error = (Map<String,String>) response.get(ResponseUtil.KEY_ERROR);
                    String resource = (String) response.get(ResponseUtil.KEY_RESOURCE);
                    String location = (String) response.get(ResponseUtil.KEY_LOCATION);
                    // Throw exception.
                    RestApiException exception = null;
                    if(error != null) {
                        String errorCode = error.get(ResponseUtil.KEY_ERROR_CODE);        // ???
                        String errorMessage = error.get(ResponseUtil.KEY_ERROR_MESSAGE);
                        exception = StatusExceptionMapper.buildException(errorMessage, null, resource, statusCode, location);
                    } else {
                        exception = new RestApiException("Unknown error: Failed to get() the resources.");
                    }
                    throw exception;
                }
            } else {
                // ???
                throw new RestApiException("Failed to get() the resources.");
            }
        } else {
            throw new MethodNotAllowedRaException("Method, " + CrudMethod.GET_LIST + ", not supporeted.");
        }
        return list;
    }


    @Override
    public List<String> keys(UserCredential credential,  Map<String, Object> params) throws RestApiException, IOException
    {
        if(crudMethodFilter.isMethodSupported(CrudMethod.GET_KEYS)) {
            // TBD:
            // This is only possible if the Web service supports such functionality.
            // ????
            // return null;
            throw new NotImplementedRaException("keys() is not supported yet.");
        } else {
            throw new MethodNotAllowedRaException("Method, " + CrudMethod.GET_KEYS + ", not supporeted.");
        }
    }


    @Override
    public Object create(UserCredential credential, Object inputData) throws RestApiException, IOException
    {
        return create(credential, inputData, null);
    }

    @Override
    public Object create(UserCredential credential, Object inputData, String id) throws RestApiException, IOException
    {
        Object object = null;
        if(crudMethodFilter.isMethodSupported(CrudMethod.CREATE_ITEM)) {
            Map<String,Object> response = null;
            if(id == null || id.isEmpty()) {
                response = getRestServiceClient().post(credential, inputData);
            } else {
                response = getRestServiceClient().put(credential, inputData, id);
            }
            if(response != null) {
                int statusCode = (Integer) response.get(ResponseUtil.KEY_STATUSCODE);
                if(StatusCode.isSuccessful(statusCode)) {
                    Object payload = response.get(ResponseUtil.KEY_PAYLOAD);
                    object = payload;
                } else {
                    @SuppressWarnings("unchecked")
                    Map<String,String> error = (Map<String,String>) response.get(ResponseUtil.KEY_ERROR);
                    String resource = (String) response.get(ResponseUtil.KEY_RESOURCE);
                    String location = (String) response.get(ResponseUtil.KEY_LOCATION);
                    // Throw exception.
                    RestApiException exception = null;
                    if(error != null) {
                        String errorCode = error.get(ResponseUtil.KEY_ERROR_CODE);        // ???
                        String errorMessage = error.get(ResponseUtil.KEY_ERROR_MESSAGE);
                        exception = StatusExceptionMapper.buildException(errorMessage, null, resource, statusCode, location);
                    } else {
                        exception = new RestApiException("Unknown error: Failed to post() to the resource for id = " + id);
                    }
                    throw exception;
                }
            } else {
                // ???
                throw new RestApiException("Failed to post() to the resource for id = " + id);
            }
        } else {
            throw new MethodNotAllowedRaException("Method, " + CrudMethod.CREATE_ITEM + ", not supporeted.");
        }
        return object;
    }

    @Override
    public Object update(UserCredential credential, Object inputData, String id) throws RestApiException, IOException
    {
        Object object = null;
        if(crudMethodFilter.isMethodSupported(CrudMethod.UPDATE_ITEM)) {
            Map<String,Object> response = null;
            if(id == null || id.isEmpty()) {
                // error.
                throw new RestApiClientException("Resource id is missing.");
            } else {
                response = getRestServiceClient().put(credential, inputData, id);
            }
            if(response != null) {
                int statusCode = (Integer) response.get(ResponseUtil.KEY_STATUSCODE);
                if(StatusCode.isSuccessful(statusCode)) {
                    Object payload = response.get(ResponseUtil.KEY_PAYLOAD);
                    object = payload;
                } else {
                    @SuppressWarnings("unchecked")
                    Map<String,String> error = (Map<String,String>) response.get(ResponseUtil.KEY_ERROR);
                    String resource = (String) response.get(ResponseUtil.KEY_RESOURCE);
                    String location = (String) response.get(ResponseUtil.KEY_LOCATION);
                    // Throw exception.
                    RestApiException exception = null;
                    if(error != null) {
                        String errorCode = error.get(ResponseUtil.KEY_ERROR_CODE);        // ???
                        String errorMessage = error.get(ResponseUtil.KEY_ERROR_MESSAGE);
                        exception = StatusExceptionMapper.buildException(errorMessage, null, resource, statusCode, location);
                    } else {
                        exception = new RestApiException("Unknown error: Failed to put() to the resource for id = " + id);
                    }
                    throw exception;
                }
            } else {
                // ???
                throw new RestApiException("Failed to put() to the resource for id = " + id);
            }
        } else {
            throw new MethodNotAllowedRaException("Method, " + CrudMethod.UPDATE_ITEM + ", not supporeted.");
        }
        return object;
    }


    @Override
    public Object modify(UserCredential credential, Object partialData, String id) throws RestApiException, IOException
    {
        Object object = null;
        if(crudMethodFilter.isMethodSupported(CrudMethod.MODIFY_ITEM)) {
            Map<String,Object> response = null;
            if(id == null || id.isEmpty()) {
                // error.
                throw new RestApiClientException("Resource id is missing.");
            } else {
                response = getRestServiceClient().patch(credential, partialData, id);
            }
            if(response != null) {
                int statusCode = (Integer) response.get(ResponseUtil.KEY_STATUSCODE);
                if(StatusCode.isSuccessful(statusCode)) {
                    Object payload = response.get(ResponseUtil.KEY_PAYLOAD);
                    object = payload;
                } else {
                    @SuppressWarnings("unchecked")
                    Map<String,String> error = (Map<String,String>) response.get(ResponseUtil.KEY_ERROR);
                    String resource = (String) response.get(ResponseUtil.KEY_RESOURCE);
                    String location = (String) response.get(ResponseUtil.KEY_LOCATION);
                    // Throw exception.
                    RestApiException exception = null;
                    if(error != null) {
                        String errorCode = error.get(ResponseUtil.KEY_ERROR_CODE);        // ???
                        String errorMessage = error.get(ResponseUtil.KEY_ERROR_MESSAGE);
                        exception = StatusExceptionMapper.buildException(errorMessage, null, resource, statusCode, location);
                    } else {
                        exception = new RestApiException("Unknown error: Failed to put() to the resource for id = " + id);
                    }
                    throw exception;
                }
            } else {
                // ???
                throw new RestApiException("Failed to put() to the resource for id = " + id);
            }
        } else {
            throw new MethodNotAllowedRaException("Method, " + CrudMethod.MODIFY_ITEM + ", not supporeted.");
        }
        return object;
    }

    @Override
    public boolean delete(UserCredential credential, String id) throws RestApiException, IOException
    {
        boolean suc = false;
        if(crudMethodFilter.isMethodSupported(CrudMethod.DELETE_ITEM)) {
            Map<String,Object> response = null;
            if(id == null || id.isEmpty()) {
                // error.
                throw new RestApiClientException("Resource id is missing.");
            } else {
                response = getRestServiceClient().delete(credential, id, null);
            }
            if(response != null) {
                int statusCode = (Integer) response.get(ResponseUtil.KEY_STATUSCODE);
                if(StatusCode.isSuccessful(statusCode)) {
                    suc = true;
                } else {
                    @SuppressWarnings("unchecked")
                    Map<String,String> error = (Map<String,String>) response.get(ResponseUtil.KEY_ERROR);
                    String resource = (String) response.get(ResponseUtil.KEY_RESOURCE);
                    String location = (String) response.get(ResponseUtil.KEY_LOCATION);
                    // Throw exception.
                    RestApiException exception = null;
                    if(error != null) {
                        String errorCode = error.get(ResponseUtil.KEY_ERROR_CODE);        // ???
                        String errorMessage = error.get(ResponseUtil.KEY_ERROR_MESSAGE);
                        exception = StatusExceptionMapper.buildException(errorMessage, null, resource, statusCode, location);
                    } else {
                        exception = new RestApiException("Unknown error: Failed to delete() the resource for id = " + id);
                    }
                    throw exception;
                }
            } else {
                // ???
                throw new RestApiException("Failed to delete() the resource for id = " + id);
            }
        } else {
            throw new MethodNotAllowedRaException("Method, " + CrudMethod.DELETE_ITEM + ", not supporeted.");
        }
        return suc;
    }

    @Override
    public int delete(UserCredential credential, Map<String, Object> params) throws RestApiException, IOException
    {
        int count = 0;
        if(crudMethodFilter.isMethodSupported(CrudMethod.DELETE_LIST)) {
            Map<String,Object> response = getRestServiceClient().delete(credential, null, params);
            if(response != null) {
                int statusCode = (Integer) response.get(ResponseUtil.KEY_STATUSCODE);
                if(StatusCode.isSuccessful(statusCode)) {
                    // ????
                    count = 0; 
                } else {
                    @SuppressWarnings("unchecked")
                    Map<String,String> error = (Map<String,String>) response.get(ResponseUtil.KEY_ERROR);
                    String resource = (String) response.get(ResponseUtil.KEY_RESOURCE);
                    String location = (String) response.get(ResponseUtil.KEY_LOCATION);
                    // Throw exception.
                    RestApiException exception = null;
                    if(error != null) {
                        String errorCode = error.get(ResponseUtil.KEY_ERROR_CODE);        // ???
                        String errorMessage = error.get(ResponseUtil.KEY_ERROR_MESSAGE);
                        exception = StatusExceptionMapper.buildException(errorMessage, null, resource, statusCode, location);
                    } else {
                        exception = new RestApiException("Unknown error: Failed to delete() the resources");
                    }
                    throw exception;
                }
            } else {
                // ???
                throw new RestApiException("Failed to delete() the resources");
            }
        } else {
            throw new MethodNotAllowedRaException("Method, " + CrudMethod.DELETE_LIST + ", not supporeted.");
        }
        return count;
    }


    @Override
    public String toString()
    {
        return "AbstractApiServiceClient [restServiceClient="
                + restServiceClient + ", apiServiceClientMaker="
                + apiServiceClientMaker + ", crudMethodFilter="
                + crudMethodFilter + ", listResponseType=" + listResponseType
                + ", authRefreshPolicy=" + authRefreshPolicy
                + ", requestRetryPolicy=" + requestRetryPolicy
                + ", clientCachePolicy=" + clientCachePolicy
                + ", autoRedirectPolicy=" + autoRedirectPolicy + "]";
    }

    @Override
    public CacheControlPolicy getCacheControlPolicy()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCacheControlPolicy(CacheControlPolicy cacheControlPolicy)
    {
        // TODO Auto-generated method stub
        
    }


}
