package restclient.impl.base;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.credential.ClientCredential;
import restclient.impl.AbstractServiceClient;


public final class DefaultServiceClient extends AbstractServiceClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public DefaultServiceClient()
    {
        this(null);
    }

    public DefaultServiceClient(ClientCredential clientCredential)
    {
        super(clientCredential);
    }

}
