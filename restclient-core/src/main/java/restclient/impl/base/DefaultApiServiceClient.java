package restclient.impl.base;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.impl.AbstractApiServiceClient;
import restclient.maker.ApiServiceClientMaker;
import restclient.maker.impl.base.DefaultApiServiceClientMaker;


public final class DefaultApiServiceClient extends AbstractApiServiceClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public DefaultApiServiceClient(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }


    // Factory methods

    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return DefaultApiServiceClientMaker.getInstance();
    }

}
