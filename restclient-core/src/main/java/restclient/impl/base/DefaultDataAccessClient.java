package restclient.impl.base;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.impl.AbstractDataAccessClient;


public final class DefaultDataAccessClient extends AbstractDataAccessClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultDataAccessClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public DefaultDataAccessClient()
    {
        super();
    }

    
}
