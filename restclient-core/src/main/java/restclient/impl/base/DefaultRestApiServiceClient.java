package restclient.impl.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import restclient.common.AuthRefreshPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.RequestRetryPolicy;
import restclient.credential.ClientCredential;
import restclient.credential.UserCredential;
import restclient.impl.AbstractRestApiServiceClient;


public final class DefaultRestApiServiceClient extends AbstractRestApiServiceClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultRestApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultRestApiServiceClient()
    {
        // TODO Auto-generated constructor stub
    }

}
