package restclient.impl.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import restclient.RestApiServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.RequestRetryPolicy;
import restclient.credential.UserCredential;
import restclient.impl.AbstractRestApiUserClient;


public final class DefaultRestApiUserClient extends AbstractRestApiUserClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultRestApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public DefaultRestApiUserClient(RestApiServiceClient restApiServiceClient)
    {
        super(restApiServiceClient);
        // TODO Auto-generated constructor stub
    }

 

}
