package restclient.impl.base;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.common.AuthRefreshPolicy;
import restclient.common.impl.base.DefaultAuthRefreshPolicy;
import restclient.credential.UserCredential;
import restclient.impl.AbstractUserClient;


public final class DefaultUserClient extends AbstractUserClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultUserClient()
    {
        this(null);
    }

    public DefaultUserClient(UserCredential userCredential)
    {
        super(userCredential);
    }

    
    // Factory method.
    protected AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new DefaultAuthRefreshPolicy();
    }

}
