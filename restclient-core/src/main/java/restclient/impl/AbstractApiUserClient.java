package restclient.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ApiUserClient;
import restclient.FlexibleResourceClient;
import restclient.FlexibleUserClient;
import restclient.RestApiException;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.RequestRetryPolicy;
import restclient.credential.UserCredential;
import restclient.maker.ApiUserClientMaker;
import restclient.maker.impl.AbstractApiUserClientMaker;


// Base class for user-specific "api client".
public abstract class AbstractApiUserClient implements ApiUserClient, FlexibleResourceClient, FlexibleUserClient
{
    private static final Logger log = Logger.getLogger(AbstractApiUserClient.class.getName());

    // Abstract factory.
    private ApiUserClientMaker apiUserClientMaker;

    // Embedded REST Client.
    private final ApiServiceClient apiServiceClient;

    // private RestUserClient restUserClient;
    private UserCredential userCredential;


    public AbstractApiUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public AbstractApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        super();

        // Abstract factory.
        apiUserClientMaker = makeApiUserClientMaker();

        // All relevant methods will be delegated to the service client. 
        this.apiServiceClient = makeServiceClient(resourceBaseUrl);

        // TBD:
        init();
    }
    
    public AbstractApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public AbstractApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        super();

        // Abstract factory.
        apiUserClientMaker = makeApiUserClientMaker();

        // All relevant methods will be delegated to the service client. 
        // This cannot be null.
        this.apiServiceClient = apiServiceClient;

        // TBD:
        init();
    }
        
//    public AbstractApiUserClient(String resourceBaseUrl)
//    {
//        this(new AbstractResourceUrlBuilder(resourceBaseUrl) {});
//    }
//    public AbstractApiUserClient(ResourceUrlBuilder resourceUrlBuilder) 
//    {
//        this((ApiServiceClient) new AbstractApiServiceClient(resourceUrlBuilder) {});
//    }
//    public AbstractApiUserClient(ApiServiceClient apiServiceClient)
//    {
//        this(apiServiceClient, null);
//    }
//    public AbstractApiUserClient(ApiServiceClient apiServiceClient,
//            UserCredential userCredential)
//    {
//        super();
////      restUserClientFactory = new AbstractRestUserClientFactory() {};
//      apiServiceClientFactory = new AbstractApiServiceClientFactory() {};
//
//      this.apiServiceClient = apiServiceClient;
//        this.userCredential = userCredential;
//
//        // TBD:
//        init();
//    }

    protected void init()
    {
        // Place holder
    }


    //  @Override
    //  public RestUserClient createRestClient(String resourceBaseUrl)
    //  {
    //      RestUserClient restUserClient = (RestUserClient) restUserClientFactory.createClient(resourceBaseUrl);
    //      return restUserClient;
    //  }
    

    // Getter only.    
    protected ApiServiceClient getApiServiceClient()
    {
        return apiServiceClient;
    }
    protected FlexibleResourceClient getFlexibleApiServiceClient()
    {
        return (FlexibleResourceClient) apiServiceClient;
    }

    
    // Factory methods

    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return createApiUserClientMaker();
    }
    private static ApiUserClientMaker createApiUserClientMaker()
    {
        return AbstractApiUserClientMaker.getInstance();
    }

    protected ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return apiUserClientMaker.makeServiceClient(resourceBaseUrl);
    }


    @Override
    public boolean isMethodSupported(String methodName)
    {
        return getApiServiceClient().isMethodSupported(methodName);
    }


    @Override
    public UserCredential getUserCredential()
    {
        return userCredential;
    }
    public void setUserCredential(UserCredential userCredential)
    {
        this.userCredential = userCredential;
    }

    // temporary
    public boolean isAccessAllowed()
    {
        return getApiServiceClient().isAccessAllowed(userCredential);
    }
    // ...    


    @Override
    public String getResourceBaseUrl()
    {
        return getApiServiceClient().getResourceBaseUrl();
    }
//    @Override
//    public void setResourceBaseUrl(String resourceBaseUrl)
//    {
//        getApiServiceClient().setResourceBaseUrl(resourceBaseUrl);
//    }


    public AuthRefreshPolicy getRestServiceAuthRefreshPolicy()
    {
        return ((AbstractApiServiceClient) getApiServiceClient()).getRestServiceAuthRefreshPolicy();
    }
    public void setRestServiceAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy)
    {
        ((AbstractApiServiceClient) getApiServiceClient()).setRestServiceAuthRefreshPolicy(authRefreshPolicy);
    }

    public RequestRetryPolicy getRestServiceRequestRetryPolicy()
    {
        return ((AbstractApiServiceClient) getApiServiceClient()).getRestServiceRequestRetryPolicy();
    }
    public void setRestServiceRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy)
    {
        ((AbstractApiServiceClient) getApiServiceClient()).setRestServiceRequestRetryPolicy(requestRetryPolicy);
    }

    public ClientCachePolicy getRestServiceClientCachePolicy()
    {
        return ((AbstractApiServiceClient) getApiServiceClient()).getRestServiceClientCachePolicy();
    }
    public void setRestServiceClientCachePolicy(ClientCachePolicy clientCachePolicy)
    {
        ((AbstractApiServiceClient) getApiServiceClient()).setRestServiceClientCachePolicy(clientCachePolicy);
    }

    public AutoRedirectPolicy getRestServiceAutoRedirectPolicy()
    {
        return ((AbstractApiServiceClient) getApiServiceClient()).getRestServiceAutoRedirectPolicy();
    }
    public void setRestServiceAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy)
    {
        ((AbstractApiServiceClient) getApiServiceClient()).setRestServiceAutoRedirectPolicy(autoRedirectPolicy);
    }

    
    
    @Override
    public AuthRefreshPolicy getAuthRefreshPolicy()
    {
        return getApiServiceClient().getAuthRefreshPolicy();
    }
    @Override
    public void setAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy)
    {
        getFlexibleApiServiceClient().setAuthRefreshPolicy(authRefreshPolicy);
    }

    @Override
    public RequestRetryPolicy getRequestRetryPolicy()
    {
        return getApiServiceClient().getRequestRetryPolicy();
    }
    @Override
    public void setRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy)
    {
        getFlexibleApiServiceClient().setRequestRetryPolicy(requestRetryPolicy);
    }

    @Override
    public ClientCachePolicy getClientCachePolicy()
    {
        return getApiServiceClient().getClientCachePolicy();
    }
    @Override
    public void setClientCachePolicy(ClientCachePolicy clientCachePolicy)
    {
        getFlexibleApiServiceClient().setClientCachePolicy(clientCachePolicy);
    }

    @Override
    public CacheControlPolicy getCacheControlPolicy()
    {
        return getApiServiceClient().getCacheControlPolicy();
    }
    @Override
    public void setCacheControlPolicy(CacheControlPolicy cacheControlPolicy)
    {
        getFlexibleApiServiceClient().setCacheControlPolicy(cacheControlPolicy);
    }

    @Override
    public AutoRedirectPolicy getAutoRedirectPolicy()
    {
        return getApiServiceClient().getAutoRedirectPolicy();
    }
    @Override
    public void setAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy)
    {
        getFlexibleApiServiceClient().setAutoRedirectPolicy(autoRedirectPolicy);
    }


    @Override
    public Object get(String id) throws RestApiException, IOException
    {
        return getApiServiceClient().get(getUserCredential(), id);
    }

    @Override
    public List<Object> list(Map<String, Object> params) throws RestApiException, IOException
    {
        return getApiServiceClient().list(getUserCredential(), params);
    }

    @Override
    public List<String> keys(Map<String, Object> params) throws RestApiException, IOException
    {
        return getApiServiceClient().keys(getUserCredential(), params);
    }

    @Override
    public Object create(Object inputData) throws RestApiException, IOException
    {
        return getApiServiceClient().create(getUserCredential(), inputData);
    }

    @Override
    public Object create(Object inputData, String id) throws RestApiException, IOException
    {
        return getApiServiceClient().create(getUserCredential(), inputData, id);
    }

    @Override
    public Object update(Object inputData, String id) throws RestApiException, IOException
    {
        return getApiServiceClient().update(getUserCredential(), inputData, id);
    }

    @Override
    public Object modify(Object partialData, String id) throws RestApiException, IOException
    {
        return getApiServiceClient().modify(getUserCredential(), partialData, id);
    }

    @Override
    public boolean delete(String id) throws RestApiException, IOException
    {
        return getApiServiceClient().delete(getUserCredential(), id);
    }

    @Override
    public int delete(Map<String, Object> params) throws RestApiException, IOException
    {
        return getApiServiceClient().delete(getUserCredential(), params);
    }

    
    @Override
    public String toString()
    {
        return "AbstractApiUserClient [apiUserClientMaker="
                + apiUserClientMaker + ", apiServiceClient=" + apiServiceClient
                + ", userCredential=" + userCredential + "]";
    }
    

}
