package restclient.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownServiceException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.FlexibleResourceClient;
import restclient.FlexibleServiceClient;
import restclient.RestApiException;
import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.DataAccessClient;
import restclient.common.HttpMethodFilter;
import restclient.common.RequestRetryPolicy;
import restclient.common.ResourceUrlBuilder;
import restclient.core.AuthMethod;
import restclient.core.ContentFormat;
import restclient.core.HttpMethod;
import restclient.core.StatusCode;
import restclient.credential.ClientCredential;
import restclient.credential.DataAccessCredential;
import restclient.credential.UserCredential;
import restclient.credential.impl.AbstractUserCredential;
import restclient.helper.ClientDebugHelper;
import restclient.maker.RestServiceClientMaker;
import restclient.maker.impl.AbstractRestServiceClientMaker;
import restclient.util.JsonUtil;
import restclient.util.ResourceUrlUtil;
import restclient.util.ResponseUtil;
import restclient.util.StringUtil;



// Base class for provider-specific "REST client".
// "REST client" encapsulate various information/logic relevant to making remote Web services calls.
// Note: 
// An instance of RestClient is used across multiple users.
//    --> Don't store any user-specific info in this class.
public abstract class AbstractRestServiceClient implements RestServiceClient, FlexibleResourceClient, FlexibleServiceClient, ResourceUrlBuilder
{
    private static final Logger log = Logger.getLogger(AbstractRestServiceClient.class.getName());
    
    // Optional
    // private String name;
    // private String description;

    // RestClient is associated with one and only one (unchangeable) web services endpoint URL.
    // The URL "defines" a resource.
    private final String resourceBaseUrl;
    private final ResourceUrlBuilder resourceUrlBuilder;
//    private final ResourceUrlBuilder resourceUrlBuilder;
    
    // Abstract factory.
    private final RestServiceClientMaker restServiceClientMaker;

    // which method is this client (actually, more for the server) supporting?
    private final HttpMethodFilter httpMethodFilter;
    
    // Master flag.
    // Note: This is not authCredential.authRequired.
    private boolean authCredentialRequired; 
    
    // Each method, get/post/put/delete, takes AuthCredential as an argument.
    // This "default" value is used only when the arg is null.
    // This is useful primarily when the auth is client based (rather than per-user based).
    private UserCredential defaultAuthCredential;
    
    // Client auth credential.
    // This is primarily used for OAuth. (consumer key, and consumer secret)
    private ClientCredential clientCredential;

    // ...
    private DataAccessClient dataAccessClient;

    // Default values.
    // e.g., JSON or XML, etc.
    // These values are shared for all HTTP methods (no separate values supported).
    private String requestFormat;
    private String responseFormat;
    
    // MIME Type. These values should be "consistent" with the "format" values.
    // private String inputMimeType; 
    // private String outputMimeType; 

    // Connection timeout.
    private int timeoutSeconds;          // In seconds
    
//    private boolean followRedirect;      // for GET only....
//    private int maxFollow;


    // Auth refresh/Cache/Retry policies, ...
    private AuthRefreshPolicy authRefreshPolicy;
    private RequestRetryPolicy requestRetryPolicy;
    private ClientCachePolicy clientCachePolicy;     // This is for the client side caching. (Not, HTTP Cache control.)
    private CacheControlPolicy cacheControlPolicy;   // This is for HTTP cache control...
    private AutoRedirectPolicy autoRedirectPolicy;
    
    // etc...
    

    public AbstractRestServiceClient(String resourceBaseUrl)
    {
        this.resourceBaseUrl = resourceBaseUrl;
        restServiceClientMaker = makeRestServiceClientMaker();

        // Default values through factory methods
        this.resourceUrlBuilder = makeResourceUrlBuilder(resourceBaseUrl);
        this.httpMethodFilter = makeHttpMethodFilter();
        this.dataAccessClient = makeDataAccessClient();
        this.authRefreshPolicy = makeAuthRefreshPolicy();
        this.requestRetryPolicy = makeRequestRetryPolicy();
        this.clientCachePolicy = makeClientCachePolicy();
        this.cacheControlPolicy = makeCacheControlPolicy();
        this.autoRedirectPolicy = makeAutoRedirectPolicy();

        init();
    }

//    public AbstractRestServiceClient(String resourceBaseUrl)
//    {
//        this(new AbstractResourceUrlBuilder(resourceBaseUrl) {});
//    }
//    public AbstractRestServiceClient(ResourceUrlBuilder resourceUrlBuilder)
//    {
//        if(resourceUrlBuilder == null) {
//            // Error.
//            // What to do ???
//            this.resourceUrlBuilder = new DefaultResourceUrlBuilder(null);
//        } else {
//            // TBD: Validation ???
//            this.resourceUrlBuilder = resourceUrlBuilder;
//        }
//        // ...
//
//        // TBD:
//        init();
//    }

    protected void init()
    {
        // Default values.


        authCredentialRequired = true;  // ???
        this.defaultAuthCredential = new AbstractUserCredential() {};   // ???
        // this.clientCredential = new AbstractClientCredential() {};
        this.clientCredential = null;     // ???
        
        // We only support JSON formats.
        requestFormat = ContentFormat.JSON;
        responseFormat = ContentFormat.JSON;
        timeoutSeconds = 10;   // ???
//        followRedirect = false;
//        maxFollow = 0;

    }

    
    public ResourceUrlBuilder getResourceUrlBuilder()
    {
        return resourceUrlBuilder;
    }

    public HttpMethodFilter getHttpMethodFilter()
    {
        return httpMethodFilter;
    }
//    public void setHttpMethodFilter(HttpMethodFilter httpMethodFilter)
//    {
//        this.httpMethodFilter = httpMethodFilter;
//    }


    @Override
    public boolean isMethodSupported(String methodName)
    {
        return httpMethodFilter.isMethodSupported(methodName);
    }

    
    // No setters.
    
    @Override
    public String getResourceBaseUrl()
    {
        return resourceUrlBuilder.getResourceBaseUrl();
    }
//    @Override
//    public void setResourceBaseUrl(String resourceBaseUrl)
//    {
//        resourceUrlBuilder.setResourceBaseUrl(resourceBaseUrl);        
//    }

    @Override
    public String getResourceGetUrl(String id, Map<String, Object> params)
    {
        return resourceUrlBuilder.getResourceGetUrl(id, params);
    }
    @Override
    public String getResourcePostUrl()
    {
        return resourceUrlBuilder.getResourcePostUrl();
    }
    @Override
    public String getResourcePutUrl(String id)
    {
        return resourceUrlBuilder.getResourcePutUrl(id);
    }
    @Override
    public String getResourcePatchUrl(String id)
    {
        return resourceUrlBuilder.getResourcePatchUrl(id);
    }
    @Override
    public String getResourceDeleteUrl(String id, Map<String, Object> params)
    {
        return resourceUrlBuilder.getResourceDeleteUrl(id, params);
    }

    
    // Factory methods

    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return createRestServiceClientMaker();
    }
    private static RestServiceClientMaker createRestServiceClientMaker()
    {
        return AbstractRestServiceClientMaker.getInstance();
    }

    protected ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return restServiceClientMaker.makeResourceUrlBuilder(resourceBaseUrl);
    }

    protected HttpMethodFilter makeHttpMethodFilter()
    {
        return restServiceClientMaker.makeHttpMethodFilter();
    }

    protected DataAccessClient makeDataAccessClient()
    {
        return restServiceClientMaker.makeDataAccessClient();
    }

    protected AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return restServiceClientMaker.makeAuthRefreshPolicy();
    }
    protected RequestRetryPolicy makeRequestRetryPolicy()
    {
        return restServiceClientMaker.makeRequestRetryPolicy();
    }
    protected ClientCachePolicy makeClientCachePolicy()
    {
        return restServiceClientMaker.makeClientCachePolicy();
    }
    protected CacheControlPolicy makeCacheControlPolicy()
    {
        return restServiceClientMaker.makeCacheControlPolicy();
    }
    protected AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return restServiceClientMaker.makeAutoRedirectPolicy();
    }
    
    

    public boolean isAuthCredentialRequired()
    {
        return authCredentialRequired;
    }
    public void setAuthCredentialRequired(boolean authCredentialRequired)
    {
        this.authCredentialRequired = authCredentialRequired;
    }

    public UserCredential getDefaultAuthCredential()
    {
        return defaultAuthCredential;
    }
    public void setDefaultAuthCredential(UserCredential defaultAuthCredential)
    {
        this.defaultAuthCredential = defaultAuthCredential;
    }

    @Override
    public ClientCredential getClientCredential()
    {
        return clientCredential;
    }
    public void setClientCredential(ClientCredential clientCredential)
    {
        this.clientCredential = clientCredential;
    }

    
    @Override
    public Set<String> getRequiredScopes()
    {
        return dataAccessClient.getRequiredScopes();
    }
    public boolean addRequiredScope(String scope)
    {
        return ((AbstractDataAccessClient) dataAccessClient).addRequiredScope(scope);
    }
    public boolean addRequiredScopes(Collection<String> scopes)
    {
        return ((AbstractDataAccessClient) dataAccessClient).addRequiredScopes(scopes);
    }
    public boolean setRequiredScopes(Collection<String> scopes)
    {
        return ((AbstractDataAccessClient) dataAccessClient).setRequiredScopes(scopes);
    }
    public boolean removeRequiredScope(String scope)
    {
        return ((AbstractDataAccessClient) dataAccessClient).removeRequiredScope(scope);
    }
    public boolean removeRequiredScopes(Collection<String> scopes)
    {
        return ((AbstractDataAccessClient) dataAccessClient).removeRequiredScopes(scopes);
    }
    public void clearRequiredScopes()
    {
        ((AbstractDataAccessClient) dataAccessClient).clearRequiredScopes();
    }

    @Override
    public boolean requiresScope(String scope)
    {
        return dataAccessClient.requiresScope(scope);
    }

    @Override
    public boolean isAccessAllowed(DataAccessCredential dataAccessCredential)
    {
        return dataAccessClient.isAccessAllowed(dataAccessCredential);
    }
    

    public String getRequestFormat()
    {
        return requestFormat;
    }
    public void setRequestFormat(String requestFormat)
    {
        this.requestFormat = requestFormat;
    }

    public String getResponseFormat()
    {
        return responseFormat;
    }
    public void setResponseFormat(String responseFormat)
    {
        this.responseFormat = responseFormat;
    }

    public int getTimeoutSeconds()
    {
        return timeoutSeconds;
    }
    public void setTimeoutSeconds(int timeoutSeconds)
    {
        this.timeoutSeconds = timeoutSeconds;
    }

//    public boolean isFollowRedirect()
//    {
//        return followRedirect;
//    }
//    public void setFollowRedirect(boolean followRedirect)
//    {
//        this.followRedirect = followRedirect;
//    }
//
//    public int getMaxFollow()
//    {
//        return maxFollow;
//    }
//    public void setMaxFollow(int maxFollow)
//    {
//        this.maxFollow = maxFollow;
//    }




    @Override
    public AuthRefreshPolicy getAuthRefreshPolicy()
    {
        return authRefreshPolicy;
    }
    @Override
    public void setAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy)
    {
        this.authRefreshPolicy = authRefreshPolicy;
    }

    @Override
    public RequestRetryPolicy getRequestRetryPolicy()
    {
        return requestRetryPolicy;
    }
    @Override
    public void setRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy)
    {
        this.requestRetryPolicy = requestRetryPolicy;
    }

    @Override
    public ClientCachePolicy getClientCachePolicy()
    {
        return clientCachePolicy;
    }
    @Override
    public void setClientCachePolicy(ClientCachePolicy clientCachePolicy)
    {
        this.clientCachePolicy = clientCachePolicy;
    }

    @Override
    public CacheControlPolicy getCacheControlPolicy()
    {
        return cacheControlPolicy;
    }
    @Override
    public void setCacheControlPolicy(CacheControlPolicy cacheControlPolicy)
    {
        this.cacheControlPolicy = cacheControlPolicy;
    }

    @Override
    public AutoRedirectPolicy getAutoRedirectPolicy()
    {
        return autoRedirectPolicy;
    }
    @Override
    public void setAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy)
    {
        this.autoRedirectPolicy = autoRedirectPolicy;
    }

    
    @Override
    public Map<String, Object> get(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        Map<String, Object> response = null;
        if(httpMethodFilter.isMethodSupported(HttpMethod.GET)) {
            response = process(HttpMethod.GET, credential, null, id, params);            
        } else {
            // What to do???
            throw new UnknownServiceException("Method, " + HttpMethod.GET + ", not supporeted.");
        }
        return response;
    }

    @Override
    public Map<String, Object> post(UserCredential credential, Object inputData) throws IOException
    {
        Map<String, Object> response = null;
        if(httpMethodFilter.isMethodSupported(HttpMethod.POST)) {
            response = process(HttpMethod.POST, credential, inputData, null, null);
        } else {
            // What to do???
            throw new UnknownServiceException("Method, " + HttpMethod.POST + ", not supporeted.");
        }
        return response;
    }

    @Override
    public Map<String, Object> put(UserCredential credential, Object inputData,
            String id) throws IOException
    {
        Map<String, Object> response = null;
        if(httpMethodFilter.isMethodSupported(HttpMethod.PUT)) {
            response = process(HttpMethod.PUT, credential, inputData, id, null);
        } else {
            // What to do???
            throw new UnknownServiceException("Method, " + HttpMethod.PUT + ", not supporeted.");
        }
        return response;
    }

    @Override
    public Map<String, Object> patch(UserCredential credential,
            Object partialData, String id) throws IOException
    {
        Map<String, Object> response = null;
        if(httpMethodFilter.isMethodSupported(HttpMethod.PATCH)) {
            response = process(HttpMethod.PATCH, credential, partialData, id, null);
        } else {
            // What to do???
            throw new UnknownServiceException("Method, " + HttpMethod.PATCH + ", not supporeted.");
        }
        return response;
    }


    @Override
    public Map<String, Object> delete(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        Map<String, Object> response = null;
        if(httpMethodFilter.isMethodSupported(HttpMethod.DELETE)) {
            response = process(HttpMethod.DELETE, credential, null, id, params);
        } else {
            // What to do???
            throw new UnknownServiceException("Method, " + HttpMethod.DELETE + ", not supporeted.");
        }
        return response;
    }

    
    // Main implementation
    // Using JDK URLConnection for now.
    // (TBD: Use Apache HttpClient ???)
    // Method arg should be all caps.
    protected Map<String, Object> process(String method, UserCredential userCredential, Object inputData, String id,
            Map<String, Object> params) throws IOException
    {
        return process(method, userCredential, inputData, id, params, false);
    }
    protected Map<String, Object> process(String method, UserCredential userCredential, Object inputData, String id,
            Map<String, Object> params, boolean retrying) throws IOException
    {
        String endpointUrl = null;
        boolean doOutput = false;    // request
        boolean doInput = false;     // response
        String postData = null;
        // boolean expectingListPayload = false;
        // switch(method) {
        // case HttpMethod.GET:
        if(HttpMethod.GET.equals(method)) {
            endpointUrl = getResourceGetUrl(id, params);
            doOutput = false;
            doInput = true;
            // postData = null;
            // if(id != null) {
            //     expectingListPayload = false;
            // } else {
            //     expectingListPayload = true;
            // }
            // break;
        // case HttpMethod.POST:
        } else if(HttpMethod.POST.equals(method)) {
            endpointUrl = getResourcePostUrl();
            doOutput = true;
            doInput = true;
            // postData =   
            // expectingListPayload = false;
            // break;
        // case HttpMethod.PUT:
        } else if(HttpMethod.PUT.equals(method)) {
            endpointUrl = getResourcePutUrl(id);
            doOutput = true;
            doInput = true;
            // postData =   
            // expectingListPayload = false;
            // break;
        // case HttpMethod.PATCH:
        } else if(HttpMethod.PATCH.equals(method)) {
            endpointUrl = getResourcePatchUrl(id);
            doOutput = true;
            doInput = true;
            // postData =   
            // expectingListPayload = false;
            // break;
        // case HttpMethod.DELETE:
        } else if(HttpMethod.DELETE.equals(method)) {
            endpointUrl = getResourceDeleteUrl(id, params);
            doOutput = false;
            doInput = true;
            // postData = null;
            // expectingListPayload = false;
            // break;
        // default:
        } else {
            // error
            // what to do?                
        }
        // temporary
        if(log.isLoggable(Level.INFO)) log.info("postData = " + postData + " for endpointUrl, " + endpointUrl);
        // temporary
        
        Map<String, Object> response = null;
        try {
            URL refreshTokenEndpointURL = new URL(endpointUrl);
            HttpURLConnection conn = (HttpURLConnection) refreshTokenEndpointURL.openConnection();
            conn.setRequestMethod(method);

            // conn.setInstanceFollowRedirects(false);
            conn.setConnectTimeout(timeoutSeconds * 1000);   // ????
            
            
            conn.setDoOutput(doOutput);
            conn.setDoInput(doInput);
            conn.setUseCaches(false);

            // ???
            if(doOutput == true) {
                String requestContentType = ContentFormat.getCotentType(requestFormat);
                if(requestContentType != null) {
                    conn.setRequestProperty("Content-Type", requestContentType);
                } else {
                    // ???
                    log.warning("Invalid requestFormat = " + requestFormat);
                    // TBD: Use the default value???
                }
            }
            if(doInput == true) {
                String responseContentType = ContentFormat.getCotentType(responseFormat);
                if(responseContentType != null) {
                    conn.setRequestProperty("Accept", responseContentType);
                } else {
                    // ???
                    log.warning("Invalid responseFormat = " + responseFormat);
                    // TBD: Use the default value???
                }
            }
            

            // It's a bit "out of order", but
            // We need to do this here
            // because we need to set the auth header for some auth methods
            // ...

            // ????
            int postContentLength = 0;
            if(doOutput == true) {
                if(requestFormat.equals(ContentFormat.JSON)) {
                    try {
                        postData = JsonUtil.buildJson(inputData);
                    } catch (RestApiException e) {
                        log.log(Level.WARNING, "Failed to generate the postData.", e);
                    }
                    if(log.isLoggable(Level.FINE)) log.fine("postData = " + postData);
//                } else if(requestFormat.equals(ContentFormat.XML)) {
//                    // Not implemented yet...
//                    postData = XmlUtil.buildXml(inputData);
//                    if(log.isLoggable(Level.FINE)) log.fine("postData = " + postData);    
                } else if(requestFormat.equals(ContentFormat.QUERY)) {
                    if(inputData instanceof Map<?,?>) {
                        try {
                            @SuppressWarnings("unchecked")
                            Map<String,Object> inputParams = (Map<String,Object>) inputData;
                            postData = ResourceUrlUtil.buildQueryString(inputParams);
                        } catch(ClassCastException e) {
                            // ignore.
                            log.log(Level.WARNING, "Invalid inputData for requestFormat = " + requestFormat + "; inputData = " + inputData, e);
                        }
                    } else {
                        // ???
                        log.warning("Invalid inputData for requestFormat = " + requestFormat + "; inputData = " + inputData);
                    }
                } else {
                    // TBD: Url-encoded post, etc...
                    // ....
                    
                    // ????
                    log.warning("Unsupported requestFormat = " + requestFormat);
                    // What to do ???
                    // Throw excetions ???
                    
                }
                if(postData != null) {
                    // ???
                    postContentLength = postData.getBytes("UTF8").length;
                }
            }


            // null input credential does not mean the call does not require auth, in our design.
            // It simply means, we use the client default value (which can be null or its authRequired == false).
            if(userCredential == null) {
                userCredential = defaultAuthCredential;
                if(log.isLoggable(Level.INFO)) log.info("Input credential is null. Default value is used: " + defaultAuthCredential);
            }
            if(authCredentialRequired == true && userCredential == null) {
                // Error.
                // TBD: return error ???
                log.warning("Auth credential required, but credential is not set.");
            }
            
            if(userCredential == null) {
                // error? or is it allowed??
                // --> probably it's ok to allow this. ???
                // TBD...
            } else {
                
                // Note:
                // Even when we only need client credential (e.g., OAuth 2LO), we still require non-null userCredential...
                // TBD: the authmethod should be moved out of userClient ????
                // ....
                
                boolean authRequired = userCredential.isAuthRequired();
                String authMethod = userCredential.getAuthMethod();
                if(authRequired == true) {      // ???
                    if(authMethod == null || authMethod.equals(AuthMethod.NONE)) {
                        // This cannot happen at this point...
                        log.warning("Auth required, but no authMethod is set??? Something's wrong!!");
                    } else if(authMethod.equals(AuthMethod.BASIC)) {
                        String username = userCredential.getAuthToken();
                        String password = userCredential.getAuthSecret();
                        // TBD:
                        // Validation?
                        

                        
                        // TBD:
                        // We need a base64 encoder...
                        
                        String pair = username + ":" + password;
                        byte[] pairBytes = pair.getBytes();
                        String encodedPair = "";   // = DatatypeConverter.printBase64Binary(pairBytes);
                        // TBD:

                        
                        
                        // ???
                        conn.addRequestProperty("Authorization", "Basic " + encodedPair);
                        
                    } else if(authMethod.equals(AuthMethod.DIGEST)) {
                        // TBD:
                        
                        log.warning("Unsupported auth method: " + authMethod);
                        
                    } else if(authMethod.equals(AuthMethod.TWOLEGGED) || authMethod.equals(AuthMethod.OAUTH)) {

                        String consumerKey = null;
                        String consumerSecret = null;
                        if(clientCredential != null) {
                            consumerKey = clientCredential.getClientKey();
                            consumerSecret = clientCredential.getClientSecret();
                        }
                        
                        // TBD:
                        // Sign the payload...
                        String signature = null;
                        String OAuthHeader = null;

                        if(authMethod.equals(AuthMethod.TWOLEGGED)) {
//                            String clientId = credential.getAuthKey();
//                            String clientSecret = credential.getAuthToken();

                            // TBD:
                            
                            log.warning("Unsupported auth method: " + authMethod);

                        } else {  // if(authMethod.equals(AuthMethod.OAUTH)) {
                            
                            String accessToken = userCredential.getAuthToken();
                            String tokenSecret = userCredential.getAuthSecret();
                            
                            
                            // TBD:
                            
                            log.warning("Unsupported auth method: " + authMethod);
                            
                        }
                        
                        
                        // TBD:
                        // This is probably a good spot to compute OAuth signature
                        // ...
                        // ...
                        
                        
//                        // TBD:
//                        // we need to decide where to put the oauth params (including the signature)
//                        // For now, they will be added to the header...
//                        OAuthSignatureGenerator oauthSignatureGenerator = new OAuthSignatureGenerator();
//                        AccessCredential accessCredential = null;
//                        String httpMethod = method;
//                        BaseURIInfo uriInfo = null;
//                        Map<String,String[]> authHeaders = null;
//                        Map<String,String[]> formParams = null;
//                        Map<String,String[]> queryParams = null;
//                        String oauthSignature = null;
//                        try {
//                            oauthSignature = oauthSignatureGenerator.generate(accessCredential, httpMethod, uriInfo, authHeaders, formParams, queryParams);
//                        } catch (MiniAuthException e) {
//                            // ...
//                            // what to do???
//                        }
//                        
//                        
//                        String nonce = NonceGenerator.generateRandomNonce();
//                        int timestamp = (int) (System.currentTimeMillis() / 1000L);
//                        
//                        // generate header...
//                        // ...
//                        
//                        
//                        
//                        // ...
//                        OAuthParamMap oauthParamMap = null;
//                        try {
//                            OAuthHeader = OAuthSignatureUtil.buildOAuthParamString(oauthParamMap);
//                        } catch (MiniAuthException e) {
//                            // ...
//                            // what to do???
//                        }
//                        
                        

                        
                        
                        // ???
                        if(OAuthHeader != null) {
                            // TBD:
                            
                            conn.addRequestProperty("Authorization", "OAuth " + OAuthHeader);
                        } else {
                            // ????
                        }
                        
                    } else if(authMethod.equals(AuthMethod.BEARER)) {   // OAuth2
                        
                        String accessToken = userCredential.getAuthToken();
                        Long tokenExpirationTime = userCredential.getExpirationTime();  
                        if(tokenExpirationTime == null) {
                            tokenExpirationTime = 0L;     // ???
                        }
                        
                        // TBD:
                        // Work in progress...
                        // This should depend on auth method....
                        // and, it should be done other auth methods as well not just for bearer/oauth2...
                        // ....
                        if(retrying == false) {    // Do this only for the first time...
                            if(authRefreshPolicy != null) {
                                boolean isRefreshBefore = authRefreshPolicy.isRefreshBeforeRequest();
                                if(isRefreshBefore == true) {
                                    long now = System.currentTimeMillis();
                                    int futureMarginSecs = authRefreshPolicy.getFutureMarginSeconds();
                                    long threshhold = now + (futureMarginSecs * 1000L);
                                    if(tokenExpirationTime < threshhold) {
                                        if(authRefreshPolicy.isPerformable()) {
                                            UserCredential refreshedUserCredential = null;
                                            try {
                                                Long refreshedTokenExpirationTime = null;
                                                refreshedUserCredential = (UserCredential) authRefreshPolicy.perform(userCredential);
                                                if(refreshedUserCredential != null) {
                                                    refreshedTokenExpirationTime = refreshedUserCredential.getExpirationTime();
                                                } else {
                                                    log.warning("Failed to refresh the user credential.");                                                
                                                }
                                                if(refreshedTokenExpirationTime != null && refreshedTokenExpirationTime > tokenExpirationTime) {
                                                    // Refreshed.
                                                    String newAccessToken = null;
                                                    if(refreshedUserCredential != null) {
                                                        newAccessToken = refreshedUserCredential.getAuthToken();
                                                    }
                                                    if(newAccessToken != null && !newAccessToken.isEmpty()) {
                                                        if(log.isLoggable(Level.INFO)) log.info("Access token refreshed. New expiration time = " + refreshedTokenExpirationTime);
                                                        // Use the new access token.
                                                        accessToken = newAccessToken;   
                                                    } else {
                                                        // This cannot happen.
                                                        // Just use the old/current accessToken.
                                                        log.warning("User credential refreshed. But, failed to get the new access token.");
                                                    }
                                                } else {
                                                    // Not refreshed.
                                                    log.info("User credential is not refreshed.");
                                                }
                                            } catch(RestApiException e) {
                                                // ...
                                                log.log(Level.WARNING, "Failed to refresh userCredential", e);
                                            }
                                        } else {
                                            log.warning("User Auth refresh required, but authRefreshHandler is not implemented!");
                                        }
                                    } else {
                                        if(log.isLoggable(Level.INFO)) log.info("isRefreshBeforeRequest() == true. But, tokenExpirationTime has not reached: " + tokenExpirationTime);
                                    }
                                } else {
                                    log.info("User Auth refresh is not required before request.");
                                }
                            } else {
                                // Can this happen?
                                log.info("authRefreshPolicy is not set.");
                            }
                        } else {
                            log.info("Skipping auth refresh because retrying == true.");
                        }

                        // ???
                        if(accessToken != null) {
                            conn.addRequestProperty("Authorization", "Bearer " + accessToken);
                        } else {
                            log.warning("Access Token not set!");
                        }

                    } else {
                        // Unsupported auth. (as of now)
                        log.warning("Unsupported auth method: " + authMethod);
                    }
                    
                    
                    // ???
                    // conn.addRequestProperty("Authorization", authMethod + " " + authToken);
                    // ...

                } else {
                    // ignore
                    log.fine("Auth not required");
                }
            }            
            
            
            
            // ????
            if(doOutput == true) {
                if(postData != null && !postData.isEmpty()) {
                    // TBD: Set contentLengh;
                    conn.setRequestProperty("Content-Length", Integer.toString(postContentLength));
                    // etc...
                    DataOutputStream requestOuput = new DataOutputStream(conn.getOutputStream());
                    requestOuput.writeBytes(postData);
                    requestOuput.flush();
                    requestOuput.close();
                } else {
                    // ????
                }
            } else {
                // ???
                conn.connect();
            }
            
            


            int statusCode = conn.getResponseCode();
            if(log.isLoggable(Level.INFO)) log.info("statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);

            // temporary
            String contentType = conn.getContentType();
            int contentLength = conn.getContentLength();
            // String contentLanguage = conn.getHeaderField("Content-Language");
            String location = conn.getHeaderField("Location");
            Map<String,List<String>> headerFields = conn.getHeaderFields();
            if(ClientDebugHelper.getInstance().isTraceEnabled()) {
                log.info("contentType = " + contentType);
                log.info("contentLength = " + contentLength);
                if(headerFields != null) {
                    for(String key : headerFields.keySet()) {
                        List<String> list = headerFields.get(key);
                        String value = StringUtil.join(list, ",");
                        log.info("header key = " + key + "; value = " + value);
                    }
                }
            }

            String responseContent = null;
            if(doInput == true) {
                
                if(StatusCode.isError(statusCode)) {
                    // ???
                    if(log.isLoggable(Level.INFO)) log.info("Because the status code is error, we are skipping the response parsing for endpointUrl, " + endpointUrl);
                } else {
                    BufferedReader responseInput = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String line = null;
                    StringBuilder sb = new StringBuilder();
                    while (null != (line = responseInput.readLine())) {
                        sb.append(line);
                    }
                    responseInput.close();
                    responseContent = sb.toString();
                    // temporary.
                    if(log.isLoggable(Level.FINE)) log.fine("content received = " + responseContent + " for endpointUrl, " + endpointUrl);
                    // temporary
                }
            }
            
            
//            
//            // TBD:
//            // Verify OAuth signature here ????
//            // ....
//            // if the client is used as a "server"... ???
//            //       --> is this possible??? is restclient can be used on the server side???  probably NOT...
//            // and,
//            // if put/post ???
//            // if(doInput == true)
//            // if(StatusCode.isSuccessful(statusCode)) ???
//            // ....
//            
           

            
            // TBD:
            // The return code should be 200 for success, I think,
            // but we check this more broadly (for 2xx), for now.
            if(StatusCode.isSuccessful(statusCode)) { 

                if(doInput) { // this condition is unnecessary...
                    if(responseContent != null) {

                        Object payload = null;
                        
                        // TBD:
                        // parse to the contentType to "format"
                        // TBD:
                        String responseContentFormat = ContentFormat.getContentFormat(contentType);
                        if(responseContentFormat == null) {   // TBD: validation?
                            responseContentFormat = responseFormat;
                        }
                        if(responseContentFormat != null) {   // TBD: validation?
                            if(responseContentFormat.equals(ContentFormat.JSON)) {
                                try {
                                    payload = JsonUtil.parseJson(responseContent);
                                } catch (RestApiException e) {
                                    log.log(Level.WARNING, "Failed to get the payload.", e);
                                }
//                            } else if(responseContentFormat.equals(ContentFormat.XML)) {
//                                payload = XmlUtil.parseXml(responseContent);
                            } else {
                                // ???
                                log.warning("Unsupported response contentType = " + contentType);
                            }
                        } else {
                            // ???
                            log.warning("Failed to parse the response contentType = " + contentType);
                        }

                        response = ResponseUtil.buildResponse(endpointUrl, statusCode, payload, location);
                        
                    } else {
                        // ???
                        if(log.isLoggable(Level.INFO)) log.info("Response expected, but none received for endpointUrl, " + endpointUrl);
                    }
                }

            } else {
                // ???
                log.warning("Processing failed: statusCode = " + statusCode + " for endpointUrl, " + endpointUrl);
                
                // TBD:
                // Check redirection code???
                // ....
                if(StatusCode.isRedirection(statusCode)) {
                    // TBD:
                    // ...
                }
                
                
                if(statusCode == StatusCode.UNAUTHORIZED) {
                    // TBD:
                    if(retrying == false) {    // Do this only for the first time...
                        if(authRefreshPolicy != null) {

                            String accessToken = userCredential.getAuthToken();
                            Long tokenExpirationTime = userCredential.getExpirationTime();  
                            if(tokenExpirationTime == null) {
                                tokenExpirationTime = 0L;     // ???
                            }
                            boolean isRefreshIfFails = authRefreshPolicy.isRefreshIfFails();
                            if(isRefreshIfFails == true) {
                                if(authRefreshPolicy.isPerformable()) {
                                    try {
                                        Long refreshedTokenExpirationTime = null;
                                        UserCredential refreshedUserCredential = (UserCredential) authRefreshPolicy.perform(userCredential);
                                        if(refreshedUserCredential != null) {
                                            refreshedTokenExpirationTime = refreshedUserCredential.getExpirationTime();
                                        }
                                        if(refreshedTokenExpirationTime != null && refreshedTokenExpirationTime > tokenExpirationTime) {
                                            // Refreshed.
                                            String newAccessToken = null;
                                            if(refreshedUserCredential != null) {
                                                newAccessToken = refreshedUserCredential.getAuthToken();
                                            }
                                            if(newAccessToken != null && !newAccessToken.isEmpty()) {
                                                if(log.isLoggable(Level.INFO)) log.info("Access token refreshed. New expiration time = " + refreshedTokenExpirationTime);
                                                // Use the new access token.
        
                                                // Retry???
                                                return process(method, userCredential, inputData, id, params, true);
                                                // TBD: how to call conn.disconnect() ??? etc. ????
                                                // ????
                                                
                                            } else {
                                                // This cannot happen.
                                                // Just use the old/current accessToken. ????
                                                log.warning("User credential refreshed. But, failed to get the new access token.");
                                            }
                                        } else {
                                            // Not refreshed.
                                            log.info("User credential is not refreshed.");
                                        }
                                    } catch (RestApiException e) {
                                        // ...
                                        log.log(Level.WARNING, "Failed to refresh userCredential", e);
                                    }
                                } else {
                                    log.warning("User Auth refresh required if fails, but authRefreshHandler is not implemented!");
                                }
                            } else {
                                log.info("authRefreshPolicy.isRefreshIfFails() == false");
                            }
                        } else {
                            // Can this happen?
                            log.info("authRefreshPolicy is not set.");
                        }
                    } else {
                        log.info("Skipping auth refresh because retrying == true.");
                    }
                    // temporary
                
                }
                
                // TBD:
                // RequestRetryPolicy???
                // ...
                
                // temporary
                String errorCode = String.valueOf(statusCode);
                String errorMessage = "Processing failed: statusCode = " + statusCode + " for endpointUrl, " + endpointUrl;
                response = ResponseUtil.buildResponse(endpointUrl, statusCode, errorCode, errorMessage);
//                // ????
//                if(location != null && !location.isEmpty()) {
//                    response.put(ResponseUtil.KEY_LOCATION, location);
//                }
//                // ????
            }
                        

            // ???
            conn.disconnect();

        } catch (MalformedURLException e) {
            log.log(Level.WARNING, "Bad endpointUrl, " + endpointUrl, e);
            throw e;  // ???
        } catch (IOException e) {
            log.log(Level.WARNING, "Processing failed for endpointUrl, " + endpointUrl + "; method = " + method, e);
            throw e;  // ???
        }
        
        return response;
    }


    @Override
    public String toString()
    {
        return "AbstractRestServiceClient [resourceBaseUrl=" + resourceBaseUrl
                + ", resourceUrlBuilder=" + resourceUrlBuilder
                + ", restServiceClientMaker=" + restServiceClientMaker
                + ", httpMethodFilter=" + httpMethodFilter
                + ", authCredentialRequired=" + authCredentialRequired
                + ", defaultAuthCredential=" + defaultAuthCredential
                + ", clientCredential=" + clientCredential
                + ", dataAccessClient=" + dataAccessClient + ", requestFormat="
                + requestFormat + ", responseFormat=" + responseFormat
                + ", timeoutSeconds=" + timeoutSeconds + ", authRefreshPolicy="
                + authRefreshPolicy + ", requestRetryPolicy="
                + requestRetryPolicy + ", clientCachePolicy="
                + clientCachePolicy + ", autoRedirectPolicy="
                + autoRedirectPolicy + "]";
    }


}
