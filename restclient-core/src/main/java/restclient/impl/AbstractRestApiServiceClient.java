package restclient.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.FlexibleResourceClient;
import restclient.FlexibleServiceClient;
import restclient.RestApiException;
import restclient.RestApiServiceClient;
import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.RequestRetryPolicy;
import restclient.core.CrudMethod;
import restclient.core.HttpMethod;
import restclient.credential.ClientCredential;
import restclient.credential.DataAccessCredential;
import restclient.credential.UserCredential;


public abstract class AbstractRestApiServiceClient implements RestApiServiceClient, FlexibleResourceClient, FlexibleServiceClient
{
    private static final Logger log = Logger.getLogger(AbstractRestApiServiceClient.class.getName());

    
    // TBD:
    // ....
    private RestServiceClient restServiceClient = null;
    private ApiServiceClient apiServiceClient = null;
    // ...
    

    public AbstractRestApiServiceClient()
    {
        // TODO Auto-generated constructor stub

        // TBD:
        init();
    }
    
    protected void init()
    {
        // Place holder
    }

    
    public RestServiceClient getRestServiceClient()
    {
        return restServiceClient;
    }
    protected FlexibleResourceClient getFlexibleRestServiceClient()
    {
        return (FlexibleResourceClient) restServiceClient;
    }
    public ApiServiceClient getApiServiceClient()
    {
        return apiServiceClient;
    }
    protected FlexibleResourceClient getFlexibleApiServiceClient()
    {
        return (FlexibleResourceClient) apiServiceClient;
    }


    @Override
    public boolean isMethodSupported(String methodName)
    {
        if(HttpMethod.isValid(methodName)) {
            return getRestServiceClient().isMethodSupported(methodName);
        } else if(CrudMethod.isValid(methodName)) {
            return getApiServiceClient().isMethodSupported(methodName);
        } else {
            return false;
        }
    }
    

    @Override
    public String getResourceBaseUrl()
    {
        return getApiServiceClient().getResourceBaseUrl();
    }
//    @Override
//    public void setResourceBaseUrl(String resourceBaseUrl)
//    {
//        getApiServiceClient().setResourceBaseUrl(resourceBaseUrl);
//    }


    @Override
    public ClientCredential getClientCredential()
    {
        return getRestServiceClient().getClientCredential();
    }
    public void setClientCredential(ClientCredential clientCredential)
    {
        ((AbstractRestServiceClient) getRestServiceClient()).setClientCredential(clientCredential);
    }

    
    public AuthRefreshPolicy getRestServiceAuthRefreshPolicy()
    {
        return getRestServiceClient().getAuthRefreshPolicy();
    }
    public void setRestServiceAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy)
    {
        getFlexibleRestServiceClient().setAuthRefreshPolicy(authRefreshPolicy);
    }

    public RequestRetryPolicy getRestServiceRequestRetryPolicy()
    {
        return getRestServiceClient().getRequestRetryPolicy();
    }
    public void setRestServiceRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy)
    {
        getFlexibleRestServiceClient().setRequestRetryPolicy(requestRetryPolicy);
    }

    public ClientCachePolicy getRestServiceClientCachePolicy()
    {
        return getRestServiceClient().getClientCachePolicy();
    }
    public void setRestServiceClientCachePolicy(ClientCachePolicy clientCachePolicy)
    {
        getFlexibleRestServiceClient().setClientCachePolicy(clientCachePolicy);
    }

    public CacheControlPolicy getRestServiceCacheControlPolicy()
    {
        return getRestServiceClient().getCacheControlPolicy();
    }
    public void setRestServiceCacheControlPolicy(CacheControlPolicy cacheControlPolicy)
    {
        getFlexibleRestServiceClient().setCacheControlPolicy(cacheControlPolicy);
    }

    public AutoRedirectPolicy getRestServiceAutoRedirectPolicy()
    {
        return getRestServiceClient().getAutoRedirectPolicy();
    }
    public void setRestServiceAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy)
    {
        getFlexibleRestServiceClient().setAutoRedirectPolicy(autoRedirectPolicy);
    }


    @Override
    public Set<String> getRequiredScopes()
    {
        return getApiServiceClient().getRequiredScopes();
    }

    @Override
    public boolean requiresScope(String scope)
    {
        return getApiServiceClient().requiresScope(scope);
    }

    @Override
    public boolean isAccessAllowed(DataAccessCredential dataAccessCredential)
    {
        return getApiServiceClient().isAccessAllowed(dataAccessCredential);
    }


    @Override
    public AuthRefreshPolicy getAuthRefreshPolicy()
    {
        return getApiServiceClient().getAuthRefreshPolicy();
    }
    @Override
    public void setAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy)
    {
        getFlexibleApiServiceClient().setAuthRefreshPolicy(authRefreshPolicy);
    }

    @Override
    public RequestRetryPolicy getRequestRetryPolicy()
    {
        return getApiServiceClient().getRequestRetryPolicy();
    }
    @Override
    public void setRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy)
    {
        getFlexibleApiServiceClient().setRequestRetryPolicy(requestRetryPolicy);
    }

    @Override
    public ClientCachePolicy getClientCachePolicy()
    {
        return getApiServiceClient().getClientCachePolicy();
    }
    @Override
    public void setClientCachePolicy(ClientCachePolicy clientCachePolicy)
    {
        getFlexibleApiServiceClient().setClientCachePolicy(clientCachePolicy);
    }

    @Override
    public CacheControlPolicy getCacheControlPolicy()
    {
        return getApiServiceClient().getCacheControlPolicy();
    }
    @Override
    public void setCacheControlPolicy(CacheControlPolicy cacheControlPolicy)
    {
        getFlexibleApiServiceClient().setCacheControlPolicy(cacheControlPolicy);
    }

    @Override
    public AutoRedirectPolicy getAutoRedirectPolicy()
    {
        return getApiServiceClient().getAutoRedirectPolicy();
    }
    @Override
    public void setAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy)
    {
        getFlexibleApiServiceClient().setAutoRedirectPolicy(autoRedirectPolicy);
    }


    
    @Override
    public Map<String, Object> get(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        return getRestServiceClient().get(credential, id, params);
    }

    @Override
    public Map<String, Object> post(UserCredential credential, Object inputData) throws IOException
    {
        return getRestServiceClient().post(credential, inputData);
    }

    @Override
    public Map<String, Object> put(UserCredential credential, Object inputData,
            String id) throws IOException
    {
        return getRestServiceClient().put(credential, inputData, id);
    }

    @Override
    public Map<String, Object> patch(UserCredential credential,
            Object partialData, String id) throws IOException
    {
        return getRestServiceClient().patch(credential, partialData, id);
    }

    @Override
    public Map<String, Object> delete(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        return getRestServiceClient().delete(credential, id, params);
    }

    
    @Override
    public Object get(UserCredential credential, String id) throws RestApiException, IOException
    {
        return getApiServiceClient().get(credential, id);
    }

    @Override
    public List<Object> list(UserCredential credential, Map<String, Object> params) throws RestApiException, IOException
    {
        return getApiServiceClient().list(credential, params);
    }

    @Override
    public List<String> keys(UserCredential credential,  Map<String, Object> params) throws RestApiException, IOException
    {
        return getApiServiceClient().keys(credential, params);
    }

    @Override
    public Object create(UserCredential credential, Object inputData)
            throws RestApiException, IOException
    {
        return getApiServiceClient().create(credential, inputData);
    }

    @Override
    public Object create(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        return getApiServiceClient().create(credential, inputData, id);
    }

    @Override
    public Object update(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        return getApiServiceClient().update(credential, inputData, id);
    }

    @Override
    public Object modify(UserCredential credential, Object partialData, String id)
            throws RestApiException, IOException
    {
        return getApiServiceClient().modify(credential, partialData, id);
    }

    @Override
    public boolean delete(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        return getApiServiceClient().delete(credential, id);
    }

    @Override
    public int delete(UserCredential credential, Map<String, Object> params)
            throws RestApiException, IOException
    {
        return getApiServiceClient().delete(credential, params);
    }


    @Override
    public String toString()
    {
        return "AbstractRestApiServiceClient [restServiceClient="
                + restServiceClient + ", apiServiceClient=" + apiServiceClient
                + "]";
    }


}
