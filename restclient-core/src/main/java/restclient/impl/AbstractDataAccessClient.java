package restclient.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.common.DataAccessClient;
import restclient.credential.DataAccessCredential;


public abstract class AbstractDataAccessClient implements DataAccessClient
{
    private static final Logger log = Logger.getLogger(AbstractDataAccessClient.class.getName());

    
    private final Set<String> requiredScopes;
    
    public AbstractDataAccessClient()
    {
        requiredScopes = new HashSet<String>();
    }


    @Override
    public Set<String> getRequiredScopes()
    {
        return requiredScopes;
    }
    public boolean addRequiredScope(String scope)
    {
        return requiredScopes.add(scope);
    }
    public boolean addRequiredScopes(Collection<String> scopes)
    {
        return requiredScopes.addAll(scopes);
    }
    public boolean setRequiredScopes(Collection<String> scopes)
    {
        requiredScopes.clear();
        return requiredScopes.addAll(scopes);
    }
    public boolean removeRequiredScope(String scope)
    {
        return requiredScopes.remove(scope);
    }
    public boolean removeRequiredScopes(Collection<String> scopes)
    {
        return requiredScopes.removeAll(scopes);
    }
    public void clearRequiredScopes()
    {
        requiredScopes.clear();
    }
    
    
    @Override
    public boolean requiresScope(String scope)
    {
        if(scope == null || scope.isEmpty()) {
            return true;   // ????
        }

        boolean included = requiredScopes.contains(scope);
        if(log.isLoggable(Level.FINER)) log.finer("Is scope, " + scope + ", included? " + included);
        return included;
    }

    @Override
    public boolean isAccessAllowed(DataAccessCredential dataAccessCredential)
    {
        if(requiredScopes.isEmpty()) {
            return true;
        }
        if(dataAccessCredential == null) {
            return false;
        }
        
        Set<String> credScopes = dataAccessCredential.getDataScopes();
        if(credScopes == null || credScopes.isEmpty()) {
            return false;
        }
        
        boolean allowed = credScopes.containsAll(requiredScopes);
        if(log.isLoggable(Level.FINER)) log.finer("credScopes = " + credScopes + "requiredScopes = " + requiredScopes + "; allowed " + allowed);
        return allowed;
    }


    @Override
    public String toString()
    {
        return "AbstractDataAccessClient [requiredScopes=" + requiredScopes
                + "]";
    }

    
}
