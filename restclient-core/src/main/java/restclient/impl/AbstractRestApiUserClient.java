package restclient.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import restclient.FlexibleResourceClient;
import restclient.FlexibleUserClient;
import restclient.RestApiException;
import restclient.RestApiServiceClient;
import restclient.RestApiUserClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.RequestRetryPolicy;
import restclient.credential.UserCredential;


public abstract class AbstractRestApiUserClient implements RestApiUserClient, FlexibleResourceClient, FlexibleUserClient
{
    private static final Logger log = Logger.getLogger(AbstractRestApiUserClient.class.getName());

    
    
//    // TBD:
//    // ....
//    private RestUserClient restUserClient = null;
//    private ApiUserClient apiUserClient = null;
//    // ...
    


    // Embedded REST Client(s).
    // Which one to use???

    // Option 1.
    private final RestApiServiceClient restApiServiceClient;

    // Options 2.
    // private RestUserClient restUserClient = null;
    // private ApiUserClient apiUserClient = null;

    
    // User-specific data.
    private UserCredential authCredential;



    public AbstractRestApiUserClient(RestApiServiceClient restApiServiceClient)
    {
        this(restApiServiceClient, null);
    }
    public AbstractRestApiUserClient(RestApiServiceClient restApiServiceClient,
            UserCredential authCredential)
    {
        super();
        this.restApiServiceClient = restApiServiceClient;
        this.authCredential = authCredential;

        // TBD:
        init();
    }
    
    protected void init()
    {
        // Place holder
    }


    protected RestApiServiceClient getRestApiServiceClient()
    {
        return restApiServiceClient;
    }
    protected FlexibleResourceClient getFlexibleRestApiServiceClient()
    {
        return (FlexibleResourceClient) restApiServiceClient;
    }


    @Override
    public boolean isMethodSupported(String methodName)
    {
        return getRestApiServiceClient().isMethodSupported(methodName);
    }
    

    @Override
    public UserCredential getUserCredential()
    {
        return authCredential;
    }
    @Override
    public void setUserCredential(UserCredential userCredential)
    {
        this.authCredential = authCredential;
    }


    @Override
    public String getResourceBaseUrl()
    {
        return getRestApiServiceClient().getResourceBaseUrl();
    }
//    @Override
//    public void setResourceBaseUrl(String resourceBaseUrl)
//    {
//        getRestApiServiceClient().setResourceBaseUrl(resourceBaseUrl);
//    }


    @Override
    public AuthRefreshPolicy getAuthRefreshPolicy()
    {
        return getRestApiServiceClient().getAuthRefreshPolicy();
    }
    @Override
    public void setAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy)
    {
        getFlexibleRestApiServiceClient().setAuthRefreshPolicy(authRefreshPolicy);
    }

    @Override
    public RequestRetryPolicy getRequestRetryPolicy()
    {
        return getRestApiServiceClient().getRequestRetryPolicy();
    }
    @Override
    public void setRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy)
    {
        getFlexibleRestApiServiceClient().setRequestRetryPolicy(requestRetryPolicy);
    }

    @Override
    public ClientCachePolicy getClientCachePolicy()
    {
        return getRestApiServiceClient().getClientCachePolicy();
    }
    @Override
    public void setClientCachePolicy(ClientCachePolicy clientCachePolicy)
    {
        getFlexibleRestApiServiceClient().setClientCachePolicy(clientCachePolicy);
    }

    @Override
    public CacheControlPolicy getCacheControlPolicy()
    {
        return getRestApiServiceClient().getCacheControlPolicy();
    }
    @Override
    public void setCacheControlPolicy(CacheControlPolicy cacheControlPolicy)
    {
        getFlexibleRestApiServiceClient().setCacheControlPolicy(cacheControlPolicy);
    }

    @Override
    public AutoRedirectPolicy getAutoRedirectPolicy()
    {
        return getRestApiServiceClient().getAutoRedirectPolicy();
    }
    @Override
    public void setAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy)
    {
        getFlexibleRestApiServiceClient().setAutoRedirectPolicy(autoRedirectPolicy);
    }

    

    @Override
    public Map<String, Object> get(String id, Map<String, Object> params) throws IOException
    {
        return getRestApiServiceClient().get(getUserCredential(), id, params);
    }

    @Override
    public Map<String, Object> post(Object inputData) throws IOException
    {
        return getRestApiServiceClient().post(getUserCredential(), inputData);
    }

    @Override
    public Map<String, Object> put(Object inputData, String id) throws IOException
    {
        return getRestApiServiceClient().put(getUserCredential(), inputData, id);
    }

    @Override
    public Map<String, Object> patch(Object partialData, String id) throws IOException
    {
        return getRestApiServiceClient().patch(getUserCredential(), partialData, id);
    }

    @Override
    public Map<String, Object> delete(String id, Map<String, Object> params) throws IOException
    {
        return getRestApiServiceClient().delete(getUserCredential(), id, params);
    }


    @Override
    public Object get(String id) throws RestApiException, IOException
    {
        return getRestApiServiceClient().get(getUserCredential(), id);
    }

    @Override
    public List<Object> list(Map<String, Object> params) throws RestApiException, IOException
    {
        return getRestApiServiceClient().list(getUserCredential(), params);
    }

    @Override
    public List<String> keys(Map<String, Object> params) throws RestApiException, IOException
    {
        return getRestApiServiceClient().keys(getUserCredential(), params);
    }

    @Override
    public Object create(Object inputData) throws RestApiException, IOException
    {
        return getRestApiServiceClient().create(getUserCredential(), inputData);
    }

    @Override
    public Object create(Object inputData, String id) throws RestApiException, IOException
    {
        return getRestApiServiceClient().create(getUserCredential(), inputData, id);
    }

    @Override
    public Object update(Object inputData, String id) throws RestApiException, IOException
    {
        return getRestApiServiceClient().update(getUserCredential(), inputData, id);
    }

    @Override
    public Object modify(Object partialData, String id) throws RestApiException, IOException
    {
        return getRestApiServiceClient().modify(getUserCredential(), partialData, id);
    }

    @Override
    public boolean delete(String id) throws RestApiException, IOException
    {
        return getRestApiServiceClient().delete(getUserCredential(), id);
    }

    @Override
    public int delete(Map<String, Object> params) throws RestApiException, IOException
    {
        return getRestApiServiceClient().delete(getUserCredential(), params);
    }

    
    @Override
    public String toString()
    {
        return "AbstractRestApiUserClient [restApiServiceClient="
                + restApiServiceClient + ", authCredential=" + authCredential
                + "]";
    }


}
