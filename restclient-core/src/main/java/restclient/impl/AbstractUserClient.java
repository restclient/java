package restclient.impl;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.FlexibleUserClient;
import restclient.RestApiException;
import restclient.common.AuthRefreshPolicy;
import restclient.common.UserAuthRefreshHandler;
import restclient.common.impl.AbstractAuthRefreshPolicy;
import restclient.credential.UserCredential;


public abstract class AbstractUserClient implements FlexibleUserClient, UserCredential
{
    private static final Logger log = Logger.getLogger(AbstractUserClient.class.getName());
    
    // AuthCredential contains userId as well as the user's auth token, etc.
    private UserCredential userCredential;

    // We use factory method to make authRefreshPolicy.
    private final AuthRefreshPolicy authRefreshPolicy;


    public AbstractUserClient()
    {
        this(null);
    }
    public AbstractUserClient(UserCredential userCredential)
    {
        super();
        this.userCredential = userCredential;
        this.authRefreshPolicy = makeAuthRefreshPolicy();
    }

    
    // Factory method.
    protected AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new AbstractAuthRefreshPolicy() {};
    }

    public AuthRefreshPolicy getAuthRefreshPolicy()
    {
        return authRefreshPolicy;
    }


    @Override
    public boolean isAuthRequired()
    {
        return getUserCredential().isAuthRequired();
    }
    @Override
    public String getUser()
    {
        return getUserCredential().getUser();
    }
    @Override
    public String getUserId()
    {
        return getUserCredential().getUserId();
    }
    @Override
    public String getAuthMethod()
    {
        return getUserCredential().getAuthMethod();
    }
    @Override
    public String getAuthToken()
    {
        return getUserCredential().getAuthToken();
    }
    @Override
    public String getAuthSecret()
    {
        return getUserCredential().getAuthSecret();
    }

    @Override
    public Set<String> getDataScopes()
    {
        return getUserCredential().getDataScopes();
    }
    @Override
    public boolean containsScope(String scope)
    {
        return getUserCredential().containsScope(scope);
    }
    
    @Override
    public Long getExpirationTime()
    {
        return getUserCredential().getExpirationTime();
    }
    @Override
    public Long getRefreshedTime()
    {
        return getUserCredential().getRefreshedTime();
    }
    

    @Override
    public UserCredential getUserCredential()
    {
        return getUserCredential(false);   // TBD: Default value for ignoreAuthRefresh ???
    }
    public UserCredential getUserCredential(boolean ignoreAuthRefresh)
    {
        // TBD:
        // Implement authRefreshPolicy here ????
        // ....
        
        
        if(ignoreAuthRefresh == false) {
            if(authRefreshPolicy != null) {
                Long tokenExpirationTime = userCredential.getExpirationTime();  
                if(tokenExpirationTime == null) {
                    tokenExpirationTime = 0L;     // ???
                }
    
                boolean authRefreshHandlerImplemented = false;
                UserAuthRefreshHandler userAuthRefreshHandler = authRefreshPolicy.getAuthRefreshHandler();
                if(userAuthRefreshHandler != null) {
                    authRefreshHandlerImplemented = userAuthRefreshHandler.isImeplemented();
                }
                boolean isRefreshBefore = authRefreshPolicy.isRefreshBeforeRequest();
                if(isRefreshBefore == true) {
                    long now = System.currentTimeMillis();
                
                    int futureMarginSecs = authRefreshPolicy.getFutureMarginSeconds();
                    long futureMarginMillis = futureMarginSecs * 1000L;
                    long threshhold = now + futureMarginMillis;
                    if(tokenExpirationTime < threshhold) {
                        if(authRefreshHandlerImplemented == true) {
                            Long refreshedTokenExpirationTime = null;
                            UserCredential refresedhUserCredential = userAuthRefreshHandler.refreshAuthToken(userCredential);
                            if(refresedhUserCredential != null) {
                                refreshedTokenExpirationTime = refresedhUserCredential.getExpirationTime();
                            }
                            if(refreshedTokenExpirationTime != null && refreshedTokenExpirationTime > tokenExpirationTime) {
                                // Refreshed.
                                String newAccessToken = null;
                                if(refresedhUserCredential != null) {
                                    newAccessToken = refresedhUserCredential.getAuthToken();
                                }
                                if(newAccessToken != null && !newAccessToken.isEmpty()) {
                                    if(log.isLoggable(Level.INFO)) log.info("Access token refreshed. New expiration time = " + refreshedTokenExpirationTime);
                                    // Use the new access token.
                                    
                                    this.userCredential = refresedhUserCredential;
                                } else {
                                    // This cannot happen.
                                    // Just use the old/current accessToken.
                                    log.warning("User credential refreshed. But, failed to get the new access token.");
                                }
                            } else {
                                // Not refreshed.
                                log.info("User credential is not refreshed.");
                            }
                        } else {
                            log.warning("User Auth refresh required, but authRefreshHandler is not implemented!");
                        }
                    } else {
                        if(log.isLoggable(Level.INFO)) log.info("authRefreshPolicy.isRefreshBeforeRequest() == true. But, tokenExpirationTime has not reached: " + tokenExpirationTime);
                    }
                } else {
                    log.info("authRefreshPolicy.isRefreshBeforeRequest() == false.");
                }
            } else {
                // Can this happen?
                log.info("authRefreshPolicy is not set.");
            }
        } else {
            log.info("ignoreAuthRefresh == true.");
        }
        
        return userCredential;
    }
    @Override
    public void setUserCredential(UserCredential userCredential)
    {
        this.userCredential = userCredential;
    }

    
    // TBD:
    // "Callback" ????
    public UserCredential refreshUserCredential()
    {
        try {
            UserCredential refreshedUserCredential = (UserCredential) authRefreshPolicy.perform(userCredential);
            userCredential = refreshedUserCredential;
        } catch (RestApiException e) {
            // ????
            log.log(Level.WARNING, "Failed to refresh ", e);
            return null;   // ???
        }
        return userCredential;
    }

    
    @Override
    public String toString()
    {
        return "AbstractUserClient [userCredential=" + userCredential
                + ", authRefreshPolicy=" + authRefreshPolicy + "]";
    }


}
