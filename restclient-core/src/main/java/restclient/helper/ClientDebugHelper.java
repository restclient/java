package restclient.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


/**
 * It's primarily used for keeping debug-related "global variables". 
 */
public class ClientDebugHelper
{
    private static final Logger log = Logger.getLogger(ClientDebugHelper.class.getName());

    // Initialization-on-demand holder.
    private static final class DebugHelperHolder
    {
        private static final ClientDebugHelper INSTANCE = new ClientDebugHelper();
    }

    // Singleton method
    public static ClientDebugHelper getInstance()
    {
        return DebugHelperHolder.INSTANCE;
    }


    // Log level: INFO
    private boolean traceEnabled;
    // Log level: FINE
    private boolean debugEnabled;
    
    private ClientDebugHelper()
    {
        traceEnabled = false;
        debugEnabled = false;
    }

    
    public boolean isTraceEnabled()
    {
        return traceEnabled;
    }
    public void setTraceEnabled(boolean traceEnabled)
    {
        this.traceEnabled = traceEnabled;
        if(this.traceEnabled == false) {
            debugEnabled = false;
        }
    }

    public boolean isDebugEnabled()
    {
        return debugEnabled;
    }
    public void setDebugEnabled(boolean debugEnabled)
    {
        this.debugEnabled = debugEnabled;
        if(this.debugEnabled == true) {
            traceEnabled = true;
        }
    }
    

}
