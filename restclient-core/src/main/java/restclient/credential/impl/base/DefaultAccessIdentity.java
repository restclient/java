package restclient.credential.impl.base;

import java.util.logging.Logger;

import restclient.credential.impl.AbstractAccessIdentity;


public final class DefaultAccessIdentity extends AbstractAccessIdentity
{
    private static final Logger log = Logger.getLogger(DefaultAccessIdentity.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultAccessIdentity(String consumerKey, String accessToken)
    {
        super(consumerKey, accessToken);
    }

}
