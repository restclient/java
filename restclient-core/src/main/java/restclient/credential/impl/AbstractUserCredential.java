package restclient.credential.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.credential.UserCredential;


public abstract class AbstractUserCredential implements UserCredential, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractUserCredential.class.getName());
    private static final long serialVersionUID = 1L;

    // Is this necessary???
    private boolean authRequired;      
   
    // (1) User IDs.
    private String user;              // User.guid.  user id in the client system.
    private String userId;            // User id or username. User id in the server system. (could be long or string, etc. Always converted to string.)
    
    // (2) Auth method. Optional.
    // If a valid value is set, it overwrites the client's default value.
    private String authMethod;        // ????

    // (3) Auth token/secret
    // Different auth methods require different set of auth credential
    // authToken and authSecret are interpreted according to the auth method.
    // Basic: authToken=username, authSecret=password
    // Digest: ???
    // OAuth 2-legged: authToken,authSecret: not used. Use ClientCredential.clientKey, clientSecret.
    // OAuth 1.0a: authToken=accessToken, authSecrent=tokenSecret
    // OAuth2 Bearer Token: authToken=accessToken, authSecret: not used.
    // String getAuthKey();
    private String authToken;         // access token.
    private String authSecret;        // token secret

    // (4) Data scope list associated with authToken.
    private Set<String> dataScopes;
    
    // (5) Auth token expiration time.
    // Null means "non-expiring" or "unknown".
    private Long expirationTime;      // Unix epoch time in milli seconds.

    // This is needed to support auth refresh.
    // Returns the last refreshed time, if known.
    private Long refreshedTime;


    public AbstractUserCredential()
    {
        this(false);
    }
    public AbstractUserCredential(boolean authRequired)
    {
        this(authRequired, null, null);
    }
    public AbstractUserCredential(boolean authRequired, String user, String userId)
    {
        this(authRequired, user, userId, null);
    }
    public AbstractUserCredential(boolean authRequired, String user,
            String userId, String authMethod)
    {
        this(authRequired, user, userId, authMethod, null, null);
    }
    public AbstractUserCredential(boolean authRequired, String user,
            String userId, String authMethod, String authToken, String authSecret)
    {
        this(authRequired, user, userId, authMethod, authToken, authSecret, null);
    }
    public AbstractUserCredential(boolean authRequired, String user,
            String userId, String authMethod, String authToken, String authSecret,
            Set<String> dataScopes)
    {
        this(authRequired, user, userId, authMethod, authToken, authSecret, dataScopes, null, null);
    }
    public AbstractUserCredential(boolean authRequired, String user,
            String userId, String authMethod, String authToken, String authSecret, Set<String> dataScopes,
            Long expirationTime, Long refreshedTime)
    {
        super();
        this.authRequired = authRequired;
        this.user = user;
        this.userId = userId;
        this.authMethod = authMethod;
        this.authToken = authToken;
        this.authSecret = authSecret;
        this.dataScopes = dataScopes;
        this.expirationTime = expirationTime;
        this.refreshedTime = refreshedTime;

        // TBD:
        init();
    }
    
    protected void init()
    {
        // Place holder
    }


    @Override
    public boolean isAuthRequired()
    {
        return authRequired;
    }
    public void setAuthRequired(boolean authRequired)
    {
        this.authRequired = authRequired;
//        if(this.authRequired == false) {
//            authMethod = AuthMethod.NONE;   // ???
//        } else {
//            // Defaulte auth method???
//            // ...
//        }
    }

    @Override
    public String getUser()
    {
        return user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @Override
    public String getUserId()
    {
        return userId;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    @Override
    public String getAuthMethod()
    {
        return authMethod;
    }
    public void setAuthMethod(String authMethod)
    {
        this.authMethod = authMethod;
//        if(this.authMethod == null || this.authMethod.equals(AuthMethod.NONE)) {
//            authRequired = false;    // ???
//        } else {
//            // ???
//            authRequired = true;
//        }
    }

    @Override
    public String getAuthToken()
    {
        return authToken;
    }
    public void setAuthToken(String authToken)
    {
        this.authToken = authToken;
    }

    @Override
    public String getAuthSecret()
    {
        return authSecret;
    }
    public void setAuthSecret(String authSecret)
    {
        this.authSecret = authSecret;
    }

    @Override
    public Set<String> getDataScopes()
    {
        return dataScopes;
    }
    public void setDataScopes(Set<String> dataScopes)
    {
        this.dataScopes = dataScopes;
    }
    public boolean addDataScope(String scope)
    {
        return dataScopes.add(scope);
    }
    public boolean addDataScopes(Collection<String> scopes)
    {
        return dataScopes.addAll(scopes);
    }
    public boolean setDataScopes(Collection<String> scopes)
    {
        dataScopes.clear();
        return dataScopes.addAll(scopes);
    }
    public boolean removeDataScope(String scope)
    {
        return dataScopes.remove(scope);
    }
    public boolean removeDataScopes(Collection<String> scopes)
    {
        return dataScopes.removeAll(scopes);
    }
    public void clearDataScopes()
    {
        dataScopes.clear();
    }

    @Override
    public boolean containsScope(String scope)
    {
        if(dataScopes == null || dataScopes.isEmpty()) {
            return false;
        }
        if(scope == null || scope.isEmpty()) {
            return true;   // ????
        }

        boolean included = dataScopes.contains(scope);
        if(log.isLoggable(Level.FINER)) log.finer("Is scope, " + scope + ", included? " + included);
        return included;
    }
    public boolean containsAllScopes(Set<String> scopes)
    {
        if(dataScopes == null || dataScopes.isEmpty()) {
            return false;
        }
        if(scopes == null || scopes.isEmpty()) {
            return true;   // ????
        }

        boolean included = dataScopes.containsAll(scopes);
        if(log.isLoggable(Level.FINER)) log.finer("Are all scopes, " + scopes + ", included? " + included);
        return included;
    }
    public boolean containsAnyScope(Set<String> scopes)
    {
        if(dataScopes == null || dataScopes.isEmpty()) {
            return false;
        }
        if(scopes == null || scopes.isEmpty()) {
            return true;   // ????
        }

        for(String s : scopes) {
            if(dataScopes.contains(s)) {
                if(log.isLoggable(Level.FINER)) log.finer("scope, " + s + ", included.");
                return true;
            }
        }
        if(log.isLoggable(Level.FINER)) log.finer("None of the scopes, " + scopes + ", included.");
        return false;
    }

    
    
    
    @Override
    public Long getExpirationTime()
    {
        return expirationTime;
    }
    public void setExpirationTime(Long expirationTime)
    {
        this.expirationTime = expirationTime;
    }

    @Override
    public Long getRefreshedTime()
    {
        return refreshedTime;
    }
    public void setRefreshedTime(Long refreshedTime)
    {
        this.refreshedTime = refreshedTime;
    }


    // For debugging.
    // Note that authToken/authSecret have been obfuscated.
    @Override
    public String toString()
    {
        return "AbstractUserCredential [authRequired=" + authRequired
                + ", user=" + user + ", userId=" + userId + ", authMethod="
                + authMethod + ", authToken=" + "..." + ", authSecret="
                + "..." + ", dataScopes=" + dataScopes
                + ", expirationTime=" + expirationTime + ", refreshedTime="
                + refreshedTime + "]";
    }


}
