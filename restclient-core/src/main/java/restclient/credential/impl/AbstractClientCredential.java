package restclient.credential.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.credential.ClientCredential;


public abstract class AbstractClientCredential implements ClientCredential, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractClientCredential.class.getName());
    private static final long serialVersionUID = 1L;

    // Read only?
    private final String clientKey;
    private final String clientSecret;


    // ???
    public AbstractClientCredential()
    {
        this(null, null);
    }
    public AbstractClientCredential(String clientKey, String clientSecret)
    {
        super();
        this.clientKey = clientKey;
        this.clientSecret = clientSecret;
    }


    @Override
    public String getClientKey()
    {
        return clientKey;
    }
//    public void setClientKey(String clientKey)
//    {
//        this.clientKey = clientKey;
//    }

    @Override
    public String getClientSecret()
    {
        return clientSecret;
    }
//    public void setClientSecret(String clientSecret)
//    {
//        this.clientSecret = clientSecret;
//    }

    
    // clientSecret obfuscated...
    @Override
    public String toString()
    {
        return "AbstractClientCredential [clientKey=" + clientKey
                + ", clientSecret=" + "..." + "]";
    }

    
}
