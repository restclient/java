package restclient.credential;



public interface ClientCredential extends AuthCredential
{
    String getClientKey();
    String getClientSecret();
}
