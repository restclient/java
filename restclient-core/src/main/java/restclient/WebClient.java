package restclient;

import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.RequestRetryPolicy;


/**
 * Base interface for "http" based client.
 */
public interface WebClient
{
    // Using the "strategy" pattern...
    AuthRefreshPolicy getAuthRefreshPolicy();
    RequestRetryPolicy getRequestRetryPolicy();
    ClientCachePolicy getClientCachePolicy();
    CacheControlPolicy getCacheControlPolicy();
    AutoRedirectPolicy getAutoRedirectPolicy();
    // etc...
    
}
