package restclient.ext.twitter.factory;

import restclient.ApiServiceClient;
import restclient.credential.UserCredential;
import restclient.ext.twitter.TwitterApiUserClient;
import restclient.factory.ApiUserClientFactory;
import restclient.factory.ClientFactory;


public interface TwitterApiUserClientFactory extends ApiUserClientFactory, ClientFactory
{
    TwitterApiUserClient createTwitterApiUserClient(String resourceBaseUrl, UserCredential userCredential);
    TwitterApiUserClient createTwitterApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential);

}
