package restclient.ext.twitter.factory.impl;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ext.twitter.TwitterApiServiceClient;
import restclient.ext.twitter.factory.TwitterApiServiceClientFactory;
import restclient.ext.twitter.impl.BaseTwitterApiServiceClient;
import restclient.ext.twitter.maker.TwitterApiServiceClientMaker;
import restclient.factory.impl.AbstractApiServiceClientFactory;
import restclient.maker.ApiServiceClientMaker;


public class BaseTwitterApiServiceClientFactory extends AbstractApiServiceClientFactory implements TwitterApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(BaseTwitterApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class TwitterApiServiceClientFactoryHolder
    {
        private static final BaseTwitterApiServiceClientFactory INSTANCE = new BaseTwitterApiServiceClientFactory();
    }

    // Singleton method
    public static BaseTwitterApiServiceClientFactory getInstance()
    {
        return TwitterApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new BaseTwitterApiServiceClient(resourceBaseUrl);
    }
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return TwitterApiServiceClientMaker.getInstance();
    }


    @Override
    public TwitterApiServiceClient createTwitterApiServiceClient(String resourceBaseUrl)
    {
        return new BaseTwitterApiServiceClient(resourceBaseUrl);
    }

    
}
