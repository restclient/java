package restclient.ext.twitter.factory.impl;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ApiUserClient;
import restclient.credential.UserCredential;
import restclient.ext.twitter.TwitterApiUserClient;
import restclient.ext.twitter.factory.TwitterApiUserClientFactory;
import restclient.ext.twitter.impl.BaseTwitterApiUserClient;
import restclient.ext.twitter.maker.TwitterApiUserClientMaker;
import restclient.factory.impl.AbstractApiUserClientFactory;
import restclient.maker.ApiUserClientMaker;


public class BaseTwitterApiUserClientFactory extends AbstractApiUserClientFactory implements TwitterApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(BaseTwitterApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class TwitterApiUserClientFactoryHolder
    {
        private static final BaseTwitterApiUserClientFactory INSTANCE = new BaseTwitterApiUserClientFactory();
    }

    // Singleton method
    public static BaseTwitterApiUserClientFactory getInstance()
    {
        return TwitterApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new BaseTwitterApiUserClient(resourceBaseUrl);
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return TwitterApiUserClientMaker.getInstance();
    }


    @Override
    public TwitterApiUserClient createTwitterApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        return new BaseTwitterApiUserClient(resourceBaseUrl, userCredential);
    }

    @Override
    public TwitterApiUserClient createTwitterApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        return new BaseTwitterApiUserClient(apiServiceClient, userCredential);
    }

 
}
