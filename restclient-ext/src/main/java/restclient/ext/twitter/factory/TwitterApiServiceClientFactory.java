package restclient.ext.twitter.factory;

import restclient.ext.twitter.TwitterApiServiceClient;
import restclient.factory.ApiServiceClientFactory;
import restclient.factory.ClientFactory;


public interface TwitterApiServiceClientFactory extends ApiServiceClientFactory, ClientFactory
{
    TwitterApiServiceClient createTwitterApiServiceClient(String resourceBaseUrl);

}
