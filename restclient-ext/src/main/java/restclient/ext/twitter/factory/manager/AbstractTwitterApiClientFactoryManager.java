package restclient.ext.twitter.factory.manager;

import java.util.logging.Logger;

import restclient.ext.twitter.factory.TwitterApiServiceClientFactory;
import restclient.ext.twitter.factory.TwitterApiUserClientFactory;
import restclient.ext.twitter.factory.impl.BaseTwitterApiServiceClientFactory;
import restclient.ext.twitter.factory.impl.BaseTwitterApiUserClientFactory;


/**
 * This manager class uses the "Abstract Factory" pattern.
 * Client can inherit from this class to return a proper concrete factory(s).
 */
public abstract class AbstractTwitterApiClientFactoryManager
{
    private static final Logger log = Logger.getLogger(AbstractTwitterApiClientFactoryManager.class.getName());

    // FactoryManager can be a singleton.
    protected AbstractTwitterApiClientFactoryManager() {}


//    // Initialization-on-demand holder.
//    private static final class AbstractTwitterApiClientFactoryManagerHolder
//    {
//        private static final AbstractTwitterApiClientFactoryManager INSTANCE = new AbstractTwitterApiClientFactoryManager() {};
//    }
//
//    // Singleton method
//    public static AbstractTwitterApiClientFactoryManager getInstance()
//    {
//        return AbstractTwitterApiClientFactoryManagerHolder.INSTANCE;
//    }


    // Note:
    // Client should override these methods to return appropriate concrete factories.
    
    // Returns a service client factory.
    public TwitterApiServiceClientFactory getTwitterApiServiceClientFactory()
    {
        return BaseTwitterApiServiceClientFactory.getInstance();
    }

    // Returns a user client factory.
    public TwitterApiUserClientFactory getTwitterApiUserClientFactory()
    {
        return BaseTwitterApiUserClientFactory.getInstance();
    }


}
