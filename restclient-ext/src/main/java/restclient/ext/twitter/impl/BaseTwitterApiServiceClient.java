package restclient.ext.twitter.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.core.ListResponseType;
import restclient.ext.twitter.TwitterApiServiceClient;
import restclient.ext.twitter.maker.TwitterApiServiceClientMaker;
import restclient.impl.AbstractApiServiceClient;
import restclient.maker.ApiServiceClientMaker;



// temporary
// Base class for all Twitter API resources.
public class BaseTwitterApiServiceClient extends AbstractApiServiceClient implements TwitterApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseTwitterApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public BaseTwitterApiServiceClient(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }


    @Override
    protected void init()
    {
        super.init();
        
        // TBD: Need to check this...
        setListResponseType(ListResponseType.LIST);
        // ...

    }


    // Factory methods

    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return TwitterApiServiceClientMaker.getInstance();
    }

    
}
