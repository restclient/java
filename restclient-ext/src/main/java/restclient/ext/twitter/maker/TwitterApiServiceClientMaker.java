package restclient.ext.twitter.maker;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.CrudMethodFilter;
import restclient.common.DataAccessClient;
import restclient.common.RequestRetryPolicy;
import restclient.common.impl.AbstractAuthRefreshPolicy;
import restclient.common.impl.AbstractAutoRedirectPolicy;
import restclient.common.impl.AbstractCacheControlPolicy;
import restclient.common.impl.AbstractClientCachePolicy;
import restclient.common.impl.AbstractCrudMethodFilter;
import restclient.common.impl.AbstractRequestRetryPolicy;
import restclient.impl.AbstractDataAccessClient;
import restclient.impl.AbstractRestServiceClient;
import restclient.maker.ApiServiceClientMaker;


// Abstract factory.
public class TwitterApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(TwitterApiServiceClientMaker.class.getName());


    protected TwitterApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class TwitterApiServiceClientMakerHolder
    {
        private static final TwitterApiServiceClientMaker INSTANCE = new TwitterApiServiceClientMaker();
    }

    // Singleton method
    public static TwitterApiServiceClientMaker getInstance()
    {
        return TwitterApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new AbstractRestServiceClient(resourceBaseUrl) {};
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new AbstractCrudMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new AbstractAuthRefreshPolicy() {};
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new AbstractRequestRetryPolicy() {};
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new AbstractClientCachePolicy() {};
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new AbstractCacheControlPolicy() {};
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new AbstractAutoRedirectPolicy() {};
    }


}
