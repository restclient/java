package restclient.ext.google;

import restclient.RestServiceClient;
import restclient.common.ResourceUrlBuilder;


public interface GoogleRestServiceClient extends RestServiceClient, ResourceUrlBuilder
{

}
