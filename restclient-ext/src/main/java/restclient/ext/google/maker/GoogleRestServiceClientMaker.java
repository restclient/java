package restclient.ext.google.maker;

import java.util.logging.Logger;

import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.DataAccessClient;
import restclient.common.HttpMethodFilter;
import restclient.common.RequestRetryPolicy;
import restclient.common.ResourceUrlBuilder;
import restclient.common.impl.AbstractHttpMethodFilter;
import restclient.ext.google.common.GoogleAuthRefreshPolicy;
import restclient.ext.google.common.GoogleAutoRedirectPolicy;
import restclient.ext.google.common.GoogleCacheControlPolicy;
import restclient.ext.google.common.GoogleClientCachePolicy;
import restclient.ext.google.common.GoogleRequestRetryPolicy;
import restclient.ext.google.common.GoogleResourceUrlBuilder;
import restclient.impl.AbstractDataAccessClient;
import restclient.maker.RestServiceClientMaker;


// Google factory.
public class GoogleRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(GoogleRestServiceClientMaker.class.getName());


    protected GoogleRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class GoogleRestServiceClientMakerHolder
    {
        private static final GoogleRestServiceClientMaker INSTANCE = new GoogleRestServiceClientMaker();
    }

    // Singleton method
    public static GoogleRestServiceClientMaker getInstance()
    {
        return GoogleRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new GoogleResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new AbstractHttpMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new GoogleAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new GoogleRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new GoogleClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new GoogleCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new GoogleAutoRedirectPolicy();
    }


    
    @Override
    public String toString()
    {
        return "GoogleRestServiceClientMaker []";
    }


}
