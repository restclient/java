package restclient.ext.google.maker;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ext.google.impl.BaseGoogleApiServiceClient;
import restclient.maker.ApiUserClientMaker;


// Abstract factory.
public class GoogleApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(GoogleApiUserClientMaker.class.getName());


    protected GoogleApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static final class GoogleApiUserClientMakerHolder
    {
        private static final GoogleApiUserClientMaker INSTANCE = new GoogleApiUserClientMaker();
    }

    // Singleton method
    public static GoogleApiUserClientMaker getInstance()
    {
        return GoogleApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleApiServiceClient(resourceBaseUrl);
    }


    @Override
    public String toString()
    {
        return "GoogleApiUserClientMaker []";
    }

    
}
