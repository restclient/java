package restclient.ext.google.maker;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.ext.google.impl.BaseGoogleRestServiceClient;
import restclient.maker.RestUserClientMaker;


// Google factory.
public class GoogleRestUserClientMaker implements RestUserClientMaker
{
    private static final Logger log = Logger.getLogger(GoogleRestUserClientMaker.class.getName());

    
    protected GoogleRestUserClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class GoogleRestUserClientMakerHolder
    {
        private static final GoogleRestUserClientMaker INSTANCE = new GoogleRestUserClientMaker();
    }

    // Singleton method
    public static GoogleRestUserClientMaker getInstance()
    {
        return GoogleRestUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestServiceClient(resourceBaseUrl);
    }


    @Override
    public String toString()
    {
        return "GoogleRestUserClientMaker []";
    }

}
