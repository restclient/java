package restclient.ext.google.maker;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.CrudMethodFilter;
import restclient.common.DataAccessClient;
import restclient.common.RequestRetryPolicy;
import restclient.common.impl.AbstractCrudMethodFilter;
import restclient.ext.google.common.GoogleAuthRefreshPolicy;
import restclient.ext.google.common.GoogleAutoRedirectPolicy;
import restclient.ext.google.common.GoogleCacheControlPolicy;
import restclient.ext.google.common.GoogleClientCachePolicy;
import restclient.ext.google.common.GoogleRequestRetryPolicy;
import restclient.ext.google.impl.BaseGoogleRestServiceClient;
import restclient.impl.AbstractDataAccessClient;
import restclient.maker.ApiServiceClientMaker;


// Abstract factory.
public class GoogleApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(GoogleApiServiceClientMaker.class.getName());


    protected GoogleApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class GoogleApiServiceClientMakerHolder
    {
        private static final GoogleApiServiceClientMaker INSTANCE = new GoogleApiServiceClientMaker();
    }

    // Singleton method
    public static GoogleApiServiceClientMaker getInstance()
    {
        return GoogleApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestServiceClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new AbstractCrudMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }
    
    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new GoogleAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new GoogleRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new GoogleClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new GoogleCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new GoogleAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "GoogleApiServiceClientMaker []";
    }

}
