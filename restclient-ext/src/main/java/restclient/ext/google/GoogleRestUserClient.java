package restclient.ext.google;

import restclient.RestUserClient;
import restclient.common.ResourceUrlBuilder;


public interface GoogleRestUserClient extends RestUserClient, ResourceUrlBuilder
{

}
