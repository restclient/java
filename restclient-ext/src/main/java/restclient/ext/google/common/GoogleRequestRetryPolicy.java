package restclient.ext.google.common;

import java.util.logging.Logger;

import restclient.common.impl.AbstractRequestRetryPolicy;


public class GoogleRequestRetryPolicy extends AbstractRequestRetryPolicy
{
    private static final Logger log = Logger.getLogger(GoogleRequestRetryPolicy.class.getName());

    public GoogleRequestRetryPolicy()
    {
        // TODO Auto-generated constructor stub
    }


    
    
    @Override
    public String toString()
    {
        return "GoogleRequestRetryPolicy [isRetryIfFails()=" + isRetryIfFails()
                + ", getMaxRetryCount()=" + getMaxRetryCount() + "]";
    }
    


}
