package restclient.ext.google.common;

import java.util.logging.Logger;

import restclient.common.impl.AbstractAuthRefreshPolicy;
import restclient.common.impl.AbstractAutoRedirectPolicy;


public class GoogleAutoRedirectPolicy extends AbstractAutoRedirectPolicy
{
    private static final Logger log = Logger.getLogger(AbstractAuthRefreshPolicy.class.getName());


    
    @Override
    public String toString()
    {
        return "GoogleAutoRedirectPolicy [isAutoFollowPrimaryChoice()="
                + isAutoFollowPrimaryChoice() + ", isAutoRedirect()="
                + isAutoRedirect() + "]";
    }

    
}
