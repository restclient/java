package restclient.ext.google.common;

import java.util.logging.Logger;

import restclient.common.impl.AbstractAuthRefreshPolicy;


public class GoogleAuthRefreshPolicy extends AbstractAuthRefreshPolicy
{
    private static final Logger log = Logger.getLogger(GoogleAuthRefreshPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    
    public GoogleAuthRefreshPolicy()
    {
    }



    @Override
    public String toString()
    {
        return "GoogleAuthRefreshPolicy [getAuthRefreshHandler()="
                + getAuthRefreshHandler() + "]";
    }


 

}
