package restclient.ext.google.common;

import java.util.logging.Logger;

import restclient.common.impl.AbstractCacheControlPolicy;


public class GoogleCacheControlPolicy extends AbstractCacheControlPolicy
{
    private static final Logger log = Logger.getLogger(GoogleCacheControlPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    
    public GoogleCacheControlPolicy()
    {
    }


    @Override
    public String toString()
    {
        return "GoogleCacheControlPolicy []";
    }


}
