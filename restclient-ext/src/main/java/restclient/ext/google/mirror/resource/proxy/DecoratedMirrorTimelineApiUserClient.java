package restclient.ext.google.mirror.resource.proxy;

import restclient.ext.google.mirror.resource.MirrorTimelineApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorTimelineApiUserClient extends MirrorTimelineApiUserClient
{
    // No need for API to return the decorated client.
    // MirrorTimelineApiUserClient getDecoratedClient();
}
