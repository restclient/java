package restclient.ext.google.mirror.resource.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.ext.google.mirror.MirrorApiServiceClient;
import restclient.ext.google.mirror.impl.BaseMirrorApiServiceClient;


// temporary
public class BaseMirrorTimelineApiServiceClient extends BaseMirrorApiServiceClient implements MirrorApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorTimelineApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public static final String TIMELINE_RESOURCE_BASE_URL = "https://www.googleapis.com/mirror/v1/timeline";

    public BaseMirrorTimelineApiServiceClient()
    {
        super(TIMELINE_RESOURCE_BASE_URL);
    }


    
    @Override
    public String toString()
    {
        return "BaseMirrorTimelineApiServiceClient [getRestServiceClient()="
                + getRestServiceClient() + ", getCrudMethodFilter()="
                + getCrudMethodFilter() + ", getListResponseType()="
                + getListResponseType() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }
    

}
