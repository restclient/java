package restclient.ext.google.mirror.resource.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.common.impl.AbstractResourceUrlBuilder;
import restclient.credential.UserCredential;
import restclient.ext.google.mirror.MirrorApiUserClient;
import restclient.ext.google.mirror.impl.BaseMirrorApiUserClient;


public class BaseMirrorTimelineAttachmentApiUserClient extends BaseMirrorApiUserClient implements MirrorApiUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorTimelineAttachmentApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public BaseMirrorTimelineAttachmentApiUserClient(String timelineItemId)
    {
        this(timelineItemId, null);
    }
    public BaseMirrorTimelineAttachmentApiUserClient(String timelineItemId, UserCredential userCredential)
    {
        super(AbstractResourceUrlBuilder.buildBaseUrl(BaseMirrorTimelineAttachmentApiServiceClient.TIMELINE_PARENTRESOURCE_BASE_URL, timelineItemId, BaseMirrorTimelineAttachmentApiServiceClient.ATTACHMENT_RESOURCENAME), userCredential);
    }

    public BaseMirrorTimelineAttachmentApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public BaseMirrorTimelineAttachmentApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    
    @Override
    public String toString()
    {
        return "BaseMirrorTimelineAttachmentApiUserClient [getApiServiceClient()="
                + getApiServiceClient()
                + ", getUserCredential()="
                + getUserCredential()
                + ", isAccessAllowed()="
                + isAccessAllowed()
                + ", getResourceBaseUrl()="
                + getResourceBaseUrl()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy()
                + ", getClientCachePolicy()="
                + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
