package restclient.ext.google.mirror.factory;

import restclient.credential.UserCredential;
import restclient.ext.google.factory.GoogleApiUserClientFactory;
import restclient.ext.google.mirror.MirrorApiServiceClient;
import restclient.ext.google.mirror.MirrorApiUserClient;


public interface MirrorApiUserClientFactory extends GoogleApiUserClientFactory
{
    MirrorApiUserClient createMirrorContactApiUserClient(UserCredential userCredential);
    MirrorApiUserClient createMirrorLocationApiUserClient(UserCredential userCredential);
    MirrorApiUserClient createMirrorSubscriptionApiUserClient(UserCredential userCredential);
    MirrorApiUserClient createMirrorTimelineApiUserClient(UserCredential userCredential);
    MirrorApiUserClient createMirrorTimelineAttachmentApiUserClient(String timelineItemId, UserCredential userCredential);

    MirrorApiUserClient createMirrorContactApiUserClient(MirrorApiServiceClient serviceClient, UserCredential userCredential);
    MirrorApiUserClient createMirrorLocationApiUserClient(MirrorApiServiceClient serviceClient, UserCredential userCredential);
    MirrorApiUserClient createMirrorSubscriptionApiUserClient(MirrorApiServiceClient serviceClient, UserCredential userCredential);
    MirrorApiUserClient createMirrorTimelineApiUserClient(MirrorApiServiceClient serviceClient, UserCredential userCredential);
    MirrorApiUserClient createMirrorTimelineAttachmentApiUserClient(MirrorApiServiceClient serviceClient, UserCredential userCredential);
}
