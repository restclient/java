package restclient.ext.google.mirror.resource.proxy;

import restclient.ext.google.mirror.resource.MirrorSubscriptionApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorSubscriptionApiServiceClient extends MirrorSubscriptionApiServiceClient
{
    // No need for API to return the decorated client.
    // MirrorSubscriptionApiServiceClient getDecoratedClient();
}
