package restclient.ext.google.mirror.resource.proxy;

import restclient.ext.google.mirror.resource.MirrorContactApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorContactApiUserClient extends MirrorContactApiUserClient
{
    // No need for API to return the decorated client.
    // MirrorContactApiUserClient getDecoratedClient();
}
