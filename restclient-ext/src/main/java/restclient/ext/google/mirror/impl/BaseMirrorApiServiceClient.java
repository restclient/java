package restclient.ext.google.mirror.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.core.ListResponseType;
import restclient.ext.google.impl.BaseGoogleApiServiceClient;
import restclient.ext.google.mirror.MirrorApiServiceClient;
import restclient.ext.google.mirror.maker.MirrorApiServiceClientMaker;
import restclient.maker.ApiServiceClientMaker;


// temporary
// Base class for all Mirror API resources.
public class BaseMirrorApiServiceClient extends BaseGoogleApiServiceClient implements MirrorApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public BaseMirrorApiServiceClient(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }

    @Override
    protected void init()
    {
        super.init();
        
        // TBD: Need to check this...
        setListResponseType(ListResponseType.MAP_ITEMS);
        // ...

    }


    // Factory methods

    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return MirrorApiServiceClientMaker.getInstance();
    }


    
    @Override
    public String toString()
    {
        return "BaseMirrorApiServiceClient [getRestServiceClient()="
                + getRestServiceClient() + ", getCrudMethodFilter()="
                + getCrudMethodFilter() + ", getListResponseType()="
                + getListResponseType() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }


}
