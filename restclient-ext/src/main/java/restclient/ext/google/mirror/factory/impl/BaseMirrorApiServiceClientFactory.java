package restclient.ext.google.mirror.factory.impl;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ext.google.factory.GoogleApiUserClientFactory;
import restclient.ext.google.factory.impl.BaseGoogleApiServiceClientFactory;
import restclient.ext.google.mirror.MirrorApiServiceClient;
import restclient.ext.google.mirror.factory.MirrorApiServiceClientFactory;
import restclient.ext.google.mirror.factory.MirrorApiUserClientFactory;
import restclient.ext.google.mirror.impl.BaseMirrorApiServiceClient;
import restclient.ext.google.mirror.maker.MirrorApiServiceClientMaker;
import restclient.ext.google.mirror.resource.impl.BaseMirrorContactApiServiceClient;
import restclient.ext.google.mirror.resource.impl.BaseMirrorLocationApiServiceClient;
import restclient.ext.google.mirror.resource.impl.BaseMirrorSubscriptionApiServiceClient;
import restclient.ext.google.mirror.resource.impl.BaseMirrorTimelineApiServiceClient;
import restclient.ext.google.mirror.resource.impl.BaseMirrorTimelineAttachmentApiServiceClient;
import restclient.factory.ApiUserClientFactory;
import restclient.maker.ApiServiceClientMaker;


public class BaseMirrorApiServiceClientFactory extends BaseGoogleApiServiceClientFactory implements MirrorApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(BaseMirrorApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class MirrorApiServiceClientFactoryHolder
    {
        private static final BaseMirrorApiServiceClientFactory INSTANCE = new BaseMirrorApiServiceClientFactory();
    }

    // Singleton method
    public static BaseMirrorApiServiceClientFactory getInstance()
    {
        return MirrorApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new BaseMirrorApiServiceClient(resourceBaseUrl);
    }
    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return MirrorApiServiceClientMaker.getInstance();
    }
    @Override
    protected ApiUserClientFactory makeApiUserClientFactory()
    {
        return makeGoogleApiUserClientFactory();
    }
    @Override
    protected GoogleApiUserClientFactory makeGoogleApiUserClientFactory()
    {
        return makeMirrorApiUserClientFactory();
    }
    protected MirrorApiUserClientFactory makeMirrorApiUserClientFactory()
    {
        return BaseMirrorApiUserClientFactory.getInstance();
    }

    
    // Resource create methods

    @Override
    public MirrorApiServiceClient createMirrorContactApiServiceClient()
    {
        return new BaseMirrorContactApiServiceClient();
    }

    @Override
    public MirrorApiServiceClient createMirrorLocationApiServiceClient()
    {
        return new BaseMirrorLocationApiServiceClient();
    }

    @Override
    public MirrorApiServiceClient createMirrorSubscriptionApiServiceClient()
    {
        return new BaseMirrorSubscriptionApiServiceClient();
    }

    @Override
    public MirrorApiServiceClient createMirrorTimelineApiServiceClient()
    {
        return new BaseMirrorTimelineApiServiceClient();
    }

    @Override
    public MirrorApiServiceClient createMirrorTimelineAttachmentApiServiceClient(String timelineItemId)
    {
        return new BaseMirrorTimelineAttachmentApiServiceClient(timelineItemId);
    }

//    @Override
//    public MirrorApiUserClientFactory createMirrorApiUserClientFactory()
//    {
//        return makeMirrorApiUserClientFactory();
//    }


    @Override
    public String toString()
    {
        return "BaseMirrorApiServiceClientFactory []";
    }


}
