package restclient.ext.google.mirror.resource.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.ext.google.mirror.MirrorApiServiceClient;
import restclient.ext.google.mirror.impl.BaseMirrorApiServiceClient;


// temporary
public class BaseMirrorSubscriptionApiServiceClient extends BaseMirrorApiServiceClient implements MirrorApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorSubscriptionApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public static final String SUBSCRIPTION_RESOURCE_BASE_URL = "https://www.googleapis.com/mirror/v1/subscription";

    public BaseMirrorSubscriptionApiServiceClient()
    {
        super(SUBSCRIPTION_RESOURCE_BASE_URL);
    }


    
    @Override
    public String toString()
    {
        return "BaseMirrorSubscriptionApiServiceClient [getRestServiceClient()="
                + getRestServiceClient()
                + ", getCrudMethodFilter()="
                + getCrudMethodFilter()
                + ", getListResponseType()="
                + getListResponseType()
                + ", getResourceBaseUrl()="
                + getResourceBaseUrl()
                + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getRequiredScopes()="
                + getRequiredScopes()
                + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy()
                + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }

    
}
