package restclient.ext.google.mirror.factory.manager;

import java.util.logging.Logger;

import restclient.ext.google.mirror.factory.MirrorApiServiceClientFactory;
import restclient.ext.google.mirror.factory.MirrorApiUserClientFactory;
import restclient.ext.google.mirror.factory.impl.BaseMirrorApiServiceClientFactory;
import restclient.ext.google.mirror.factory.impl.BaseMirrorApiUserClientFactory;


/**
 * This manager class uses the "Abstract Factory" pattern.
 * Client can inherit from this class to return a proper concrete factory(s).
 */
public abstract class AbstractMirrorApiClientFactoryManager
{
    private static final Logger log = Logger.getLogger(AbstractMirrorApiClientFactoryManager.class.getName());

    private MirrorApiServiceClientFactory mirrorApiServiceClientFactory;
    private MirrorApiUserClientFactory mirrorApiUserClientFactory;
    
    // FactoryManager can be a singleton.
    protected AbstractMirrorApiClientFactoryManager() 
    {
        mirrorApiServiceClientFactory = BaseMirrorApiServiceClientFactory.getInstance();
        // mirrorApiUserClientFactory = BaseMirrorApiUserClientFactory.getInstance();
        mirrorApiUserClientFactory = (MirrorApiUserClientFactory) mirrorApiServiceClientFactory.createApiUserClientFactory();
    }


    // Initialization-on-demand holder.
    private static final class AbstractMirrorApiClientFactoryManagerHolder
    {
        private static final AbstractMirrorApiClientFactoryManager INSTANCE = new AbstractMirrorApiClientFactoryManager() {};
    }

    // Singleton method
    public static AbstractMirrorApiClientFactoryManager getInstance()
    {
        return AbstractMirrorApiClientFactoryManagerHolder.INSTANCE;
    }


    // Note:
    // Client should override these methods, or the constructor, to return appropriate concrete factories.
    
    // Returns a service client factory.
    public MirrorApiServiceClientFactory getMirrorApiServiceClientFactory()
    {
        return mirrorApiServiceClientFactory;
    }
    
    // Returns a user client factory.
    public MirrorApiUserClientFactory getMirrorApiUserClientFactory()
    {
        return mirrorApiUserClientFactory;
    }


    // TBD:
    // Setters to inject factories, if needed.
    // ....
    
    public void setMirrorApiClientFactories(MirrorApiServiceClientFactory mirrorApiServiceClientFactory)
    {
        setMirrorApiClientFactories(mirrorApiServiceClientFactory, null);
    }
    public void setMirrorApiClientFactories(MirrorApiServiceClientFactory mirrorApiServiceClientFactory, MirrorApiUserClientFactory mirrorApiUserClientFactory)
    {
        if(mirrorApiServiceClientFactory != null) {
            this.mirrorApiServiceClientFactory = mirrorApiServiceClientFactory;
            if(mirrorApiUserClientFactory != null) {
                this.mirrorApiUserClientFactory = mirrorApiUserClientFactory;
            } else {
                this.mirrorApiUserClientFactory = (MirrorApiUserClientFactory) mirrorApiServiceClientFactory.createApiUserClientFactory();
            }
        } else {
            // ???
            log.info("Input mirrorApiServiceClientFactory is null. Both mirrorApiServiceClientFactory and mirrorApiUserClientFactory will be ignored.");
        }

    }
    

}
