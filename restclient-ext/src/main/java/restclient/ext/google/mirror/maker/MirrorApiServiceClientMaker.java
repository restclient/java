package restclient.ext.google.mirror.maker;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.common.AuthRefreshPolicy;
import restclient.common.AutoRedirectPolicy;
import restclient.common.CacheControlPolicy;
import restclient.common.ClientCachePolicy;
import restclient.common.CrudMethodFilter;
import restclient.common.DataAccessClient;
import restclient.common.RequestRetryPolicy;
import restclient.common.impl.AbstractCrudMethodFilter;
import restclient.ext.google.common.GoogleAuthRefreshPolicy;
import restclient.ext.google.common.GoogleAutoRedirectPolicy;
import restclient.ext.google.common.GoogleCacheControlPolicy;
import restclient.ext.google.common.GoogleClientCachePolicy;
import restclient.ext.google.common.GoogleRequestRetryPolicy;
import restclient.ext.google.impl.BaseGoogleRestServiceClient;
import restclient.impl.AbstractDataAccessClient;
import restclient.maker.ApiServiceClientMaker;


// Abstract factory.
public class MirrorApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MirrorApiServiceClientMaker.class.getName());


    protected MirrorApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class MirrorApiServiceClientMakerHolder
    {
        private static final MirrorApiServiceClientMaker INSTANCE = new MirrorApiServiceClientMaker();
    }

    // Singleton method
    public static MirrorApiServiceClientMaker getInstance()
    {
        return MirrorApiServiceClientMakerHolder.INSTANCE;
    }


    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestServiceClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new AbstractCrudMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new GoogleAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new GoogleRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new GoogleClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new GoogleCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new GoogleAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MirrorApiServiceClientMaker []";
    }


}
