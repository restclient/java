package restclient.ext.google.mirror.resource.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.credential.UserCredential;
import restclient.ext.google.mirror.MirrorApiUserClient;
import restclient.ext.google.mirror.impl.BaseMirrorApiUserClient;


public class BaseMirrorSubscriptionApiUserClient extends BaseMirrorApiUserClient implements MirrorApiUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorSubscriptionApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    
    public BaseMirrorSubscriptionApiUserClient()
    {
        this((UserCredential) null);
    }
    public BaseMirrorSubscriptionApiUserClient(UserCredential userCredential)
    {
        super(BaseMirrorSubscriptionApiServiceClient.SUBSCRIPTION_RESOURCE_BASE_URL, userCredential);
    }

    public BaseMirrorSubscriptionApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public BaseMirrorSubscriptionApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    
    @Override
    public String toString()
    {
        return "BaseMirrorSubscriptionApiUserClient [getApiServiceClient()="
                + getApiServiceClient() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
