package restclient.ext.google.mirror.resource.proxy;

import restclient.ext.google.mirror.resource.MirrorTimelineAttachmentApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorTimelineAttachmentApiUserClient extends MirrorTimelineAttachmentApiUserClient
{
    // No need for API to return the decorated client.
    // MirrorTimelineAttachmentApiUserClient getDecoratedClient();
}
