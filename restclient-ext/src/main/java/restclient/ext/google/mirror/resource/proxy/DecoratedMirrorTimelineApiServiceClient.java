package restclient.ext.google.mirror.resource.proxy;

import restclient.ext.google.mirror.resource.MirrorTimelineApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorTimelineApiServiceClient extends MirrorTimelineApiServiceClient
{
    // No need for API to return the decorated client.
    // MirrorTimelineApiServiceClient getDecoratedClient();
}
