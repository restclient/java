package restclient.ext.google.mirror.proxy;

import restclient.ext.google.mirror.MirrorApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorApiServiceClient extends MirrorApiServiceClient
{
    // No need for API to return the decorated client.
    // MirrorApiServiceClient getDecoratedClient();
}
