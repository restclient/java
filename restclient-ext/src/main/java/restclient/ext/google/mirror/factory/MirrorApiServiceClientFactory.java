package restclient.ext.google.mirror.factory;

import restclient.ext.google.factory.GoogleApiServiceClientFactory;
import restclient.ext.google.mirror.MirrorApiServiceClient;


public interface MirrorApiServiceClientFactory extends GoogleApiServiceClientFactory
{
    MirrorApiServiceClient createMirrorContactApiServiceClient();
    MirrorApiServiceClient createMirrorLocationApiServiceClient();
    MirrorApiServiceClient createMirrorSubscriptionApiServiceClient();
    MirrorApiServiceClient createMirrorTimelineApiServiceClient();
    MirrorApiServiceClient createMirrorTimelineAttachmentApiServiceClient(String timelineItemId);

    // ???
    // MirrorApiUserClientFactory createMirrorApiUserClientFactory();
}
