package restclient.ext.google.mirror.resource.proxy;

import restclient.ext.google.mirror.resource.MirrorSubscriptionApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorSubscriptionApiUserClient extends MirrorSubscriptionApiUserClient
{
    // No need for API to return the decorated client.
    // MirrorSubscriptionApiUserClient getDecoratedClient();
}
