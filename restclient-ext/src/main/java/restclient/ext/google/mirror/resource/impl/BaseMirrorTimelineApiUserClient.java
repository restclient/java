package restclient.ext.google.mirror.resource.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.credential.UserCredential;
import restclient.ext.google.mirror.MirrorApiUserClient;
import restclient.ext.google.mirror.impl.BaseMirrorApiUserClient;


public class BaseMirrorTimelineApiUserClient extends BaseMirrorApiUserClient implements MirrorApiUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorTimelineApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public BaseMirrorTimelineApiUserClient()
    {
        this((UserCredential) null);
    }
    public BaseMirrorTimelineApiUserClient(UserCredential userCredential)
    {
        super(BaseMirrorTimelineApiServiceClient.TIMELINE_RESOURCE_BASE_URL, userCredential);
    }

    public BaseMirrorTimelineApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public BaseMirrorTimelineApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    
    @Override
    public String toString()
    {
        return "BaseMirrorTimelineApiUserClient [getApiServiceClient()="
                + getApiServiceClient() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
