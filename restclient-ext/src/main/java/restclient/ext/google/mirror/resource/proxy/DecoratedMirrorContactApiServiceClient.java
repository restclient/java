package restclient.ext.google.mirror.resource.proxy;

import restclient.ext.google.mirror.resource.MirrorContactApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorContactApiServiceClient extends MirrorContactApiServiceClient
{
    // No need for API to return the decorated client.
    // MirrorContactApiServiceClient getDecoratedClient();
}
