package restclient.ext.google.proxy;

import restclient.ext.google.GoogleApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedGoogleApiUserClient extends GoogleApiUserClient
{
    // No need for API to return the decorated client.
    // GoogleApiUserClient getDecoratedClient();
}
