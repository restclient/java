package restclient.ext.google.proxy;

import restclient.ext.google.GoogleRestUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedGoogleRestUserClient extends GoogleRestUserClient
{
    // No need for API to return the decorated client.
    // GoogleRestUserClient getDecoratedClient();
}
