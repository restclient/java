package restclient.ext.google.proxy;

import restclient.ext.google.GoogleApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedGoogleApiServiceClient extends GoogleApiServiceClient
{
    // No need for API to return the decorated client.
    // GoogleApiServiceClient getDecoratedClient();
}
