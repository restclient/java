// Copyright (c) 2013 Harry Y.
// Permission is hereby granted to any person obtaining a copy of this software 
// to deal in the software without restriction subject to the following conditions: 
// "The software shall be used for good, not evil."
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. 

/**
 * RestClient ext/google module client wrapper interface,
 * following the "decorator" design pattern.
 */
package restclient.ext.google.proxy;
