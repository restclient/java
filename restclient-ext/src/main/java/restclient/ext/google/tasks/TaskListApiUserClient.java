package restclient.ext.google.tasks;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.credential.UserCredential;
import restclient.ext.google.impl.BaseGoogleApiUserClient;


public class TaskListApiUserClient extends BaseGoogleApiUserClient
{
    private static final Logger log = Logger.getLogger(TaskListApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public TaskListApiUserClient()
    {
        this((UserCredential) null);
    }
    public TaskListApiUserClient(UserCredential userCredential)
    {
        super(TaskListApiServiceClient.TASKLIST_RESOURCE_BASE_URL, userCredential);
    }

    public TaskListApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public TaskListApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }


    @Override
    public String toString()
    {
        return "TaskListApiUserClient [getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getCacheControlPolicy()=" + getCacheControlPolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
