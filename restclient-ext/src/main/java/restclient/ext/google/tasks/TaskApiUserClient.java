package restclient.ext.google.tasks;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.common.impl.AbstractResourceUrlBuilder;
import restclient.credential.UserCredential;
import restclient.ext.google.impl.BaseGoogleApiUserClient;


public class TaskApiUserClient extends BaseGoogleApiUserClient
{
    private static final Logger log = Logger.getLogger(TaskApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public TaskApiUserClient(String taskListId)
    {
        this(taskListId, null);
    }
    public TaskApiUserClient(String taskListId, UserCredential userCredential)
    {
        super(AbstractResourceUrlBuilder.buildBaseUrl(TaskApiServiceClient.TASK_PARENTRESOURCE_BASE_URL, taskListId, TaskApiServiceClient.TASK_RESOURCENAME), userCredential);
    }

    public TaskApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public TaskApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    @Override
    public String toString()
    {
        return "TaskApiUserClient [getUserCredential()=" + getUserCredential()
                + ", isAccessAllowed()=" + isAccessAllowed()
                + ", getResourceBaseUrl()=" + getResourceBaseUrl()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getCacheControlPolicy()=" + getCacheControlPolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }

   

}
