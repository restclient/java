package restclient.ext.google.tasks;

import java.util.logging.Logger;

import restclient.RestServiceClient;
import restclient.common.impl.AbstractResourceUrlBuilder;
import restclient.core.ListResponseType;
import restclient.ext.google.impl.BaseGoogleApiServiceClient;


// temporary
// https://developers.google.com/google-apps/tasks/v1/reference/tasks
public class TaskApiServiceClient extends BaseGoogleApiServiceClient
{
    private static final Logger log = Logger.getLogger(TaskApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    public static final String TASK_PARENTRESOURCE_BASE_URL = "https://www.googleapis.com/tasks/v1/lists/";
    public static final String TASK_RESOURCENAME = "tasks";

    
    public TaskApiServiceClient(String taskListId)
    {
        super(AbstractResourceUrlBuilder.buildBaseUrl(TASK_PARENTRESOURCE_BASE_URL, taskListId, TASK_RESOURCENAME));
    }

    

    @Override
    protected void init()
    {
        super.init();
        
        // TBD: Need to check this...
        setListResponseType(ListResponseType.MAP_ITEMS);
        // ...

    }



    @Override
    public String toString()
    {
        return "TaskApiServiceClient [getCrudMethodFilter()="
                + getCrudMethodFilter() + ", getListResponseType()="
                + getListResponseType() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceCacheControlPolicy()="
                + getRestServiceCacheControlPolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getCacheControlPolicy()="
                + getCacheControlPolicy() + "]";
    }

    
    
}
