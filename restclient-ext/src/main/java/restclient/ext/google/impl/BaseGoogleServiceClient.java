package restclient.ext.google.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.credential.ClientCredential;
import restclient.ext.google.GoogleServiceClient;
import restclient.impl.AbstractServiceClient;


public class BaseGoogleServiceClient extends AbstractServiceClient implements GoogleServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseGoogleServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public BaseGoogleServiceClient()
    {
        this(null);
    }

    public BaseGoogleServiceClient(ClientCredential clientCredential)
    {
        super(clientCredential);
    }


    
    @Override
    public String toString()
    {
        return "BaseGoogleServiceClient [getClientKey()=" + getClientKey()
                + ", getClientSecret()=" + getClientSecret()
                + ", getClientCredential()=" + getClientCredential() + "]";
    }
    
    
}
