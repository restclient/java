package restclient.ext.google.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import restclient.common.AuthRefreshPolicy;
import restclient.credential.UserCredential;
import restclient.ext.google.GoogleUserClient;
import restclient.ext.google.common.GoogleAuthRefreshPolicy;
import restclient.impl.AbstractUserClient;


public class BaseGoogleUserClient extends AbstractUserClient implements GoogleUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseGoogleUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    public BaseGoogleUserClient()
    {
        this(null);
    }

    public BaseGoogleUserClient(UserCredential userCredential)
    {
        super(userCredential);
    }

    
    // Factory method.
    protected AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new GoogleAuthRefreshPolicy();
    }


    
    @Override
    public String toString()
    {
        return "BaseGoogleUserClient [getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", isAuthRequired()="
                + isAuthRequired() + ", getUser()=" + getUser()
                + ", getUserId()=" + getUserId() + ", getAuthMethod()="
                + getAuthMethod() + ", getAuthToken()=" + getAuthToken()
                + ", getAuthSecret()=" + getAuthSecret() + ", getDataScopes()="
                + getDataScopes() + ", getExpirationTime()="
                + getExpirationTime() + ", getRefreshedTime()="
                + getRefreshedTime() + ", getUserCredential()="
                + getUserCredential() + "]";
    }


}
