package restclient.ext.google.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.core.ContentFormat;
import restclient.credential.UserCredential;
import restclient.credential.impl.AbstractUserCredential;
import restclient.ext.google.GoogleRestServiceClient;
import restclient.ext.google.maker.GoogleRestServiceClientMaker;
import restclient.impl.AbstractRestServiceClient;
import restclient.maker.RestServiceClientMaker;


// This is a "final" class.
// Use AbstractRestServiceClient as a base class if you need to subclass this.
public class BaseGoogleRestServiceClient extends AbstractRestServiceClient implements GoogleRestServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseGoogleRestServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public BaseGoogleRestServiceClient(String resourceBaseUrl)
    {
        super(resourceBaseUrl);        
    }
//    public GoogleRestServiceClient(ResourceUrlBuilder resourceUrlBuilder)
//    {
//        super(resourceUrlBuilder);
//    }

    @Override
    protected void init()
    {
        super.init();

        // Set the default values.
        setAuthCredentialRequired(false);  // ???
        super.setDefaultAuthCredential(new AbstractUserCredential() {});
        setRequestFormat(ContentFormat.JSON);
        setResponseFormat(ContentFormat.JSON);
        setTimeoutSeconds(10);   // ???
//        setFollowRedirect(false);
//        setMaxFollow(0);
        // ...
    }


    // Factory methods.

    @Override
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return GoogleRestServiceClientMaker.getInstance();
    }


    // Override methods
    //   primarily, for logging.

    @Override
    protected Map<String, Object> process(String method,
            UserCredential userCredential, Object inputData, String id,
            Map<String, Object> params, boolean retrying) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestServiceClient.get(): method = " + method + "; userCredential = " + userCredential + "; inputData" + inputData + "; id = " + id + "; params = " + params);
        return super.process(method, userCredential, inputData, id, params, retrying);
    }

    @Override
    public Map<String, Object> get(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestServiceClient.get(): credential = " + credential + "; id = " + id + "; params = " + params);
        return super.get(credential, id, params);
    }

    @Override
    public Map<String, Object> post(UserCredential credential, Object inputData)
            throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestServiceClient.post(): credential = " + credential + "; inputData = " + inputData);
        return super.post(credential, inputData);
    }

    @Override
    public Map<String, Object> put(UserCredential credential, Object inputData,
            String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestServiceClient.put(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        return super.put(credential, inputData, id);
    }

    @Override
    public Map<String, Object> patch(UserCredential credential,
            Object partialData, String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestServiceClient.patch(): credential = " + credential + "; partialData = " + partialData + "; id = " + id);
        return super.patch(credential, partialData, id);
    }

    @Override
    public Map<String, Object> delete(UserCredential credential, String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestServiceClient.delete(): credential = " + credential + "; id = " + id + "; params = " + params);
        return super.delete(credential, id, params);
    }


    @Override
    public String toString()
    {
        return "BaseGoogleRestServiceClient [getResourceUrlBuilder()="
                + getResourceUrlBuilder() + ", getHttpMethodFilter()="
                + getHttpMethodFilter() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getResourcePostUrl()="
                + getResourcePostUrl() + ", isAuthCredentialRequired()="
                + isAuthCredentialRequired() + ", getDefaultAuthCredential()="
                + getDefaultAuthCredential() + ", getClientCredential()="
                + getClientCredential() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getRequestFormat()="
                + getRequestFormat() + ", getResponseFormat()="
                + getResponseFormat() + ", getTimeoutSeconds()="
                + getTimeoutSeconds() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + "]";
    }


}
