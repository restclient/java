package restclient.ext.google;

import restclient.UserClient;
import restclient.credential.UserCredential;


public interface GoogleUserClient extends UserClient, UserCredential
{

}
