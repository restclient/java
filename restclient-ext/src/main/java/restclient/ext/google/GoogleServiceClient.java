package restclient.ext.google;

import restclient.ServiceClient;
import restclient.credential.ClientCredential;


public interface GoogleServiceClient extends ServiceClient, ClientCredential
{

}
