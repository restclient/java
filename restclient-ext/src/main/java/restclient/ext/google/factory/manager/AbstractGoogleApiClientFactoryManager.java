package restclient.ext.google.factory.manager;

import java.util.logging.Logger;

import restclient.ext.google.factory.GoogleApiServiceClientFactory;
import restclient.ext.google.factory.GoogleApiUserClientFactory;
import restclient.ext.google.factory.impl.BaseGoogleApiServiceClientFactory;
import restclient.ext.google.factory.impl.BaseGoogleApiUserClientFactory;


/**
 * This manager class uses the "Abstract Factory" pattern.
 * Client can inherit from this class to return a proper concrete factory(s).
 */
public abstract class AbstractGoogleApiClientFactoryManager
{
    private static final Logger log = Logger.getLogger(AbstractGoogleApiClientFactoryManager.class.getName());


    private GoogleApiServiceClientFactory googleApiServiceClientFactory;
    private GoogleApiUserClientFactory googleApiUserClientFactory;
    
    // FactoryManager can be a singleton.
    protected AbstractGoogleApiClientFactoryManager() 
    {
        googleApiServiceClientFactory = BaseGoogleApiServiceClientFactory.getInstance();
        // googleApiUserClientFactory = BaseGoogleApiUserClientFactory.getInstance();
        googleApiUserClientFactory = (GoogleApiUserClientFactory) googleApiServiceClientFactory.createApiUserClientFactory();
    }


    // Initialization-on-demand holder.
    private static final class AbstractGoogleApiClientFactoryManagerHolder
    {
        private static final AbstractGoogleApiClientFactoryManager INSTANCE = new AbstractGoogleApiClientFactoryManager() {};
    }

    // Singleton method
    public static AbstractGoogleApiClientFactoryManager getInstance()
    {
        return AbstractGoogleApiClientFactoryManagerHolder.INSTANCE;
    }


    // Note:
    // Client should override these methods, or the constructor, to return appropriate concrete factories.
    
    // Returns a service client factory.
    public GoogleApiServiceClientFactory getGoogleApiServiceClientFactory()
    {
        return googleApiServiceClientFactory;
    }
    
    // Returns a user client factory.
    public GoogleApiUserClientFactory getGoogleApiUserClientFactory()
    {
        return googleApiUserClientFactory;
    }


    // TBD:
    // Setters to inject factories, if needed.
    // ....
    
    public void setGoogleApiClientFactories(GoogleApiServiceClientFactory googleApiServiceClientFactory)
    {
        setGoogleApiClientFactories(googleApiServiceClientFactory, null);
    }
    public void setGoogleApiClientFactories(GoogleApiServiceClientFactory googleApiServiceClientFactory, GoogleApiUserClientFactory googleApiUserClientFactory)
    {
        if(googleApiServiceClientFactory != null) {
            this.googleApiServiceClientFactory = googleApiServiceClientFactory;
            if(googleApiUserClientFactory != null) {
                this.googleApiUserClientFactory = googleApiUserClientFactory;
            } else {
                this.googleApiUserClientFactory = (GoogleApiUserClientFactory) googleApiServiceClientFactory.createApiUserClientFactory();
            }
        } else {
            // ???
            log.info("Input googleApiServiceClientFactory is null. Both googleApiServiceClientFactory and googleApiUserClientFactory will be ignored.");
        }

    }
    

}
