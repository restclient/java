package restclient.ext.google.factory;

import restclient.RestServiceClient;
import restclient.credential.UserCredential;
import restclient.ext.google.GoogleRestUserClient;
import restclient.factory.ClientFactory;
import restclient.factory.RestUserClientFactory;


public interface GoogleRestUserClientFactory extends RestUserClientFactory, ClientFactory
{
    GoogleRestUserClient createGoogleRestUserClient(String resourceBaseUrl, UserCredential userCredential);
    GoogleRestUserClient createGoogleRestUserClient(RestServiceClient restServiceClient, UserCredential userCredential);
    
}
