package restclient.ext.google.factory;

import restclient.ext.google.GoogleApiServiceClient;
import restclient.factory.ApiServiceClientFactory;
import restclient.factory.ApiUserClientFactory;
import restclient.factory.ClientFactory;


public interface GoogleApiServiceClientFactory extends ApiServiceClientFactory, ClientFactory
{
    GoogleApiServiceClient createGoogleApiServiceClient(String resourceBaseUrl);

    // ???
    // GoogleApiUserClientFactory createGoogleApiUserClientFactory();

}
