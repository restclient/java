package restclient.ext.google.factory.impl;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ApiUserClient;
import restclient.ResourceClient;
import restclient.credential.UserCredential;
import restclient.ext.google.GoogleApiUserClient;
import restclient.ext.google.factory.GoogleApiUserClientFactory;
import restclient.ext.google.impl.BaseGoogleApiUserClient;
import restclient.ext.google.maker.GoogleApiUserClientMaker;
import restclient.factory.impl.AbstractApiUserClientFactory;
import restclient.maker.ApiUserClientMaker;


public class BaseGoogleApiUserClientFactory extends AbstractApiUserClientFactory implements GoogleApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(BaseGoogleApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleApiUserClientFactoryHolder
    {
        private static final BaseGoogleApiUserClientFactory INSTANCE = new BaseGoogleApiUserClientFactory();
    }

    // Singleton method
    public static BaseGoogleApiUserClientFactory getInstance()
    {
        return GoogleApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new BaseGoogleApiUserClient(resourceBaseUrl);
    }
    @Override
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return GoogleApiUserClientMaker.getInstance();
    }


    @Override
    public GoogleApiUserClient createGoogleApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        return new BaseGoogleApiUserClient(resourceBaseUrl, userCredential);
    }

    @Override
    public GoogleApiUserClient createGoogleApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        return new BaseGoogleApiUserClient(apiServiceClient, userCredential);
    }

    
    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiUserClient(resourceBaseUrl);
    }

    

    @Override
    public String toString()
    {
        return "BaseGoogleApiUserClientFactory []";
    }


}
