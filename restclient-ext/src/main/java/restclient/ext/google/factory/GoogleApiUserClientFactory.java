package restclient.ext.google.factory;

import restclient.ApiServiceClient;
import restclient.credential.UserCredential;
import restclient.ext.google.GoogleApiUserClient;
import restclient.factory.ApiUserClientFactory;
import restclient.factory.ClientFactory;


public interface GoogleApiUserClientFactory extends ApiUserClientFactory, ClientFactory
{
    GoogleApiUserClient createGoogleApiUserClient(String resourceBaseUrl, UserCredential userCredential);
    GoogleApiUserClient createGoogleApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential);
    
}
