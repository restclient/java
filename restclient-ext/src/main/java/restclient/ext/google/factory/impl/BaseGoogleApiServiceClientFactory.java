package restclient.ext.google.factory.impl;

import java.util.logging.Logger;

import restclient.ApiServiceClient;
import restclient.ResourceClient;
import restclient.ext.google.GoogleApiServiceClient;
import restclient.ext.google.factory.GoogleApiServiceClientFactory;
import restclient.ext.google.factory.GoogleApiUserClientFactory;
import restclient.ext.google.impl.BaseGoogleApiServiceClient;
import restclient.ext.google.maker.GoogleApiServiceClientMaker;
import restclient.factory.ApiUserClientFactory;
import restclient.factory.impl.AbstractApiServiceClientFactory;
import restclient.maker.ApiServiceClientMaker;


public class BaseGoogleApiServiceClientFactory extends AbstractApiServiceClientFactory implements GoogleApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(BaseGoogleApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleApiServiceClientFactoryHolder
    {
        private static final BaseGoogleApiServiceClientFactory INSTANCE = new BaseGoogleApiServiceClientFactory();
    }

    // Singleton method
    public static BaseGoogleApiServiceClientFactory getInstance()
    {
        return GoogleApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleApiServiceClient(resourceBaseUrl);
    }
    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return GoogleApiServiceClientMaker.getInstance();
    }
    @Override
    protected ApiUserClientFactory makeApiUserClientFactory()
    {
        return makeGoogleApiUserClientFactory();
    }
    protected GoogleApiUserClientFactory makeGoogleApiUserClientFactory()
    {
        return BaseGoogleApiUserClientFactory.getInstance();
    }


    @Override
    public GoogleApiServiceClient createGoogleApiServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleApiServiceClient(resourceBaseUrl);
    }

    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiServiceClient(resourceBaseUrl);
    }



//    @Override
//    public GoogleApiUserClientFactory createGoogleApiUserClientFactory()
//    {
//        return makeGoogleApiUserClientFactory();
//    }



    @Override
    public String toString()
    {
        return "BaseGoogleApiServiceClientFactory []";
    }


}
