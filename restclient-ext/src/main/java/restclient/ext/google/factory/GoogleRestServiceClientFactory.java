package restclient.ext.google.factory;

import restclient.ext.google.GoogleRestServiceClient;
import restclient.factory.ClientFactory;
import restclient.factory.RestServiceClientFactory;
import restclient.factory.RestUserClientFactory;


public interface GoogleRestServiceClientFactory extends RestServiceClientFactory, ClientFactory
{
    GoogleRestServiceClient createGoogleRestServiceClient(String resourceBaseUrl);

    // ???
    // GoogleRestUserClientFactory createGoogleRestUserClientFactory();

}
