package restclient.ext.google.factory.impl;

import java.util.logging.Logger;

import restclient.ResourceClient;
import restclient.RestServiceClient;
import restclient.ext.google.GoogleRestServiceClient;
import restclient.ext.google.factory.GoogleApiUserClientFactory;
import restclient.ext.google.factory.GoogleRestServiceClientFactory;
import restclient.ext.google.factory.GoogleRestUserClientFactory;
import restclient.ext.google.impl.BaseGoogleRestServiceClient;
import restclient.ext.google.maker.GoogleRestServiceClientMaker;
import restclient.factory.ApiUserClientFactory;
import restclient.factory.RestUserClientFactory;
import restclient.factory.impl.AbstractApiUserClientFactory;
import restclient.factory.impl.AbstractRestServiceClientFactory;
import restclient.factory.impl.AbstractRestUserClientFactory;
import restclient.maker.RestServiceClientMaker;


public class BaseGoogleRestServiceClientFactory extends AbstractRestServiceClientFactory implements GoogleRestServiceClientFactory
{
    private static final Logger log = Logger.getLogger(BaseGoogleRestServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleRestServiceClientFactoryHolder
    {
        private static final BaseGoogleRestServiceClientFactory INSTANCE = new BaseGoogleRestServiceClientFactory();
    }

    // Singleton method
    public static BaseGoogleRestServiceClientFactory getInstance()
    {
        return GoogleRestServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected RestServiceClient makeRestServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestServiceClient(resourceBaseUrl);
    }
    @Override
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return GoogleRestServiceClientMaker.getInstance();
    }
    @Override
    protected RestUserClientFactory makeRestUserClientFactory()
    {
        return makeGoogleRestUserClientFactory();
    }
    protected GoogleRestUserClientFactory makeGoogleRestUserClientFactory()
    {
        return BaseGoogleRestUserClientFactory.getInstance();
    }

 
    @Override
    public GoogleRestServiceClient createGoogleRestServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestServiceClient(resourceBaseUrl);
    }


    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestServiceClient(resourceBaseUrl);
    }



//    @Override
//    public GoogleRestUserClientFactory createGoogleRestUserClientFactory()
//    {
//        return makeGoogleRestUserClientFactory();
//    }



    @Override
    public String toString()
    {
        return "BaseGoogleRestServiceClientFactory []";
    }


}
