package restclient.ext.google.factory.impl;

import java.util.logging.Logger;

import restclient.ResourceClient;
import restclient.RestServiceClient;
import restclient.RestUserClient;
import restclient.credential.UserCredential;
import restclient.ext.google.GoogleRestUserClient;
import restclient.ext.google.factory.GoogleRestUserClientFactory;
import restclient.ext.google.impl.BaseGoogleRestUserClient;
import restclient.ext.google.maker.GoogleRestUserClientMaker;
import restclient.factory.impl.AbstractRestUserClientFactory;
import restclient.maker.RestUserClientMaker;


public class BaseGoogleRestUserClientFactory extends AbstractRestUserClientFactory implements GoogleRestUserClientFactory
{
    private static final Logger log = Logger.getLogger(BaseGoogleRestUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleRestUserClientFactoryHolder
    {
        private static final BaseGoogleRestUserClientFactory INSTANCE = new BaseGoogleRestUserClientFactory();
    }

    // Singleton method
    public static BaseGoogleRestUserClientFactory getInstance()
    {
        return GoogleRestUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected RestUserClient makeRestUserClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestUserClient(resourceBaseUrl);
    }
    @Override
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return GoogleRestUserClientMaker.getInstance();
    }


    @Override
    public GoogleRestUserClient createGoogleRestUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        return new BaseGoogleRestUserClient(resourceBaseUrl, userCredential);
    }

    @Override
    public GoogleRestUserClient createGoogleRestUserClient(RestServiceClient restServiceClient, UserCredential userCredential)
    {
        return new BaseGoogleRestUserClient(restServiceClient, userCredential);
    }


    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestUserClient(resourceBaseUrl);
    }

    

    @Override
    public String toString()
    {
        return "BaseGoogleRestUserClientFactory []";
    }


}
