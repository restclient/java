package restclient.ext.google.tasks;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import restclient.RestApiException;
import restclient.core.AuthMethod;
import restclient.credential.UserCredential;
import restclient.credential.impl.AbstractUserCredential;


// Get OAuth2 access token from here: https://developers.google.com/oauthplayground/
// Scope: https://www.googleapis.com/auth/tasks
// https://developers.google.com/google-apps/tasks/v1/reference/tasklists
public class TaskListApiUserClientTest
{

    private TaskListApiUserClient taskListApiUserClient;
    

    @Before
    public void setUp() throws Exception
    {
        taskListApiUserClient = new TaskListApiUserClient();
        
        String user = "ab";
        String userId = "123";
        String authToken = "ya29.AHES6ZQNJ1TWlg40_qrIJ92CLzgG8oB4IDXacVSPrahBYehJ0rcfCZ5M";
        Set<String> dataScopes = null;
        Long expirationTime = null;
        UserCredential authCredential = new AbstractUserCredential(true, user, userId, AuthMethod.BEARER, authToken, null, dataScopes, expirationTime, null) {};
        taskListApiUserClient.setUserCredential(authCredential);

        
   
    }

    @After
    public void tearDown() throws Exception
    {
        taskListApiUserClient = null;
    }

    @Test
    public void testGet()
    {
        Object taskList = null;
        String id = "MTIxOTAxOTk1MTYwMDQ0MjA2NTY6NDQ0NTMwMTUyOjA";
        try {
            taskList = taskListApiUserClient.get(id);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("taskList = " + taskList);
    }

    @Test
    public void testList()
    {
        List<Object> taskLists = null;
        try {
            Map<String,Object> params = null;
            taskLists = taskListApiUserClient.list(params);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("taskLists = " + taskLists);
    }

    @Test
    public void testKeys()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testCreateObject()
    {
        Map<String,Object> inputData = new HashMap<String,Object>();
        inputData.put("kind", "tasks#taskList");
        inputData.put("title", "test tasklist 1");
        // etc...
        
        Object taskList = null;
        try {
            taskList = taskListApiUserClient.create(inputData);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("taskList = " + taskList);
    }

    @Test
    public void testCreateObjectString()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testUpdate()
    {
        String id = "MTIxOTAxOTk1MTYwMDQ0MjA2NTY6NDQ0NTMwMTUyOjA";

        Map<String,Object> inputData = new HashMap<String,Object>();
        inputData.put("kind", "tasks#taskList");
        inputData.put("id", id);
        inputData.put("title", "test tasklist 1 modified - " + System.currentTimeMillis());
        // etc...
        
        Object taskList = null;
        try {
            taskList = taskListApiUserClient.update(inputData, id);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("taskList = " + taskList);
    }

    @Test
    public void testDeleteString()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testDeleteMapOfStringObject()
    {
        // fail("Not yet implemented");
    }

}
