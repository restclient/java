package restclient.json;


// Base class of all RestClient exceptions.
public class RestClientJsonException extends Exception 
{
    private static final long serialVersionUID = 1L;


    public RestClientJsonException()
    {
        super();
    }
    public RestClientJsonException(String message)
    {
        super(message);
    }
    public RestClientJsonException(Throwable cause)
    {
        super(cause);
    }
    public RestClientJsonException(String message, Throwable cause)
    {
        super(message, cause);
    }
    public RestClientJsonException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
