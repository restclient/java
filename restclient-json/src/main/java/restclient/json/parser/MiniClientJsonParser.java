package restclient.json.parser;

import static restclient.json.common.TokenTypes.BOOLEAN;
import static restclient.json.common.TokenTypes.COLON;
import static restclient.json.common.TokenTypes.COMMA;
import static restclient.json.common.TokenTypes.EOF;
import static restclient.json.common.TokenTypes.LCURLY;
import static restclient.json.common.TokenTypes.LSQUARE;
import static restclient.json.common.TokenTypes.NULL;
import static restclient.json.common.TokenTypes.NUMBER;
import static restclient.json.common.TokenTypes.RCURLY;
import static restclient.json.common.TokenTypes.RSQUARE;
import static restclient.json.common.TokenTypes.STRING;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.json.LiteJsonParser;
import restclient.json.LiteJsonTokenizer;
import restclient.json.RestClientJsonException;
import restclient.json.common.JsonNull;
import restclient.json.common.JsonToken;
import restclient.json.common.MapEntry;
import restclient.json.common.TokenTypes;



// Recursive descent parser implementation using Java types.
public final class RestClientJsonParser implements LiteJsonParser
{
    private static final Logger log = Logger.getLogger(RestClientJsonParser.class.getName());
    // temporary
    private static final int HEAD_TRACE_LENGTH = 35;
    // ...


    public RestClientJsonParser()
    {
    }

    protected char[] peekCharStream(LiteJsonTokenizer tokenizer)
    {
        if(tokenizer instanceof RestClientJsonTokenizer) {
            return ((RestClientJsonTokenizer) tokenizer).peekCharStream(HEAD_TRACE_LENGTH);
        } else {
            return null;
        }
    }


    @Override
    public Object parse(String jsonStr) throws RestClientJsonException
    {
        StringReader sr = new StringReader(jsonStr);
        Object jsonObj = null;
        try {
            jsonObj = parse(sr);
        } catch (IOException e) {
            // throw new JsonException("IO error during JSON parsing. " + tokenTailBuffer.toTraceString(), e);
            throw new RestClientJsonException("IO error during JSON parsing.", e);
        }
        return jsonObj;
    }
    @Override
    public Object parse(Reader reader) throws RestClientJsonException, IOException
    {
        Object topNode = _parse(reader);
        // TBD:
        // Convert it to map/list...
        // ...
        return topNode;
    }


    private Object _parse(Reader reader) throws RestClientJsonException
    {
        if(reader == null) {
            return null;
        }

        // TBD:
        // Does this make it thread safe???
        // ...

        LiteJsonTokenizer jsonTokenizer = null;
        jsonTokenizer = new RestClientJsonTokenizer(reader);

        return _parse(jsonTokenizer);
    }
    private Object _parse(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        if(tokenizer == null) {
            return null;
        }
        
        Object topNode = null;
        int type = peekAndGetType(tokenizer);
        if(type == EOF || type == LCURLY || type == LSQUARE) {
            if(type == EOF) {
                topNode = produceJsonNull(tokenizer);
            } else if(type == LCURLY) {
                topNode = produceJsonObject(tokenizer);
            } else if(type == LSQUARE) {
                topNode = produceJsonArray(tokenizer);            
            }
        } else {
            // ???
            throw new RestClientJsonException("Json string should be Object or Array. Input tokenType = " + TokenTypes.getDisplayName(type));
        }

        if(log.isLoggable(Level.FINE)) log.fine("topnNode = " + topNode);
        return topNode;
    }
    
    
    private Map<String,Object> produceJsonObject(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        int lcurl = nextAndGetType(tokenizer);   // pop the leading {.
        if(lcurl != LCURLY) {
            // this cannot happen.
            throw new RestClientJsonException("JSON object should start with {. ");
        }

        Map<String,Object> map = new HashMap<String,Object>();
        int type = peekAndGetType(tokenizer);
        if(type == RCURLY) {
            // empty object
            JsonToken t = tokenizer.next();   // discard the trailing }.
        } else {
            Map<String,Object> members = produceJsonObjectMembers(tokenizer);
            int rcurl;
            rcurl = nextAndGetType(tokenizer);  // discard the trailing }.
            if(rcurl == RCURLY) {
                // Done
                map.putAll(members);
            } else {
                // ???
                throw new RestClientJsonException("JSON object should end with }. ");
            }
        }
        Map<String,Object> jObject = map;

        if(log.isLoggable(Level.FINE)) log.fine("jObject = " + jObject);
        return jObject;
    }

    
 
    private Map<String,Object> produceJsonObjectMembers(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        Map<String,Object> members = new HashMap<String,Object>();
        
        int type = peekAndGetType(tokenizer);
        while(type != RCURLY) {
            Map.Entry<String,Object> member = produceJsonObjectMember(tokenizer);
            if(member != null) {
                members.put(member.getKey(), member.getValue());
            }
            type = peekAndGetType(tokenizer);
            
            // "consume" the comma.
            // Note: We are very lenient when it comes to extra/repeated commas...
            while(type == COMMA) {
                JsonToken t = tokenizer.next();
                type = peekAndGetType(tokenizer);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("members = " + members);
        return members;
    }
    private Map.Entry<String,Object> produceJsonObjectMember(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        JsonToken keyToken = nextAndGetToken(tokenizer);
        int keyType = keyToken.getType();
        if(keyType != STRING) {
            throw new RestClientJsonException("JSON Object member should start with a string key. keyType = " + keyType + "; ");
        }
        String key = (String) keyToken.getValue();
        
        JsonToken colonToken = nextAndGetToken(tokenizer);   // "consume" :.
        int colonType = colonToken.getType();
        if(colonType != COLON) {
            throw new RestClientJsonException("JSON Object member should include a colon (:). ");
        }

        Object value = null;
        int type = peekAndGetType(tokenizer);
        switch(type) {
        case NULL:
            value = produceJsonNull(tokenizer);
            break;
        case BOOLEAN:
            value = produceJsonBoolean(tokenizer);
            break;
        case NUMBER:
            value = produceJsonNumber(tokenizer);
            break;
        case STRING:
            value = produceJsonString(tokenizer);
            break;
        case LCURLY:
            value = produceJsonObject(tokenizer);
            break;
        case LSQUARE:
            value = produceJsonArray(tokenizer);
            break;
        default:
            // ???
            throw new RestClientJsonException("Json array element not recognized: token = " + tokenizer.peek() + "; ");
        }
        
        // TBD: Use type factory ???
        Map.Entry<String,Object> member = new MapEntry<String,Object>(key, value);
 
        if(log.isLoggable(Level.FINER)) log.finer("member = " + member);
        return member;
    }


    
    private List<Object> produceJsonArray(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        int lsq;
        lsq = nextAndGetType(tokenizer);             
        if(lsq != LSQUARE) {
            // this cannot happen.
            throw new RestClientJsonException("JSON array should start with [. ");
        }

        List<Object> list = new ArrayList<Object>();
        int type = peekAndGetType(tokenizer);
        if(type == RSQUARE) {
            // empty array
            JsonToken t = tokenizer.next();   // discard the trailing ].
        } else {
            List<Object> elements = produceJsonArrayElements(tokenizer);

            int rsq = nextAndGetType(tokenizer);  // discard the trailing ].
            if(rsq == RSQUARE) {
                // Done
                list.addAll(elements);
            } else {
                // ???
                throw new RestClientJsonException("JSON array should end with ]. ");
            }
        }
        List<Object> jArray = list;

        if(log.isLoggable(Level.FINE)) log.fine("jArray = " + jArray);
        return jArray;
    }

    private List<Object> produceJsonArrayElements(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        List<Object> elements = new ArrayList<Object>();

        int type = peekAndGetType(tokenizer);
        while(type != RSQUARE) {
            Object element = produceJsonArrayElement(tokenizer);
            if(element != null) {
                elements.add(element);
            }
            type = peekAndGetType(tokenizer);

            // "consume" the comma.
            // Note: We are very lenient when it comes to extra/repeated commas...
            while(type == COMMA) {
                JsonToken t = tokenizer.next();
                type = peekAndGetType(tokenizer);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("elements = " + elements);
        return elements;
    }
    private Object produceJsonArrayElement(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        Object element = null;
        int type = peekAndGetType(tokenizer);
        switch(type) {
        case NULL:
            element = produceJsonNull(tokenizer);
            break;
        case BOOLEAN:
            element = produceJsonBoolean(tokenizer);
            break;
        case NUMBER:
            element = produceJsonNumber(tokenizer);
            break;
        case STRING:
            element = produceJsonString(tokenizer);
            break;
        case LCURLY:
            element = produceJsonObject(tokenizer);
            break;
        case LSQUARE:
            element = produceJsonArray(tokenizer);
            break;
        default:
            // ???
            throw new RestClientJsonException("Json array element not recognized: token = " + tokenizer.peek() + "; ");
        }

        if(log.isLoggable(Level.FINER)) log.finer("element = " + element);
        return element;
    }

    private JsonToken peekAndGetToken(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        JsonToken s = tokenizer.peek();
        if(s == null) {
            throw new RestClientJsonException("Failed to get the next json token. ");
        }
        return s;
    }
    private int peekAndGetType(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        JsonToken s = tokenizer.peek();
        if(s == null) {
            throw new RestClientJsonException("Failed to get the next json token. ");
        }
        int type = s.getType();
        return type;
    }
    private JsonToken nextAndGetToken(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        JsonToken s = tokenizer.next();
        if(s == null) {
            throw new RestClientJsonException("Failed to get the next json token. ");
        }
        return s;
    }
    private int nextAndGetType(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        JsonToken s = tokenizer.next();
        if(s == null) {
            throw new RestClientJsonException("Failed to get the next json token. ");
        }
        int type = s.getType();
        return type;
    }

    private String produceJsonString(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        String jString = null;
        try {
            JsonToken t = tokenizer.next();
            // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> t = " + t);
            jString = (String) t.getValue();
            // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> jString = " + jString);
        } catch(Exception e) {
            throw new RestClientJsonException("Failed to create a String node. ");
        }
        return jString;
    }
    private Number produceJsonNumber(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        Number jNumber = null;
        try {
            JsonToken t = tokenizer.next();
            jNumber = (Number) t.getValue();
        } catch(Exception e) {
            throw new RestClientJsonException("Failed to create a Number node. ");
        }
        return jNumber;
    }
    private Boolean produceJsonBoolean(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        Boolean jBoolean = null;
        try {
            JsonToken t = tokenizer.next();
            jBoolean = (Boolean) t.getValue();
        } catch(Exception e) {
            throw new RestClientJsonException("Failed to create a Boolean node. ");
        }
        return jBoolean;
    }

    private Object produceJsonNull(LiteJsonTokenizer tokenizer) throws RestClientJsonException
    {
        Object jNull = null;
        try {
            JsonToken t = tokenizer.next();   // Consume the "null" literal.
            jNull = JsonNull.NULL;
        } catch(Exception e) {
            throw new RestClientJsonException("Failed to create a Null node. ");
        }
        return jNull;
    }


}
