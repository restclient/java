package restclient.json;

import java.io.IOException;
import java.io.Writer;


public interface LiteJsonBuilder
{
    String build(Object jsonObj) throws RestClientJsonException;
    void build(Writer writer, Object jsonObj) throws RestClientJsonException, IOException;

}
