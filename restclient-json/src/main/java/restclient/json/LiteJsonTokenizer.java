package restclient.json;

import restclient.json.common.JsonToken;


public interface LiteJsonTokenizer
{
    /**
     * @return true if there is more tokens in the stream.
     * @throws RestClientJsonException
     */
    boolean hasMore() throws RestClientJsonException;
    
    /**
     * @return the next token.
     * @throws RestClientJsonException
     */
    JsonToken next() throws RestClientJsonException;
    
    /**
     * @return the next token, without removing it from the stream.
     * @throws RestClientJsonException
     */
    JsonToken peek() throws RestClientJsonException;

}
