package restclient.json;

import java.io.IOException;
import java.io.Reader;


public interface LiteJsonParser
{
    Object parse(String jsonStr) throws RestClientJsonException;
    Object parse(Reader reader) throws RestClientJsonException, IOException;
}
