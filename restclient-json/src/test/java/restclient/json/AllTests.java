package restclient.json;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ restclient.json.common.AllTests.class, restclient.json.parser.AllTests.class, restclient.json.builder.AllTests.class })
public class AllTests
{

}
