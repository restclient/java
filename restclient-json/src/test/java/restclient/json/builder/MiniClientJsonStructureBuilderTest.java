package restclient.json.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import restclient.json.RestClientJsonException;

public class RestClientJsonStructureBuilderTest
{
    // private LiteJsonBuilder structureBuilder;
    private RestClientJsonStructureBuilder structureBuilder;

    @Before
    public void setUp() throws Exception
    {
        structureBuilder = new RestClientJsonStructureBuilder();
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testToJsonStructure()
    {
        Map<String,Object> map = new LinkedHashMap<String,Object>();
        List<Object> list1 = new ArrayList<Object>();
        list1.add("x");
        list1.add("y\ny\ty\ry\\y ___ \\/ / ___ </ ___ y/y\"\u0033\u0035y\u001ay");
        list1.add("z");
        list1.add(null);
        map.put("a", list1);
        map.put("b", false);
        Map<String,Object> map2 = new LinkedHashMap<String,Object>();
        map2.put("p", 100);
        map2.put("q", null);
        map2.put("r", 200);
        map2.put("s", Byte.MAX_VALUE);
        map2.put("t", new Byte[]{});
        map2.put("u", new Byte[]{Byte.MIN_VALUE});

        map.put("b2", null);
        map.put("c", map2);
        map.put("c2", map2);

        Map<String,Object> mapC1 = new LinkedHashMap<String,Object>();
        mapC1.put("ii", 33);
        mapC1.put("ii22", new Object[]{1,2,3});
        mapC1.put("jj", new char[]{'k','q','p'});
        map.put("d", mapC1);
        List<Object> listD3 = new ArrayList<Object>(Arrays.asList(new Object[]{1,2,3,4,5}));
        map.put("e", listD3);
        map.put("f", new TestBean(7, "seven"));
        
        Object obj = map;
        Object jsonObj = null;
        try {
            jsonObj = structureBuilder.buildJsonStructure(obj);
        } catch (RestClientJsonException e) {
            e.printStackTrace();
        }
        System.out.println("jsonObj = " + jsonObj);

        // For tracing
        String jsonStr = null;
        try {
            RestClientJsonBuilder jsonBuilder = new RestClientJsonBuilder();
            jsonStr = jsonBuilder.build(jsonObj);
        } catch (RestClientJsonException e) {
            e.printStackTrace();
        }
        System.out.println("jsonStr = " + jsonStr);


    }

}
