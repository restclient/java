package restclient.json.parser;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import restclient.json.LiteJsonBuilder;
import restclient.json.LiteJsonParser;
import restclient.json.RestClientJsonException;
import restclient.json.builder.RestClientJsonBuilder;


public class RestClientJsonParserTest
{

    private LiteJsonParser jsonParser;
    
    @Before
    public void setUp() throws Exception
    {
        jsonParser = new RestClientJsonParser();
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testParseString()
    {
        // String jsonStr = "{\"a\":[3, 5, 7]}";
        String jsonStr = "[31, {\"a\":[3, false, true], \"b\":null}, \"ft\\/st/lt\\/gt\", null]";

        try {
            Object node = jsonParser.parse(jsonStr);
            System.out.println("node = " + node);

        } catch (RestClientJsonException e) {
            e.printStackTrace();
        }
    }

    // @Test
    public void testParseReader()
    {
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\minijson\\extra\\fastjson-bug.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\minijson\\extra\\random-json1.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\minijson\\extra\\random-json2.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\minijson\\extra\\random-json3.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\minijson\\extra\\sample.json";

        String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\minijson\\extra\\mirrorapi.json";
        
        Object node = null;
        try {
            Reader reader = new FileReader(filePath);
            
            node = jsonParser.parse(reader);
            // System.out.println("node = " + node);
            
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RestClientJsonException e) {
            e.printStackTrace();
       }
        
        
        // format the jsonStr
        LiteJsonBuilder jsonBuilder = new RestClientJsonBuilder();
        String jsonStr = null;
        try {
            jsonStr = jsonBuilder.build(node);
        } catch (RestClientJsonException e) {
            e.printStackTrace();
        }
        System.out.println("--------------------------------------------------------");
        System.out.println("jsonStr = " + jsonStr);
        
        int jsonLen = jsonStr.length();
        System.out.println("jsonLen = " + jsonLen);

    }


}
