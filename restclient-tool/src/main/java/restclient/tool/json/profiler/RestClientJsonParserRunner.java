package restclient.tool.json.profiler;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

import restclient.json.LiteJsonBuilder;
import restclient.json.RestClientJsonException;
import restclient.json.builder.RestClientJsonBuilder;
import restclient.json.parser.RestClientJsonParser;


public class RestClientJsonParserRunner
{
    private static final Logger log = Logger.getLogger(RestClientJsonParser.class.getName());

    private RestClientJsonParser jsonParser = null;
    public RestClientJsonParserRunner()
    {
        init();
    }

    private void init()
    {
        jsonParser = new RestClientJsonParser();
    }
    
    public void runParse()
    {
        // Recreate RestClientJsonParser for every run.
        // For profiling purposes. (when done through a loop, we need to re-initialize it at every run).
        // init();
        
        String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\restclient\\extra\\mirrorapi.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\restclient\\extra\\random-json0.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\restclient\\extra\\random-json1.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\restclient\\extra\\random-json2.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\restclient\\extra\\random-json3.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\restclient\\extra\\fastjson-bug.json";
        // String filePath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\restclient\\extra\\sample.json";
        
        Object node = null;
        try {
            Reader reader = new FileReader(filePath);
            
            node = jsonParser.parse(reader);
            // System.out.println("node = " + node);
            String str = node.toString();
            // System.out.println("str = " + str);
            int len = str.length();
            System.out.println("str.lenth = " + len);
            
        } catch (FileNotFoundException e) {
            log.log(Level.WARNING, "Failed to find the JSON file: filePath = " + filePath, e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse the JSON file: filePath = " + filePath, e);
        } catch (RestClientJsonException e) {
            log.log(Level.WARNING, "Failed to parse the JSON file: filePath = " + filePath, e);
        }

        
        // ..
        String outputPath = "C:\\Projects\\gitprojects\\glass\\app\\appengine\\restclient\\extra\\sample-output.json";
        
        // format the jsonStr
        LiteJsonBuilder jsonBuilder = new RestClientJsonBuilder();

//        Writer writer = null;
//        try {
//            writer = new BufferedWriter(new FileWriter(outputPath));
//            // jsonBuilder.build(writer, node);
//            // jsonBuilder.build(writer, node, 4);
//            jsonBuilder.build(writer, node, -1);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        String jsonStr = null;
        try {
            jsonStr = jsonBuilder.build(node);
            // String jsonStr = jsonBuilder.build(node, 4);
            // String jsonStr = jsonBuilder.build(node, -1);

            // System.out.println("jsonStr = " + jsonStr);
        } catch (RestClientJsonException e) {
            e.printStackTrace();
        }
        if(jsonStr != null) {
            int jsonLen = jsonStr.length();
            System.out.println("jsonStr.length = " + jsonLen);
        } else {
            System.out.println("Failed build JSON string.");
        }

    }
    
    public static void main(String[] args)
    {
        System.out.println("Running...");
        
        RestClientJsonParserRunner runner = new RestClientJsonParserRunner();
        
        int counter = 0;
        while(counter < 200) {
            System.out.println(">>>> counter = " + counter);
            runner.runParse();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ++counter;
        }
        
        System.out.println("Done.");
    }
    
}
